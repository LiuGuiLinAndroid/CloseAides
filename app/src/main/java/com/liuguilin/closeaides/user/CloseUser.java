package com.liuguilin.closeaides.user;

import cn.bmob.v3.BmobUser;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.user
 *  文件名:   CloseUser
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/8 00:00
 *  描述：    用户对象
 */
public class CloseUser extends BmobUser {

    //昵称
    private String nick;
    //简介
    private String desc;

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
