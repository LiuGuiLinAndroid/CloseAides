package com.liuguilin.closeaides.ui;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.liuguilin.closeaides.R;
import com.liuguilin.closeaides.base.BaseActivity;

import de.hdodenhof.circleimageview.CircleImageView;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.ui
 *  文件名:   RegisterActivity
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/6 20:23
 *  描述：    注册
 */
public class RegisterActivity extends BaseActivity implements View.OnClickListener {

    private CircleImageView profile_image;
    private EditText et_name;
    private EditText et_desc;
    private EditText et_phone;
    private EditText et_password;
    private EditText et_code;
    private Button btn_send_sms;
    private Button btn_register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        initView();
    }

    private void initView() {
        profile_image = (CircleImageView) findViewById(R.id.profile_image);
        et_name = (EditText) findViewById(R.id.et_name);
        et_desc = (EditText) findViewById(R.id.et_desc);
        et_phone = (EditText) findViewById(R.id.et_phone);
        et_password = (EditText) findViewById(R.id.et_password);
        et_code = (EditText) findViewById(R.id.et_code);
        btn_send_sms = (Button) findViewById(R.id.btn_send_sms);
        btn_register = (Button) findViewById(R.id.btn_register);

        btn_send_sms.setOnClickListener(this);
        btn_register.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_send_sms:
                submit(true);
                break;
            case R.id.btn_register:
                submit(false);
                break;
        }
    }

    private void submit(boolean status) {

        String phone = et_phone.getText().toString().trim();
        if (TextUtils.isEmpty(phone)) {
            Toast.makeText(this, "号码", Toast.LENGTH_SHORT).show();
            return;
        }

        if (status) {
            return;
        }

        String name = et_name.getText().toString().trim();
        if (TextUtils.isEmpty(name)) {
            Toast.makeText(this, "姓名", Toast.LENGTH_SHORT).show();
            return;
        }

        String desc = et_desc.getText().toString().trim();
        if (TextUtils.isEmpty(desc)) {
            Toast.makeText(this, "简介", Toast.LENGTH_SHORT).show();
            return;
        }

        String password = et_password.getText().toString().trim();
        if (TextUtils.isEmpty(password)) {
            Toast.makeText(this, "密码", Toast.LENGTH_SHORT).show();
            return;
        }

        String code = et_code.getText().toString().trim();
        if (TextUtils.isEmpty(code)) {
            Toast.makeText(this, "验证码", Toast.LENGTH_SHORT).show();
            return;
        }
    }
}
