package com.liuguilin.closeaides.ui;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.liuguilin.closeaides.R;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.ui
 *  文件名:   ScreenBadPointActivity
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/12 20:25
 *  描述：    检查屏幕坏点
 */
public class ScreenBadPointActivity extends Activity implements View.OnClickListener {

    private int index = 0;
    private TextView tv_screen_bg;
    private int[] mColor = {Color.RED, Color.GREEN, Color.BLUE, Color.BLACK, Color.WHITE, Color.GRAY};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_bad_point);

        initView();
    }

    //初始化
    private void initView() {
        tv_screen_bg = (TextView) findViewById(R.id.tv_screen_bg);
        tv_screen_bg.setBackgroundColor(mColor[index]);
        tv_screen_bg.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_screen_bg:
                index++;
                if (index >= mColor.length) {
                    Toast.makeText(this, "即将退出", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            finish();
                        }
                    }, 1500);
                } else {
                    tv_screen_bg.setBackgroundColor(mColor[index]);
                }
                break;
        }
    }
}
