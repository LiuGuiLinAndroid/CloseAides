package com.liuguilin.closeaides.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.liuguilin.closeaides.R;
import com.liuguilin.closeaides.adapter.FileBrowseAdapter;
import com.liuguilin.closeaides.base.BaseActivity;
import com.liuguilin.closeaides.model.FileBrowseModel;
import com.liuguilin.closeaides.utils.CompareUtils;
import com.liuguilin.closeaides.utils.L;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.ui
 *  文件名:   FileBrowseActivity
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/13 21:39
 *  描述：    文件浏览
 */
public class FileBrowseActivity extends BaseActivity {

    private RecyclerView mRecyclerView;
    private FileBrowseAdapter fileBrowseAdapter;
    private List<FileBrowseModel> mList = new ArrayList<>();

    //内存卡根目录
    private String base_url = "/sdcard";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_browse);

        initView();
    }

    private void initView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.mRecyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        //获取数据
        getFileData(base_url);
        fileBrowseAdapter = new FileBrowseAdapter(this, mList);
        mRecyclerView.setAdapter(fileBrowseAdapter);

        //设置点击,长按事件
        fileBrowseAdapter.setOnClickListener(new FileBrowseAdapter.onClickListener() {
            @Override
            public void onClick(int position) {
                String title = mList.get(position).getTitle();
                String path = mList.get(position).getPath();
                Integer drawable = mList.get(position).getIcon();
                if (drawable == R.drawable.icon_system_folder) {
                    mList.clear();
                    getFileData(path);
                    fileBrowseAdapter.notifyDataSetChanged();
                } else {
                    openFile(title, path);
                }
            }

            @Override
            public void onLongClick(int position) {

            }
        });
    }

    //打开文件
    private void openFile(String title, String path) {
        L.i("path:" + path);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (title.endsWith(".apk")) {
            intent.setDataAndType(Uri.fromFile(new File(path)), "application/vnd.android.package-archive");
        } else if (title.endsWith(".ppt")) {
            intent.setDataAndType(Uri.fromFile(new File(path)), "application/vnd.ms-powerpoint");
        } else if (title.endsWith(".zip")) {
            intent.setDataAndType(Uri.fromFile(new File(path)), "application/x-gzip");
        } else if (title.endsWith(".rar")) {
            intent.setDataAndType(Uri.fromFile(new File(path)), "application/x-gzip");
        } else if (title.endsWith(".doc")) {
            intent.setDataAndType(Uri.fromFile(new File(path)), "application/msword");
        } else if (title.endsWith(".txt")) {
            intent.setDataAndType(Uri.fromFile(new File(path)), "text/plain");
        } else if (title.endsWith(".mp3")) {
            intent.setDataAndType(Uri.fromFile(new File(path)), "audio/*");
        } else if (title.endsWith(".mp4")) {
            intent.setDataAndType(Uri.fromFile(new File(path)), "video/*");
        } else if (title.endsWith(".pdf")) {
            intent.setDataAndType(Uri.fromFile(new File(path)), "application/pdf");
        } else if (title.endsWith(".execl")) {
            intent.setDataAndType(Uri.fromFile(new File(path)), "application/vnd.ms-excel");
        } else if (title.endsWith(".png")) {
            intent.setDataAndType(Uri.fromFile(new File(path)), "image/*");
        } else if (title.endsWith(".jpg")) {
            intent.setDataAndType(Uri.fromFile(new File(path)), "image/*");
        } else if (title.endsWith(".java")) {
            intent.setDataAndType(Uri.fromFile(new File(path)), "text/plain");
        } else if (title.endsWith(".c")) {
            intent.setDataAndType(Uri.fromFile(new File(path)), "text/plain");
        } else if (title.endsWith(".py")) {
            intent.setDataAndType(Uri.fromFile(new File(path)), "text/plain");
        } else if (title.endsWith(".json")) {
            intent.setDataAndType(Uri.fromFile(new File(path)), "text/plain");
        } else if (title.endsWith(".html")) {
            intent.setDataAndType(Uri.fromFile(new File(path)), "text/html");
        } else if (title.endsWith(".gif")) {
            intent.setDataAndType(Uri.fromFile(new File(path)), "image/gif");
        } else {
            //未知类型
            Toast.makeText(this, "未知File Type", Toast.LENGTH_SHORT).show();
        }
        try {
            startActivity(intent);
        } catch (Exception e) {
            Toast.makeText(this, "没用安装打开工具", Toast.LENGTH_SHORT).show();
        }

    }

    //获取文件列表 url为目录
    private void getFileData(String url) {
        File f = new File(url);
        File[] files = f.listFiles();

        if (files != null) {
            for (int i = 0; i < files.length; i++) {
                FileBrowseModel model = new FileBrowseModel();
                String title = files[i].getName();
                String path = files[i].getPath();
                model.setTitle(title);
                model.setPath(path);
                if (files[i].isDirectory()) {
                    model.setIcon(R.drawable.icon_system_folder);
                } else {
                    //又分很多类
                    if (title.endsWith("apk")) {
                        model.setIcon(R.drawable.icon_system_apk);
                    } else if (title.endsWith("mp3")) {
                        model.setIcon(R.drawable.icon_system_mp3);
                    } else if (title.endsWith("mp4")) {
                        model.setIcon(R.drawable.icon_system_mp4);
                    } else if (title.endsWith("txt")) {
                        model.setIcon(R.drawable.icon_system_txt);
                    } else if (title.endsWith("pdf")) {
                        model.setIcon(R.drawable.icon_system_pdf);
                    } else if (title.endsWith("doc")) {
                        model.setIcon(R.drawable.icon_system_doc);
                    } else if (title.endsWith("ppt")) {
                        model.setIcon(R.drawable.icon_system_ppt);
                    } else if (title.endsWith("execl")) {
                        model.setIcon(R.drawable.icon_system_excel);
                    } else if (title.endsWith("png")) {
                        model.setIcon(R.drawable.icon_system_png);
                    } else if (title.endsWith("jpg")) {
                        model.setIcon(R.drawable.icon_system_jpg);
                    } else if (title.endsWith("java")) {
                        model.setIcon(R.drawable.icon_system_code);
                    } else if (title.endsWith("c")) {
                        model.setIcon(R.drawable.icon_system_code);
                    } else if (title.endsWith("py")) {
                        model.setIcon(R.drawable.icon_system_code);
                    } else if (title.endsWith("json")) {
                        model.setIcon(R.drawable.icon_system_json);
                    } else if (title.endsWith("html")) {
                        model.setIcon(R.drawable.icom_system_html);
                    } else if (title.endsWith("gif")) {
                        model.setIcon(R.drawable.icon_system_gif);
                    } else if (title.endsWith("rar")) {
                        model.setIcon(R.drawable.icon_system_rar);
                    } else if (title.endsWith("zip")) {
                        model.setIcon(R.drawable.icon_system_zip);
                    } else {
                        //未知类型
                        model.setIcon(R.drawable.icon_system_unknown);
                    }
                }
                mList.add(model);
            }
        }
        //在这里进行排序
        Collections.sort(mList, new CompareUtils());
        if (!url.equals(base_url)) {
            FileBrowseModel model = new FileBrowseModel();
            model.setTitle("返回上一页/");
            model.setPath(f.getParent());
            model.setIcon(R.drawable.icon_system_folder);
            mList.add(0, model);
        }
    }
}
