package com.liuguilin.closeaides.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.liuguilin.closeaides.R;
import com.liuguilin.closeaides.adapter.SettingAdapter;
import com.liuguilin.closeaides.base.BaseActivity;
import com.liuguilin.closeaides.entity.SharedPreKey;
import com.liuguilin.closeaides.utils.DialogUtils;
import com.liuguilin.closeaides.utils.SharedPreUtils;

import java.util.ArrayList;
import java.util.List;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.ui
 *  文件名:   SettingActivity
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/7 21:13
 *  描述：    设置
 */
public class SettingActivity extends BaseActivity {

    private RecyclerView mSettingRecyclerView;
    private SettingAdapter settingAdapter;
    private List<String> mList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        initData();
        initView();
    }

    private void initData() {
        mList.add("工程模式");
        mList.add("切换天气城市");
    }

    //初始化View
    private void initView() {
        initToastDialog();

        mSettingRecyclerView = (RecyclerView) findViewById(R.id.mSettingRecyclerView);
        mSettingRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        settingAdapter = new SettingAdapter(this, mList);
        mSettingRecyclerView.setAdapter(settingAdapter);

        settingAdapter.setOnClicklistener(new SettingAdapter.onClicklistener() {
            @Override
            public void onClick(int position) {
                switch (position) {
                    case 0:
                        Toast.makeText(SettingActivity.this, "关机 / 重启  / Recovery / bootloader", Toast.LENGTH_SHORT).show();
                        break;
                    case 1:
                        startActivity(new Intent(SettingActivity.this, SwitchCityActivity.class));
                        break;
                }
            }
        });
    }

    private void initToastDialog() {

        boolean isDialog = SharedPreUtils.getBoolean(this, SharedPreKey.SHARED_IS_DIALOG, true);
        if (isDialog) {
            DialogUtils.showStandardDialog(this, "警告", "设置下部门功能需要ROOT权限")
                    .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setNegativeButton("不再提醒", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            SharedPreUtils.putBoolean(SettingActivity.this, SharedPreKey.SHARED_IS_DIALOG, false);
                            dialog.dismiss();
                        }
                    }).show();
        }
    }
}
