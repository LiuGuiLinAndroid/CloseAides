package com.liuguilin.closeaides.ui;

import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.liuguilin.closeaides.R;
import com.liuguilin.closeaides.base.BaseActivity;
import com.liuguilin.closeaides.utils.L;

import java.io.IOException;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.ui
 *  文件名:   CompassActivity
 *  创建者:   刘某人程序员
 *  创建时间:  2017/5/12 11:10
 *  描述：    指南針
 */
public class CompassActivity extends BaseActivity implements SurfaceHolder.Callback {

    private SurfaceView mSurfaceView;
    private Camera mCamera;
    private SurfaceHolder sh;

    private ImageView iv_point;
    private TextView tv_angle;

    private SensorManager sensorManager;
    private LightSensorListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compass);
        initView();
    }

    private void initView() {
        mSurfaceView = (SurfaceView) findViewById(R.id.mSurfaceView);
        sh = mSurfaceView.getHolder();
        sh.addCallback(this);

        iv_point = (ImageView) findViewById(R.id.iv_point);
        tv_angle = (TextView) findViewById(R.id.tv_angle);

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        Sensor sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
        listener = new LightSensorListener();
        sensorManager.registerListener(listener, sensor, sensorManager.SENSOR_DELAY_FASTEST);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mCamera == null) {
            mCamera = getcCamera();
            if (sh != null) {
                showViews(mCamera, sh);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        clearCamera();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        L.i("surfaceCreated");
        showViews(mCamera, sh);  // 重启功能
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        mCamera.stopPreview();
        showViews(mCamera, sh);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        clearCamera();
    }

    private Camera getcCamera() {
        Camera camera = null;
        try {
            camera = Camera.open();
        } catch (Exception e) {
            camera = null;
        }
        return camera;
    }

    private void clearCamera() {
        // 释放hold资源
        if (mCamera != null) {
            // 停止预览
            mCamera.stopPreview();
            mCamera.setPreviewCallback(null);
            // 释放相机资源
            mCamera.release();
            mCamera = null;
        }
    }

    private void showViews(Camera camera, SurfaceHolder holder) {
        // 预览相机,绑定
        try {
            camera.setPreviewDisplay(holder);
            // 系统相机默认是横屏的，我们要旋转90°
            camera.setDisplayOrientation(90);
            // 开始预览
            camera.startPreview();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    class LightSensorListener implements SensorEventListener {

        float lastValues = 0;

        @Override
        public void onSensorChanged(SensorEvent event) {
            //夹角度数
            float values = event.values[0];
            //tvResult.setText("夹角：" + values);
            /**
             * 0：东
             * 90：南
             * 180：西
             * 270：北
             */
            RotateAnimation anim = new RotateAnimation(
                    -lastValues, values, Animation.RELATIVE_TO_SELF, 0.5f,
                    Animation.RELATIVE_TO_SELF, 0.5f);
            iv_point.startAnimation(anim);
            lastValues = values;

            //int newValues = Integer.parseInt(values)

            if (values > 0 && values < 90) {
                tv_angle.setText("东 " + (int)(360-values) + "°" );
            } else if (values > 90 && values < 180) {
                tv_angle.setText("南 " + (int)(360-values)+ "°");
            } else if (values > 180 && values < 270) {
                tv_angle.setText("西 " + (int)(360-values)+ "°");
            } else if (values > 270) {
                tv_angle.setText("北 " + (int)(360-values)+ "°");
            }

        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        sensorManager.unregisterListener(listener);
    }
}
