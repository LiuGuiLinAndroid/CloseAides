package com.liuguilin.closeaides.ui;

import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.liuguilin.closeaides.R;
import com.liuguilin.closeaides.base.BaseActivity;
import com.liuguilin.closeaides.utils.WifiUtils;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.ui
 *  文件名:   WifiManagerActivity
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/17 19:18
 *  描述：    wifi管理器
 */
public class WifiManagerActivity extends BaseActivity implements View.OnClickListener {

    private Switch sw_wifi;
    private TextView tv_connect_status;
    private ImageView iv_content_icon;
    private CardView cv_connect_status;
    private TextView tv_select_net_text;
    private RecyclerView mWifiRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wifi);

        initView();
    }

    private void initView() {
        sw_wifi = (Switch) findViewById(R.id.sw_wifi);
        sw_wifi.setOnClickListener(this);
        tv_connect_status = (TextView) findViewById(R.id.tv_connect_status);
        iv_content_icon = (ImageView) findViewById(R.id.iv_content_icon);
        cv_connect_status = (CardView) findViewById(R.id.cv_connect_status);
        tv_select_net_text = (TextView) findViewById(R.id.tv_select_net_text);
        mWifiRecyclerView = (RecyclerView) findViewById(R.id.mWifiRecyclerView);


        //判断wifi是否连接
        sw_wifi.setChecked(WifiUtils.isWiFiActive(this));

        if (!sw_wifi.isChecked()) {
            isGoneLayout(true);
        }

        if (!WifiUtils.isWiFiActive(this)) {
            return;
        }

        //判断是否连接
        if (WifiUtils.isWifiConnected(this)) {
            iv_content_icon.setVisibility(View.VISIBLE);
            //获取连接的wifi
        } else {
            tv_connect_status.setText("未连接");
            iv_content_icon.setVisibility(View.GONE);
        }

        //开始搜索

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sw_wifi:
                sw_wifi.setSelected(!sw_wifi.isSelected());
                if (sw_wifi.isChecked()) {
                    isGoneLayout(false);
                    WifiUtils.isOpenWifi(this, true);
                } else {
                    //隐藏所有
                    isGoneLayout(true);
                    WifiUtils.isOpenWifi(this, false);
                }
                break;
        }
    }

    //显示/隐藏
    private void isGoneLayout(boolean isGone) {
        if (isGone) {
            cv_connect_status.setVisibility(View.GONE);
            tv_select_net_text.setVisibility(View.GONE);
            mWifiRecyclerView.setVisibility(View.GONE);
        } else {
            cv_connect_status.setVisibility(View.VISIBLE);
            tv_select_net_text.setVisibility(View.VISIBLE);
            mWifiRecyclerView.setVisibility(View.VISIBLE);
        }
    }
}
