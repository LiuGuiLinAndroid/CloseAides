package com.liuguilin.closeaides.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.liuguilin.closeaides.R;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.ui
 *  文件名:   LoginActivity
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/5 21:33
 *  描述：    登录
 */
public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText et_phone;
    private TextInputLayout mPhoneTextInputLayout;
    private EditText et_password;
    private TextInputLayout mPasswordTextInputLayout;
    private Button btn_login;
    private TextView tv_create_user;
    private TextView tv_forget_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initView();
    }

    private void initView() {
        et_phone = (EditText) findViewById(R.id.et_phone);
        mPhoneTextInputLayout = (TextInputLayout) findViewById(R.id.mPhoneTextInputLayout);
        et_password = (EditText) findViewById(R.id.et_password);
        mPasswordTextInputLayout = (TextInputLayout) findViewById(R.id.mPasswordTextInputLayout);
        btn_login = (Button) findViewById(R.id.btn_login);
        btn_login.setOnClickListener(this);
        tv_create_user = (TextView) findViewById(R.id.tv_create_user);
        tv_create_user.setOnClickListener(this);
        tv_forget_password = (TextView) findViewById(R.id.tv_forget_password);
        tv_forget_password.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login:

                break;
            case R.id.tv_create_user:
                startActivity(new Intent(this,RegisterActivity.class));
                break;
            case R.id.tv_forget_password:

                break;
        }
    }
}
