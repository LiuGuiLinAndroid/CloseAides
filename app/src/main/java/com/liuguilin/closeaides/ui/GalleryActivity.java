package com.liuguilin.closeaides.ui;

import android.app.WallpaperManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.liuguilin.closeaides.R;
import com.liuguilin.closeaides.adapter.GalleryAdapter;
import com.liuguilin.closeaides.utils.L;
import com.liuguilin.closeaides.utils.OkHttpUtils;
import com.liuguilin.closeaides.utils.SystemUiUtils;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.ui
 *  文件名:   GalleryActivity
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/16 20:24
 *  描述：    图片预览
 */
public class GalleryActivity extends AppCompatActivity implements View.OnClickListener {

    private Gallery mGallery;
    private int position;
    private ArrayList<String> mListBigUrl;
    private ImageView iv_back;
    private Button btn_set_wallpaper;
    private Button btn_download_wallpaper;

    private WallpaperManager wpManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        SystemUiUtils.showWindowUi(this);
        initView();
    }

    private void initView() {
        Intent intent = getIntent();
        position = intent.getIntExtra("position", 0);
        mListBigUrl = intent.getStringArrayListExtra("bigUrl");

        //壁纸管理器
        wpManager = WallpaperManager.getInstance(this);

        mGallery = (Gallery) findViewById(R.id.mGallery);
        iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_back.setOnClickListener(this);
        btn_set_wallpaper = (Button) findViewById(R.id.btn_set_wallpaper);
        btn_set_wallpaper.setOnClickListener(this);
        btn_download_wallpaper = (Button) findViewById(R.id.btn_download_wallpaper);
        btn_download_wallpaper.setOnClickListener(this);

        if (mListBigUrl.size() > 0) {
            mGallery.setAdapter(new GalleryAdapter(this, mListBigUrl));
            mGallery.setSelection(position);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_set_wallpaper:
                setDesktopWallpaper();
                break;
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_download_wallpaper:
                OkHttpUtils.download(Environment.getExternalStorageDirectory() + "/" + System.currentTimeMillis() + ".png", mListBigUrl.get(mGallery.getSelectedItemPosition())
                        , new OkHttpUtils.onHttpCallback() {
                            @Override
                            public void onResponse(String result) {
                                L.i("result:" + result);
                                Snackbar.make(iv_back,"下载成功",Snackbar.LENGTH_LONG).show();
                            }

                            @Override
                            public void onFailure(Call call, IOException e) {
                                L.i(e.toString());
                            }

                            @Override
                            public void onFailure(String msg) {
                                L.i(msg);
                            }
                        }, new OkHttpUtils.ProgressResponseListener() {
                            @Override
                            public void onResponseProgress(long bytesRead, long contentLength) {
                                L.i(bytesRead + ":" + contentLength);
                            }
                        });
                break;
        }
    }

    //设置桌面壁纸
    private void setDesktopWallpaper() {
        Glide.with(this).load(mListBigUrl.get(mGallery.getSelectedItemPosition())).asBitmap().into(new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                try {
                    wpManager.setBitmap(resource);
                    Toast.makeText(GalleryActivity.this, "桌面壁纸设置成功", Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    Toast.makeText(GalleryActivity.this, "桌面壁纸设置失败", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
