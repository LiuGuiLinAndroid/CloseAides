package com.liuguilin.closeaides.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.liuguilin.closeaides.R;
import com.liuguilin.closeaides.adapter.BannerAdapter;
import com.liuguilin.closeaides.base.BaseActivity;
import com.liuguilin.closeaides.model.BannerRecyclerModel;
import com.liuguilin.closeaides.model.CategoryGsonModel;
import com.liuguilin.closeaides.utils.L;
import com.liuguilin.closeaides.utils.OkHttpUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.ui
 *  文件名:   CategoryActivity
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/18 9:44
 *  描述：    壁纸分类
 */
public class CategoryActivity extends BaseActivity {

    private String name;
    private String url;
    private String nextUrl;

    private RecyclerView mRecyclerView;
    private BannerAdapter bannerAdapter;
    private GridLayoutManager gridLayoutManager;
    private List<BannerRecyclerModel> mList = new ArrayList<>();
    private ArrayList<String> mListBig = new ArrayList<>();
    private LinearLayout ll_progressbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        initView();
    }


    private void initView() {

        name = getIntent().getStringExtra("name");

        if (!TextUtils.isEmpty(name)) {
            getSupportActionBar().setTitle(name);
        }

        url = getIntent().getStringExtra("url");
        L.i("url:" + url);
        mRecyclerView = (RecyclerView) findViewById(R.id.mRecyclerView);
        gridLayoutManager = new GridLayoutManager(this, 2);
        mRecyclerView.setLayoutManager(gridLayoutManager);
        ll_progressbar = (LinearLayout) findViewById(R.id.ll_progressbar);
        bannerAdapter = new BannerAdapter(this, mList);
        mRecyclerView.setAdapter(bannerAdapter);

        //滑动监听
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    if (mRecyclerView.getLayoutManager() instanceof GridLayoutManager) {
                        //获取屏幕可见的item
                        int lastVisiblePosition = gridLayoutManager.findLastVisibleItemPosition();
                        if (lastVisiblePosition >= gridLayoutManager.getItemCount() - 1) {
                            ll_progressbar.setVisibility(View.VISIBLE);
                            loadNextData();
                        }
                    }
                }
            }
        });

        initBanner();
    }

    //记载更多数据
    private void loadNextData() {
        OkHttpUtils.get(nextUrl, new OkHttpUtils.onHttpCallback() {
            @Override
            public void onResponse(String result) {
                ll_progressbar.setVisibility(View.GONE);
                parsingJson(result);
            }

            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onFailure(String msg) {

            }
        });
    }

    //初始化壁纸数据
    private void initBanner() {
        OkHttpUtils.get(url, new OkHttpUtils.onHttpCallback() {
            @Override
            public void onResponse(String result) {
                parsingJson(result);
            }

            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onFailure(String msg) {

            }
        });
    }

    //解析
    private void parsingJson(String result) {
        Gson gson = new Gson();
        CategoryGsonModel model = gson.fromJson(result, CategoryGsonModel.class);
        //更多地址
        nextUrl = model.getLink().getNext();
        for (int i = 0; i < model.getData().size(); i++) {
            BannerRecyclerModel Bannermodel = new BannerRecyclerModel();
            Bannermodel.setSmallImg(model.getData().get(i).getSmall());
            Bannermodel.setBigimg(model.getData().get(i).getBig());
            mListBig.add(model.getData().get(i).getBig());
            mList.add(Bannermodel);
        }
        //点击事件
        bannerAdapter.setOnClickListener(new BannerAdapter.onClickListener() {
            @Override
            public void onClick(int position) {
                Intent intent = new Intent(CategoryActivity.this, GalleryActivity.class);
                intent.putExtra("position", position);
                intent.putStringArrayListExtra("bigUrl", mListBig);
                startActivity(intent);
            }
        });
        //刷新
        bannerAdapter.notifyDataSetChanged();
    }
}
