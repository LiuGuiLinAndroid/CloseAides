package com.liuguilin.closeaides.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.liuguilin.closeaides.R;
import com.liuguilin.closeaides.adapter.BannerAdapter;
import com.liuguilin.closeaides.base.BaseActivity;
import com.liuguilin.closeaides.model.BannerModel;
import com.liuguilin.closeaides.model.BannerRecyclerModel;
import com.liuguilin.closeaides.utils.L;
import com.liuguilin.closeaides.utils.OkHttpUtils;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.ui
 *  文件名:   BannerActivity
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/8 12:51
 *  描述：    轮播详情
 */
public class BannerActivity extends BaseActivity {

    private String name;
    private String url;

    private RecyclerView mRecyclerView;
    private BannerAdapter bannerAdapter;
    private List<BannerRecyclerModel> mList = new ArrayList<>();
    private ArrayList<String> mListBig = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_banner);

        initView();
    }

    private void initView() {

        name = getIntent().getStringExtra("name");

        if (!TextUtils.isEmpty(name)) {
            getSupportActionBar().setTitle(name);
        }

        url = getIntent().getStringExtra("url");
        L.i("url:" + url);
        mRecyclerView = (RecyclerView) findViewById(R.id.mRecyclerView);
        mRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));

        initBanner();
    }

    //初始化壁纸数据
    private void initBanner() {
        OkHttpUtils.get(url, new OkHttpUtils.onHttpCallback() {
            @Override
            public void onResponse(String result) {
                parsingJson(result);
            }

            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onFailure(String msg) {

            }
        });
    }

    //解析
    private void parsingJson(String result) {
        Gson gson = new Gson();
        BannerModel model = gson.fromJson(result, BannerModel.class);
        for (int i = 0; i < model.getData().size(); i++) {
            BannerRecyclerModel Bannermodel = new BannerRecyclerModel();
            Bannermodel.setSmallImg(model.getData().get(i).getSmall());
            Bannermodel.setBigimg(model.getData().get(i).getBig());
            mListBig.add(model.getData().get(i).getBig());
            mList.add(Bannermodel);
        }
        bannerAdapter = new BannerAdapter(this, mList);
        mRecyclerView.setAdapter(bannerAdapter);

        //点击事件
        bannerAdapter.setOnClickListener(new BannerAdapter.onClickListener() {
            @Override
            public void onClick(int position) {
                Intent intent = new Intent(BannerActivity.this, GalleryActivity.class);
                intent.putExtra("position", position);
                intent.putStringArrayListExtra("bigUrl", mListBig);
                startActivity(intent);
            }
        });
    }
}
