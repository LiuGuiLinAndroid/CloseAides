package com.liuguilin.closeaides.ui;

import android.os.Bundle;

import com.liuguilin.closeaides.R;
import com.liuguilin.closeaides.base.BaseActivity;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.ui
 *  文件名:   SwitchCityActivity
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/21 11:04
 *  描述：    切换城市
 */
public class SwitchCityActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_switch_city);
    }
}
