package com.liuguilin.closeaides.ui;

import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.liuguilin.closeaides.R;
import com.liuguilin.closeaides.base.BaseActivity;
import com.liuguilin.closeaides.entity.Constans;
import com.liuguilin.closeaides.model.MusicMoreModel;
import com.liuguilin.closeaides.utils.GlideUtils;
import com.liuguilin.closeaides.utils.L;
import com.liuguilin.closeaides.utils.OkHttpUtils;
import com.liuguilin.closeaides.utils.VersionUtils;

import java.io.IOException;

import okhttp3.Call;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.ui
 *  文件名:   MusicMoreActivity
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/12 22:38
 *  描述：    音乐详情
 */
public class MusicMoreActivity extends BaseActivity implements View.OnClickListener, MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnPreparedListener {

    private ImageView iv_core;
    private TextView tv_story_title;
    private TextView tv_story;
    private TextView tv_lyric;
    private TextView tv_info;
    private ImageView btn_music;
    private TextView tv_charge_edt;

    private String title;
    private String url;
    private String musicUrl;

    private SeekBar mSeekBar;
    private MediaPlayer mMediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music_more);

        initView();
    }

    //初始化
    private void initView() {

        iv_core = (ImageView) findViewById(R.id.iv_core);
        iv_core.setOnClickListener(this);
        tv_story_title = (TextView) findViewById(R.id.tv_story_title);
        tv_story = (TextView) findViewById(R.id.tv_story);
        tv_lyric = (TextView) findViewById(R.id.tv_lyric);
        tv_info = (TextView) findViewById(R.id.tv_info);
        btn_music = (ImageView) findViewById(R.id.btn_music);
        btn_music.setOnClickListener(this);
        tv_charge_edt = (TextView) findViewById(R.id.tv_charge_edt);
        mSeekBar = (SeekBar) findViewById(R.id.mSeekBar);

        String id = getIntent().getStringExtra("id");
        L.i("id:" + id);
        OkHttpUtils.get(Constans.MUSIC_MORE + id, new OkHttpUtils.onHttpCallback() {
            @Override
            public void onResponse(String result) {
                Gson gson = new Gson();
                MusicMoreModel model = gson.fromJson(result, MusicMoreModel.class);
                initData(model);
            }

            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onFailure(String msg) {

            }
        });
    }

    //初始化数据
    private void initData(MusicMoreModel model) {
        MusicMoreModel.DataBean bean = model.getData();
        title = bean.getTitle();
        url = bean.getWeb_url();
        musicUrl = bean.getMusic_id();
        if (VersionUtils.isLollipop()) {
            getSupportActionBar().setTitle(bean.getTitle());
        }
        GlideUtils.loadImageViewCenterCrop(this, bean.getCover(), iv_core);
        tv_story_title.setText(bean.getStory_title());
        tv_story.setText(Html.fromHtml(bean.getStory()));
        tv_lyric.setText(bean.getLyric());
        tv_info.setText(bean.getInfo());
        tv_charge_edt.setText(bean.getCharge_edt());

        mMediaPlayer = new MediaPlayer();
        //设置媒体类型
        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        //设置监听
        mMediaPlayer.setOnBufferingUpdateListener(this);
        mMediaPlayer.setOnPreparedListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_core:
                Intent i = new Intent(this, WebActivity.class);
                i.putExtra("name", title);
                i.putExtra("url", url);
                startActivity(i);
                break;
            case R.id.btn_music:
                if (!TextUtils.isEmpty(musicUrl)) {
                    playMusic();
                } else {
                    Toast.makeText(this, "无法播放", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    //播放音乐
    private void playMusic() {
        try {
            mMediaPlayer.reset();
            mMediaPlayer.setDataSource(musicUrl);
            mMediaPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        L.i("onBufferingUpdate");
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        L.i("onPrepared");
    }
}
