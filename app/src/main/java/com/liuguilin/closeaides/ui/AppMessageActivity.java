package com.liuguilin.closeaides.ui;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.liuguilin.closeaides.R;
import com.liuguilin.closeaides.adapter.SimpleArrayAdapter;
import com.liuguilin.closeaides.base.BaseActivity;
import com.liuguilin.closeaides.utils.AppUtils;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.ui
 *  文件名:   AppMessageActivity
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/7 20:37
 *  描述：    应用信息
 */
public class AppMessageActivity extends BaseActivity implements View.OnClickListener {

    private Drawable appIcon;
    private String appName;
    private String appPackageName;
    private ImageView iv_icon;
    private TextView tv_app_name;
    private TextView tv_app_version;
    private TextView tv_package_name;
    private ListView mListView;
    private Button btn_uninstall;

    private SimpleArrayAdapter simpleArrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_message);

        initView();
    }

    private void initView() {
        appName = getIntent().getStringExtra("name");
        getSupportActionBar().setTitle(appName);
        appPackageName = getIntent().getStringExtra("packageName");
        iv_icon = (ImageView) findViewById(R.id.iv_icon);
        tv_app_name = (TextView) findViewById(R.id.tv_app_name);
        tv_app_version = (TextView) findViewById(R.id.tv_app_version);
        tv_package_name = (TextView) findViewById(R.id.tv_package_name);
        mListView = (ListView) findViewById(R.id.mListView);
        appIcon = AppUtils.getAppIcon(this, appPackageName);
        iv_icon.setBackground(appIcon);
        tv_app_name.setText(appName);
        tv_app_version.setText(AppUtils.getAppVersion(this, appPackageName));
        tv_package_name.setText(appPackageName);
        simpleArrayAdapter = new SimpleArrayAdapter(this,AppUtils.getAppPermission(this, appPackageName));
        mListView.setAdapter(simpleArrayAdapter);
        btn_uninstall = (Button) findViewById(R.id.btn_uninstall);
        btn_uninstall.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_uninstall:
                AppUtils.unInstallApp(this, appPackageName);
                finish();
                break;
        }
    }
}
