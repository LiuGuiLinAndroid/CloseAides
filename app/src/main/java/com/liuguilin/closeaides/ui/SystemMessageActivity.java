package com.liuguilin.closeaides.ui;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.liuguilin.closeaides.R;
import com.liuguilin.closeaides.adapter.SystemMessageAdapter;
import com.liuguilin.closeaides.base.BaseActivity;
import com.liuguilin.closeaides.entity.Constans;
import com.liuguilin.closeaides.model.SystemMessageModel;
import com.liuguilin.closeaides.utils.FileUtils;
import com.liuguilin.closeaides.utils.FormatUtils;
import com.liuguilin.closeaides.utils.ScreenUtils;
import com.liuguilin.closeaides.utils.SystemFileUtils;
import com.liuguilin.closeaides.utils.VersionUtils;

import java.util.ArrayList;
import java.util.List;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.ui
 *  文件名:   SystemMessageActivity
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/14 23:35
 *  描述：    系统信息
 */
public class SystemMessageActivity extends BaseActivity {

    /**
     * 系统信息
     * <p>
     * - 基本信息
     * <p>
     * 1.品牌 Android
     * 2.型号 Android SDK 6.0 for x86
     * 3.CPU型号:Core x86
     * 4.GPU渲染器:Android Emulator
     * 5.分辨率 1440 * 2660
     * 6.后置摄像头
     * 7.设备串号
     * 8.一键root(硬广)
     * 9.数据同步(硬广)
     * <p>
     * - 存储（可用/总量）
     * <p>
     * 1.运行内存
     * 2.机身内存
     * 3.内置存储
     * 4.红包助手(硬广)
     * <p>
     * - CPU
     * 1.CPU型号
     * 2.类型：
     * 3.核心数
     * 4.核心信息 查看
     * <p>
     * - 显示
     * <p>
     * 1.GPU渲染器:Android Emulator
     * 2.供应商 Google(intel)
     * 3.GPU版本 open es
     * 4.分辨率 1440 * 2660
     * 5.屏幕密度 560ppi
     * 6.多点触控 测试
     * <p>
     * - 相机
     * 1.后摄像头
     * 2.前摄像头
     * <p>
     * - 电池
     * <p>
     * 1.电量 50%
     * 2.温度 0°C / 32°F
     * 3.降温  使用
     * <p>
     * - OS
     * <p>
     * 1.Android SDK版本 23
     * 2.内核 xxx
     * <p>
     * - 传输
     * <p>
     * 1.NFC
     * <p>
     * - 网络
     * <p>
     * 1.网络接口  MOBILE
     * <p>
     * - 传感器
     * <p>
     * 1.加速度 传感器
     * 2.环境温度 传感器
     * 3.游戏旋转向量 传感器
     * 4.地球旋转矢量 传感器
     * 5.重力 传感器
     * 6.陀螺仪 传感器
     * 7.未校准陀螺仪 传感器
     * 8.心率 传感器
     * 9.光线 传感器
     * 10.线性加速度 传感器
     * 11.磁场 传感器
     * 12.未校准磁力 传感器
     * 13.方向 传感器
     * 14.压力 传感器
     * 15.近距离 传感器
     * 16.相对温度 传感器
     * 17.旋转矢量 传感器
     * 18.特殊动作触发 传感器
     * 19.计步 传感器
     * 20.步行检测 传感器
     */

    private RecyclerView mRecyclerView;
    private SystemMessageAdapter systemMessageAdapter;
    private List<SystemMessageModel> mList = new ArrayList<>();
    private ScreenUtils screenUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_system_message);

        initView();
    }

    private void initView() {

        screenUtils = new ScreenUtils(this);
        mRecyclerView = (RecyclerView) findViewById(R.id.mRecyclerView);
        systemMessageAdapter = new SystemMessageAdapter(this, mList);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(systemMessageAdapter);

        addTitle("基本信息");
        addContent("品牌", Build.MANUFACTURER);
        addContent("型号", Build.MODEL);
        addContent("CPU", Build.CPU_ABI);
        addContent("分辨率", screenUtils.getWidth() + " * " + screenUtils.getHeight());
        addContent("IMEI", ((TelephonyManager) getSystemService(TELEPHONY_SERVICE)).getDeviceId());
        addContent("MAC", SystemFileUtils.getLocalMacAddress(this));
        addContentButton("一键root", "root");
        addContentButton("数据同步", "同步");

        addTitle("存储（可用/总量）");
        addContent("总大小", FormatUtils.FormatSize(FileUtils.getSDSize(0)));
        addContent("可用大小", FormatUtils.FormatSize(FileUtils.getSDSize(1)));
        addContent("运行内存", SystemFileUtils.getAvailMemory(this) + "/" + SystemFileUtils.getTotalMemory(this));

        addTitle("电池");
        addContent("电量", Constans.BATTERY);
        addContent("温度", Constans.BatteryT + "°");
        addContentButton("降温", "使用");

        addTitle("OS");
        addContent("Android版本", VersionUtils.getVersion() + "");
        addContent("内核", SystemFileUtils.getLinuxCoreVersion());

        addTitle("传输");
        addContent("NFC", getPackageManager().hasSystemFeature(PackageManager.FEATURE_NFC) ? "支持" : "不支持");

        addTitle("传感器");
        addContentButton("指南针","使用");


        systemMessageAdapter.setOnButtonClicklistener(new SystemMessageAdapter.onButtonClicklistener() {
            @Override
            public void onClick(View view, int position) {
                switch (position){
                    case 23:
                        startActivity(new Intent(SystemMessageActivity.this,CompassActivity.class));
                        break;
                }
                Toast.makeText(SystemMessageActivity.this, ((Button) view).getText() + ":" + position, Toast.LENGTH_SHORT).show();
            }
        });

        //刷新
        systemMessageAdapter.notifyDataSetChanged();
    }

    private void addTitle(String title) {
        SystemMessageModel bean = new SystemMessageModel();
        bean.setTitle(title);
        bean.setType(SystemMessageAdapter.LAYOUT_TITLE);
        mList.add(bean);
    }

    private void addContent(String key, String values) {
        SystemMessageModel bean = new SystemMessageModel();
        bean.setKey(key);
        bean.setValues(values);
        bean.setType(SystemMessageAdapter.LAYOUT_CONTENT_TEXT);
        mList.add(bean);
    }

    private void addContentButton(String name, String button) {
        SystemMessageModel bean = new SystemMessageModel();
        bean.setKey(name);
        bean.setButton(button);
        bean.setType(SystemMessageAdapter.LAYOUT_CONTENT_BUTTON);
        mList.add(bean);
    }
}
