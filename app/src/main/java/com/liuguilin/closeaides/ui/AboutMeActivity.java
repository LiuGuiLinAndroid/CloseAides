package com.liuguilin.closeaides.ui;

import android.os.Bundle;

import com.liuguilin.closeaides.R;
import com.liuguilin.closeaides.base.BaseActivity;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.ui
 *  文件名:   AboutMeActivity
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/17 20:12
 *  描述：    关于我
 */
public class AboutMeActivity extends BaseActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_me);
    }
}
