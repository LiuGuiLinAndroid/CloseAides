package com.liuguilin.closeaides.ui;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.liuguilin.closeaides.R;
import com.liuguilin.closeaides.base.BaseActivity;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.ui
 *  文件名:   WebActivity
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/9 16:27
 *  描述：    地址跳转
 */
public class WebActivity extends BaseActivity {

    private String name;
    private String url;
    private WebView mWebView;
    private LinearLayout llProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        initView();
    }

    private void initView() {

        name = getIntent().getStringExtra("name");
        url = getIntent().getStringExtra("url");

        //设置标题
        if (!TextUtils.isEmpty(name)) {
            getSupportActionBar().setTitle(name);
        }

        mWebView = (WebView) findViewById(R.id.mWebView);
        llProgressBar = (LinearLayout) findViewById(R.id.llProgressBar);

        WebSettings webSettings = mWebView.getSettings();
        //设置支持JS
        webSettings.setJavaScriptEnabled(true);
        //设置缩放
        webSettings.setSupportZoom(true);
        //设置操作
        webSettings.setBuiltInZoomControls(true);
        //设置缓存
        webSettings.setAppCacheEnabled(true);

        mWebView.setWebChromeClient(new WebViewClient());
        mWebView.loadUrl(url);

        //本地加载
        mWebView.setWebViewClient(new android.webkit.WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            //去掉运行商广告
            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
                //若url含广告url
                if (url.contains("adpro.cn")) {
                    return new WebResourceResponse(null, null, null);
                }
                return null;
            }
        });

    }

    //进度回调
    public class WebViewClient extends WebChromeClient {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            if (newProgress == 100) {
                llProgressBar.setVisibility(View.GONE);
            }
            super.onProgressChanged(view, newProgress);
        }
    }

    //返回
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && mWebView.canGoBack()) {
            //返回上一页
            mWebView.goBack();
            return true;
        }
        return false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mWebView != null){
            mWebView.reload();
        }
    }
}
