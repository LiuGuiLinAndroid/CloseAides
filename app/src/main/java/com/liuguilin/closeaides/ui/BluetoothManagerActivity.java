package com.liuguilin.closeaides.ui;

import android.os.Bundle;

import com.liuguilin.closeaides.R;
import com.liuguilin.closeaides.base.BaseActivity;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.ui
 *  文件名:   BluetoothManagerActivity
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/17 19:19
 *  描述：    蓝牙管理器
 */
public class BluetoothManagerActivity extends BaseActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth);
    }
}
