package com.liuguilin.closeaides.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.google.gson.Gson;
import com.liuguilin.closeaides.R;
import com.liuguilin.closeaides.base.BaseActivity;
import com.liuguilin.closeaides.model.MovieMoreModel;
import com.liuguilin.closeaides.utils.GlideUtils;
import com.liuguilin.closeaides.utils.L;
import com.liuguilin.closeaides.utils.OkHttpUtils;
import com.liuguilin.closeaides.utils.VersionUtils;
import com.liuguilin.closeaides.view.AutoViewPager;

import java.io.IOException;

import okhttp3.Call;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.ui
 *  文件名:   MovieMoreActivity
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/19 15:46
 *  描述：    电影详情
 */
public class MovieMoreActivity extends BaseActivity implements View.OnClickListener {

    private String title;
    private String id;
    private String mUrl;

    private ImageView iv_icon;
    private TextView tv_info;
    private TextView tv_officialstory;
    private TextView tv_charge_edt;
    private VideoView mVideoView;
    private AutoViewPager mViewPager;

    private String playUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_more);

        initView();
    }

    private void initView() {
        Intent intent = getIntent();
        title = intent.getStringExtra("name");
        if (VersionUtils.isLollipop()) {
            getSupportActionBar().setTitle(title);
        }
        id = intent.getStringExtra("id");


        initMoverMore();
        iv_icon = (ImageView) findViewById(R.id.iv_icon);
        iv_icon.setOnClickListener(this);
        tv_info = (TextView) findViewById(R.id.tv_info);
        tv_officialstory = (TextView) findViewById(R.id.tv_officialstory);
        tv_charge_edt = (TextView) findViewById(R.id.tv_charge_edt);
        mVideoView = (VideoView) findViewById(R.id.mVideoView);
        mViewPager = (AutoViewPager) findViewById(R.id.mViewPager);
        mViewPager.setAutoTime(2000);
        mViewPager.setStartAuto(true);
    }

    private void initMoverMore() {
        L.i("id:" + id);
        String url = "http://v3.wufazhuce.com:8000/api/movie/detail/" + id + "?channel=wdj&source=channel_movie&source_id=9240&version=4.0.2&uuid=ffffffff-a90e-706a-63f7-ccf973aae5ee&platform=android";
        OkHttpUtils.get(url, new OkHttpUtils.onHttpCallback() {
            @Override
            public void onResponse(String result) {
                MovieMoreModel model = new Gson().fromJson(result, MovieMoreModel.class);
                loadData(model);
            }

            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onFailure(String msg) {

            }
        });
    }

    //加载数据
    private void loadData(MovieMoreModel model) {
        MovieMoreModel.DataBean bean = model.getData();
        GlideUtils.loadImageViewCenterCrop(this, bean.getDetailcover(), iv_icon);
        mUrl = bean.getWeb_url();
        playUrl = bean.getVideo();
        tv_info.setText(bean.getInfo());
        tv_charge_edt.setText(bean.getCharge_edt());
        tv_officialstory.setText(bean.getOfficialstory());

        for (int i = 0; i < bean.getPhoto().size(); i++) {
            View view = View.inflate(this, R.layout.layout_movie_photo_item, null);
            ImageView iv_bg = (ImageView) view.findViewById(R.id.iv_bg);
            GlideUtils.loadImageViewCenterCrop(this, bean.getPhoto().get(i), iv_bg);
            mViewPager.setView(view);
        }

        if(!TextUtils.isEmpty(playUrl)){
            mVideoView.setVideoPath(playUrl);
            mVideoView.setMediaController(new MediaController(this));
            mVideoView.start();
        }else {
            mVideoView.setVisibility(View.GONE);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_icon:
                Intent i = new Intent(this, WebActivity.class);
                i.putExtra("name", title);
                i.putExtra("url", mUrl);
                startActivity(i);
                break;
        }
    }
}
