package com.liuguilin.closeaides.ui;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.liuguilin.closeaides.R;
import com.liuguilin.closeaides.entity.Constans;
import com.liuguilin.closeaides.user.TimeNote;
import com.liuguilin.closeaides.utils.DialogUtils;
import com.liuguilin.closeaides.utils.TimeUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import cn.bmob.v3.exception.BmobException;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.ui
 *  文件名:   TimeAddActivity
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/11 20:42
 *  描述：    添加记录
 */
public class TimeAddActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView iv_finish;
    private ImageView iv_ok;
    private EditText et_title;
    private TextView tv_date;
    private TextView tv_red;

    private DatePickerDialog dialogDate;
    private Dialog dialogColor;

    private GridView mGridView;
    private String[] mStrColor = {"红", "橙", "黄", "绿", "青", "篮", "紫", "黑",};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_add);

        initView();
    }

    private void initView() {
        iv_finish = (ImageView) findViewById(R.id.iv_finish);
        iv_finish.setOnClickListener(this);
        iv_ok = (ImageView) findViewById(R.id.iv_ok);
        iv_ok.setOnClickListener(this);
        et_title = (EditText) findViewById(R.id.et_title);
        tv_date = (TextView) findViewById(R.id.tv_date);
        tv_date.setOnClickListener(this);
        tv_date.setText(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
        tv_red = (TextView) findViewById(R.id.tv_red);
        tv_red.setOnClickListener(this);

        //获取当前的时间
        Calendar c = Calendar.getInstance();
        dialogDate = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                tv_date.setText(year + "-" + (month + 1) + "-" + dayOfMonth);
            }
        }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));

        dialogColor = DialogUtils.showViewDialog(this, R.layout.layout_select_color);
        mGridView = (GridView) dialogColor.findViewById(R.id.mGridView);
        mGridView.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mStrColor));
        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dialogColor.dismiss();
                tv_red.setText(mStrColor[position]);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_finish:
                finish();
                overridePendingTransition(R.anim.animation_in, R.anim.animation_out);
                break;
            case R.id.iv_ok:
                //添加数据
                String title = et_title.getText().toString().trim();
                if (!TextUtils.isEmpty(title)) {
                    TimeNote timeNote = new TimeNote();
                    timeNote.setTitle(title);
                    timeNote.setTime(tv_date.getText().toString());
                    timeNote.setColor(tv_red.getText().toString());
                    //插入
                    TimeUtils.Save(timeNote, new TimeUtils.onSaveListener() {
                        @Override
                        public void onSuccessful(String s) {
                            sendBroadcast(new Intent(Constans.BRO_UPDATE_TIME));
                            finish();
                        }

                        @Override
                        public void onFailure(BmobException e) {

                        }
                    });
                } else {
                    Toast.makeText(this, "请输入标题", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.tv_date:
                dialogDate.show();
                break;
            case R.id.tv_red:
                dialogColor.show();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.animation_in, R.anim.animation_out);
    }
}
