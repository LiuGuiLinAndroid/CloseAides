package com.liuguilin.closeaides.ui;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.liuguilin.closeaides.MainActivity;
import com.liuguilin.closeaides.R;
import com.liuguilin.closeaides.base.BasePermissionsActivity;
import com.liuguilin.closeaides.entity.HandlerWhat;
import com.liuguilin.closeaides.entity.SharedPreKey;
import com.liuguilin.closeaides.listener.PermissionsResultListener;
import com.liuguilin.closeaides.utils.SharedPreUtils;
import com.liuguilin.closeaides.utils.VersionUtils;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.ui
 *  文件名:   IndexActivity
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/5 20:09
 *  描述：    闪屏页
 */
public class IndexActivity extends BasePermissionsActivity {

    private static final int INTENT_TIME = 2000;
    public static final String[] mPermissions = {
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.READ_PHONE_STATE
    };

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case HandlerWhat.HANDLER_IS_FIRST:
                    if (isFirstRunApp()) {
                        startActivity(new Intent(IndexActivity.this, MainActivity.class));
                    } else {
                        startActivity(new Intent(IndexActivity.this, GuideActivity.class));
                    }
                    finish();
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index);

        if(VersionUtils.isM()){
            checkPermissions(mPermissions, 300, new PermissionsResultListener() {
                @Override
                public void onSuccessful(int[] grantResults) {
                    mHandler.sendEmptyMessageDelayed(HandlerWhat.HANDLER_IS_FIRST, INTENT_TIME);
                }

                @Override
                public void onFailure() {

                }
            });
        }else {
            mHandler.sendEmptyMessageDelayed(HandlerWhat.HANDLER_IS_FIRST, INTENT_TIME);
        }
    }

    //判断程序是否第一次运行
    private boolean isFirstRunApp() {
        boolean isFirst = SharedPreUtils.getBoolean(this, SharedPreKey.SHARED_IS_FIRST_KEY, false);
        if (isFirst) {
            return true;
        } else {
            //设置已经第一次进来过了
            SharedPreUtils.putBoolean(this, SharedPreKey.SHARED_IS_FIRST_KEY, true);
            return false;
        }
    }

    //禁止返回键
    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}
