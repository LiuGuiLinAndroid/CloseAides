package com.liuguilin.closeaides.ui;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.liuguilin.closeaides.R;
import com.liuguilin.closeaides.base.BaseActivity;
import com.liuguilin.closeaides.utils.CacheClearUtils;
import com.liuguilin.closeaides.utils.DialogUtils;
import com.liuguilin.closeaides.view.CacheScanView;


/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.ui
 *  文件名:   CacheclearActivity
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/14 22:31
 *  描述：    缓存清理
 */
public class CacheClearActivity extends BaseActivity {

    private static final int HANDLER_CLEAR = 1234;
    private CacheScanView mCacheScanView;

    private String cache_size;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case HANDLER_CLEAR:
                    if (!cache_size.equals("0.0B")) {
                        mCacheScanView.setAnimation(false);
                        DialogUtils.showStandardDialog(CacheClearActivity.this, "", "扫描到" + cache_size + "垃圾文件,是否清理？")
                                .setNegativeButton("确定", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        CacheClearUtils.clearAllCache(CacheClearActivity.this);
                                        finish();
                                    }
                                })
                                .setPositiveButton("取消", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        finish();
                                    }
                                })
                                .show();

                    } else {
                        DialogUtils.showStandardDialog(CacheClearActivity.this, "", "你的应用没有垃圾文件!")
                                .setNegativeButton("确定", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        finish();
                                    }
                                }).show();
                        mCacheScanView.setAnimation(false);
                    }
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cache_clear);

        initView();
    }

    private void initView() {
        mCacheScanView = (CacheScanView) findViewById(R.id.mCacheScanView);
        mCacheScanView.setAnimation(true);
        try {
            cache_size = CacheClearUtils.getTotalCacheSize(this);
            mCacheScanView.setText(cache_size);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mHandler.sendEmptyMessageDelayed(HANDLER_CLEAR, 2000);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mHandler.removeMessages(HANDLER_CLEAR);
    }
}
