package com.liuguilin.closeaides.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.liuguilin.closeaides.MainActivity;
import com.liuguilin.closeaides.R;
import com.liuguilin.closeaides.utils.L;
import com.liuguilin.closeaides.utils.ScreenUtils;

import java.util.ArrayList;
import java.util.List;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.ui
 *  文件名:   GlideActivity
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/5 20:10
 *  描述：    引导
 */
public class GuideActivity extends Activity implements ViewPager.OnPageChangeListener, View.OnTouchListener {

    private ViewPager mViewPager;
    private List<View> mList = new ArrayList<>();
    private View mViewOne, mViewTwo, mViewThree;

    //当前的item
    private int currentItem;
    //屏幕工具类
    private ScreenUtils screenUtils;
    //触摸坐标
    private int startX = 0;
    private int endX = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide);

        initData();
        initView();
    }

    //初始化数据
    private void initData() {
        mViewOne = View.inflate(this, R.layout.viewpager_item_one, null);
        mViewTwo = View.inflate(this, R.layout.viewpager_item_two, null);
        mViewThree = View.inflate(this, R.layout.viewpager_item_three, null);
        mList.add(mViewOne);
        mList.add(mViewTwo);
        mList.add(mViewThree);
    }

    //初始化View
    private void initView() {

        screenUtils = new ScreenUtils(this);

        mViewPager = (ViewPager) findViewById(R.id.mViewPager);
        mViewPager.setAdapter(new GuidePagerAdapter());

        //监听滑动时间
        mViewPager.addOnPageChangeListener(this);
        //触摸事件
        mViewPager.setOnTouchListener(this);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    //切换结束
    @Override
    public void onPageSelected(int position) {
        currentItem = position;
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startX = (int) event.getX();
                break;
            case MotionEvent.ACTION_UP:
                endX = (int) event.getX();
                //首先要确定的是，是否到了最后一页，然后判断是否向左滑动，并且滑动距离是否符合，我这里的判断距离是屏幕宽度的4分之一（这里可以适当控制）
                if (currentItem == (mList.size() - 1) && startX - endX > 0 && startX - endX >= (screenUtils.getWidth() / 8)) {
                    //跳转
                    L.i("跳转主页");
                    startActivity(new Intent(this, MainActivity.class));
                    finish();
                }
                break;
        }
        return false;
    }

    //适配器
    class GuidePagerAdapter extends PagerAdapter {
        @Override
        public int getCount() {
            return mList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView(mList.get(position));
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            ((ViewPager) container).addView(mList.get(position));
            return mList.get(position);
        }
    }
}
