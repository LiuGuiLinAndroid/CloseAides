package com.liuguilin.closeaides.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.view
 *  文件名:   AutoMwasureRecyclerView
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/8 17:07
 *  描述：    自动计算高度的RecyclerView
 */
public class AutoMeasureRecyclerView extends RecyclerView {

    public AutoMeasureRecyclerView(Context context) {
        super(context);
    }

    public AutoMeasureRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AutoMeasureRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * 测量模式
     * 1.精确模式（MeasureSpec.EXACTLY）
     * 2.最大模式（MeasureSpec.AT_MOST）
     * 3.未指定模式（MeasureSpec.UNSPECIFIED）
     * <p>
     * int 是 32位数值
     * 最高两位是00的时候表示"未指定模式"
     * 最高两位是01的时候表示"'精确模式"
     * 最高两位是10的时候表示"最大模式"
     */
    @Override
    protected void onMeasure(int widthSpec, int heightSpec) {
        int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);
        super.onMeasure(widthSpec, expandSpec);
    }
}