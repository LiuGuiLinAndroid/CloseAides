package com.liuguilin.closeaides.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.SweepGradient;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;

import com.liuguilin.closeaides.utils.ScreenUtils;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.view
 *  文件名:   CacheScanView
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/14 23:00
 *  描述：    缓存扫描的View
 */
public class CacheScanView extends View {

    public static final int HANDLER_WHAT = 9999;

    //屏幕高宽
    private int w, h;
    //屏幕工具
    private ScreenUtils screenUtils;

    //画笔
    private Paint mPaintLine;
    private Paint mPaintCircle;
    private Paint mPaintText;

    //矩阵
    private Matrix matrix;
    // 旋转角度
    private int start;
    //文字
    private String text;
    //动画开关
    private boolean isAnimation;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case HANDLER_WHAT:
                    start++;
                    // 参数：旋转角度，围绕点坐标的x,y坐标点
                    matrix.postRotate(start, w / 2, h / 2);
                    // 刷新重绘
                    invalidate();
                    mHandler.sendEmptyMessageDelayed(HANDLER_WHAT, 50);
                    break;
            }
        }
    };

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setAnimation(boolean animation) {
        isAnimation = animation;
        if (animation) {
            mHandler.sendEmptyMessage(HANDLER_WHAT);
        } else {
            mHandler.removeMessages(HANDLER_WHAT);
        }

    }

    public CacheScanView(Context context) {
        super(context);
        init(context);
    }

    public CacheScanView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CacheScanView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        //获取屏幕高宽
        screenUtils = new ScreenUtils(context);
        w = screenUtils.getWidth();
        h = screenUtils.getHeight();

        mPaintLine = new Paint();
        mPaintLine.setColor(Color.BLACK);
        mPaintLine.setAntiAlias(true);
        mPaintLine.setStyle(Paint.Style.STROKE);

        mPaintCircle = new Paint();
        mPaintCircle.setColor(Color.RED);
        mPaintLine.setStyle(Paint.Style.STROKE);
        mPaintCircle.setAntiAlias(true);

        mPaintText = new Paint();
        mPaintText.setTextSize(100);
        //居中
        mPaintText.setTextAlign(Paint.Align.CENTER);

        matrix = new Matrix();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(w, h);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawText(text, w / 2, 200, mPaintText);

        //绘制四个圆
        canvas.drawCircle(w / 2, h / 2, w / 2, mPaintLine);
        canvas.drawCircle(w / 2, h / 2, w / 3, mPaintLine);
        canvas.drawCircle(w / 2, h / 2, w * 4, mPaintLine);
        canvas.drawCircle(w / 2, h / 2, w / 5, mPaintLine);

        // 绘制渐变圆
        Shader mShader = new SweepGradient(w / 2, h / 2, Color.TRANSPARENT, Color.parseColor("#FFFF4081"));
        // 绘制时渐变
        mPaintCircle.setShader(mShader);
        // 增加旋转动画，使用矩阵实现
        canvas.concat(matrix); // 前置动画
        canvas.drawCircle(w / 2, h / 2, w / 2, mPaintCircle);
    }
}
