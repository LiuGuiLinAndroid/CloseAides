package com.liuguilin.closeaides;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.liuguilin.closeaides.entity.Constans;
import com.liuguilin.closeaides.fragment.AppFragment;
import com.liuguilin.closeaides.fragment.HomeFragment;
import com.liuguilin.closeaides.fragment.SystemFragment;
import com.liuguilin.closeaides.fragment.TimeFragment;
import com.liuguilin.closeaides.fragment.WallpaperFragment;
import com.liuguilin.closeaides.ui.SettingActivity;
import com.liuguilin.closeaides.zxing.CaptureActivity;

/**
 * Material Design设计 | Android 深度开发技巧剖析大型实战项目:亲信
 */
public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Toolbar mToolbar;
    private NavigationView main_nav_view;
    private DrawerLayout main_drawer_layout;

    private HomeFragment homeFragment;
    private WallpaperFragment wallpaperFragment;
    private AppFragment appFragment;
    private SystemFragment systemFragment;
    private TimeFragment timeFragment;

    private BatteryReceiver batteryReceiver;
    private AlertDialog.Builder dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initToolBar();
        initView();
    }

    //初始化ToolBar
    private void initToolBar() {
        mToolbar = (Toolbar) findViewById(R.id.mToolbar);
        setSupportActionBar(mToolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mToolbar.setElevation(0);
        }
    }

    private void initView() {

        batteryReceiver = new BatteryReceiver();
        IntentFilter intent = new IntentFilter();
        intent.addAction(Intent.ACTION_BATTERY_CHANGED);
        registerReceiver(batteryReceiver, intent);

        main_nav_view = (NavigationView) findViewById(R.id.main_nav_view);
        //设置图标显示原色
        main_nav_view.setItemIconTintList(null);
        main_nav_view.setNavigationItemSelectedListener(this);
        main_drawer_layout = (DrawerLayout) findViewById(R.id.main_drawer_layout);
        //设置切换
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, main_drawer_layout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        //切换菜单动画
        main_drawer_layout.setDrawerListener(toggle);
        //同步状态 切换按钮
        toggle.syncState();

        initHeaderView();
        initHomeFragment();
    }

    //头部View
    private void initHeaderView() {
        View view = main_nav_view.getHeaderView(0);
        TextView tv_header_name = (TextView) view.findViewById(R.id.tv_header_name);
        tv_header_name.setText("刘某人程序员");
        TextView tv_header_desc = (TextView) view.findViewById(R.id.tv_header_desc);
        tv_header_desc.setText("“一命二运三风水，四修阴德五读书”");
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_home:
                initHomeFragment();
                break;
            case R.id.menu_wallpaper:
                initWallpaperFragment();
                break;
            case R.id.menu_app:
                initAppFragment();
                break;
            case R.id.menu_system:
                initSystemFragment();
                break;
            case R.id.menu_time:
                initTimeFragment();
                break;
            case R.id.menu_share:
                Toast.makeText(this, "分享", Toast.LENGTH_SHORT).show();
                break;
            case R.id.menu_setting:
                startActivity(new Intent(this, SettingActivity.class));
                break;
        }
        //关闭
        main_drawer_layout.closeDrawer(GravityCompat.START);
        return true;
    }

    private void initHomeFragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (homeFragment == null) {
            homeFragment = new HomeFragment();
            fragmentTransaction.add(R.id.main_content_layout, homeFragment);
        }
        hideAllFragment(fragmentTransaction);
        //需要显示的fragment
        fragmentTransaction.show(homeFragment);
        fragmentTransaction.commit();
    }

    private void initWallpaperFragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (wallpaperFragment == null) {
            wallpaperFragment = new WallpaperFragment();
            fragmentTransaction.add(R.id.main_content_layout, wallpaperFragment);
        }
        hideAllFragment(fragmentTransaction);
        //需要显示的fragment
        fragmentTransaction.show(wallpaperFragment);
        fragmentTransaction.commit();
    }

    private void initAppFragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (appFragment == null) {
            appFragment = new AppFragment();
            fragmentTransaction.add(R.id.main_content_layout, appFragment);
        }
        hideAllFragment(fragmentTransaction);
        //需要显示的fragment
        fragmentTransaction.show(appFragment);
        fragmentTransaction.commit();
    }

    private void initSystemFragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (systemFragment == null) {
            systemFragment = new SystemFragment();
            fragmentTransaction.add(R.id.main_content_layout, systemFragment);
        }
        hideAllFragment(fragmentTransaction);
        //需要显示的fragment
        fragmentTransaction.show(systemFragment);
        fragmentTransaction.commit();
    }

    private void initTimeFragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (timeFragment == null) {
            timeFragment = new TimeFragment();
            fragmentTransaction.add(R.id.main_content_layout, timeFragment);
        }
        hideAllFragment(fragmentTransaction);
        //需要显示的fragment
        fragmentTransaction.show(timeFragment);
        fragmentTransaction.commit();
    }

    private void hideAllFragment(FragmentTransaction fragmentTransaction) {
        if (homeFragment != null) {
            fragmentTransaction.hide(homeFragment);
        }
        if (wallpaperFragment != null) {
            fragmentTransaction.hide(wallpaperFragment);
        }
        if (appFragment != null) {
            fragmentTransaction.hide(appFragment);
        }
        if (systemFragment != null) {
            fragmentTransaction.hide(systemFragment);
        }
        if (timeFragment != null) {
            fragmentTransaction.hide(timeFragment);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_scan:
                startActivity(new Intent(this, CaptureActivity.class));
                break;
            case R.id.menu_http:
                Uri uri = Uri.parse("http://www.imooc.com");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                break;
            case R.id.menu_share:
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_TEXT, "亲信，努力做最好的应用！");
                shareIntent.setType("text/plain");
                startActivity(shareIntent);
                break;
            case R.id.menu_setting:
                startActivity(new Intent(this, SettingActivity.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    //获取扫描的值
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    //电量的广播
    private class BatteryReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Intent.ACTION_BATTERY_CHANGED)) {
                //当前电量
                int intLevel = intent.getIntExtra("level", 0);
                //总电量
                int intScale = intent.getIntExtra("scale", 100);
                //温度
                Constans.BatteryT = intent.getIntExtra("temperature", 0) * 0.1;
                //电量赋值
                Constans.BATTERY = (intLevel * 100 / intScale) + "%";
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(batteryReceiver);
    }
}
