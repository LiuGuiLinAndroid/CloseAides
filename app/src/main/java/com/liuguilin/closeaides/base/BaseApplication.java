package com.liuguilin.closeaides.base;

import android.app.Application;

import com.liuguilin.closeaides.entity.Key;
import com.liuguilin.closeaides.utils.ProcessUtils;

import cn.bmob.v3.Bmob;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.base
 *  文件名:   BaseApplication
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/5 20:02
 *  描述：    进程入口
 */
public class BaseApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        //判断在主进程才初始化
        if (getApplicationInfo().packageName.equals(ProcessUtils.getTopProcessName())) {
            //所有操作的初始化
            init();
        }
    }

    //初始化
    private void init() {
        Bmob.initialize(this, Key.BMOB_APPLICATION_KEY);
    }
}
