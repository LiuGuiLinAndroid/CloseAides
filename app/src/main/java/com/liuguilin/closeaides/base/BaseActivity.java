package com.liuguilin.closeaides.base;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.liuguilin.closeaides.utils.VersionUtils;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.base
 *  文件名:   BaseActivity
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/5 19:55
 *  描述：    带返回键的activity基类
 */
public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        init();
    }

    //初始化
    private void init() {
        //设置显示返回键
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //判断版本
        if (VersionUtils.isLollipop()) {
            //取消阴影
            getSupportActionBar().setElevation(0);
        }
    }

    //返回键的响应
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
