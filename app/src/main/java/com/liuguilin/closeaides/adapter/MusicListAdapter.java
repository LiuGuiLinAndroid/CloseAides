package com.liuguilin.closeaides.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.liuguilin.closeaides.R;
import com.liuguilin.closeaides.model.MusicListRecyclerModel;
import com.liuguilin.closeaides.utils.GlideUtils;

import java.util.List;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.adapter
 *  文件名:   MusicListAdapter
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/12 21:58
 *  描述：    音乐列表
 */
public class MusicListAdapter extends RecyclerView.Adapter<MusicListAdapter.ViewHolder> {

    private Context mContext;
    private LayoutInflater inflater;
    private List<MusicListRecyclerModel> mList;
    private MusicListRecyclerModel model;
    private onClickListener onClickListener;

    public void setOnClickListener(MusicListAdapter.onClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public MusicListAdapter(Context mContext, List<MusicListRecyclerModel> mList) {
        this.mContext = mContext;
        this.mList = mList;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.layout_music_list_item, null);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        model = mList.get(position);
        GlideUtils.loadImageViewCenterCrop(mContext, model.getCover(), holder.iv_icon);
        holder.tv_text.setText(model.getTitle());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener.onClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView iv_icon;
        private TextView tv_text;

        public ViewHolder(View itemView) {
            super(itemView);
            iv_icon = (ImageView) itemView.findViewById(R.id.iv_icon);
            tv_text = (TextView) itemView.findViewById(R.id.tv_text);
        }
    }

    public interface onClickListener {
        void onClick(int position);
    }
}
