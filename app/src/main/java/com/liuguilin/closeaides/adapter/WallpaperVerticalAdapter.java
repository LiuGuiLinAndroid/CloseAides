package com.liuguilin.closeaides.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.liuguilin.closeaides.R;
import com.liuguilin.closeaides.model.EverydayModel;
import com.liuguilin.closeaides.utils.GlideUtils;

import java.util.List;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.adapter
 *  文件名:   WallpaperVerticalAdapter
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/8 16:17
 *  描述：    每日推荐适配器
 */
public class WallpaperVerticalAdapter extends RecyclerView.Adapter<WallpaperVerticalAdapter.ViewHolder> {

    private Context mContext;
    private LayoutInflater inflater;
    private List<EverydayModel> mList;
    private onClickListener onClickListener;

    public void setOnClickListener(WallpaperVerticalAdapter.onClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public WallpaperVerticalAdapter(Context mContext, List<EverydayModel> mList) {
        this.mContext = mContext;
        this.mList = mList;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.layout_wallpaper_vertical_item, null);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        EverydayModel model = mList.get(position);
        holder.tv_everyday_text.setText(model.getDate());
        GlideUtils.loadImageViewCenterCrop(mContext, model.getImage(), holder.iv_everyday_bg);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener.onClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView iv_everyday_bg;
        private TextView tv_everyday_text;

        public ViewHolder(View itemView) {
            super(itemView);
            iv_everyday_bg = (ImageView) itemView.findViewById(R.id.iv_everyday_bg);
            tv_everyday_text = (TextView) itemView.findViewById(R.id.tv_everyday_text);
        }
    }

    public interface onClickListener {
        void onClick(int position);
    }
}
