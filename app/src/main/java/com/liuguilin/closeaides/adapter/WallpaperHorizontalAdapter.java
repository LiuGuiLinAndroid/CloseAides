package com.liuguilin.closeaides.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.liuguilin.closeaides.R;
import com.liuguilin.closeaides.model.CategoryModel;
import com.liuguilin.closeaides.utils.GlideUtils;

import java.util.List;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.adapter
 *  文件名:   WallpaperHorizontalAdapter
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/8 16:16
 *  描述：    壁纸横向滑动分类
 */
public class WallpaperHorizontalAdapter extends RecyclerView.Adapter<WallpaperHorizontalAdapter.ViewHolder> {

    private Context mContext;
    private LayoutInflater inflater;
    private List<CategoryModel> mList;
    private onClickListener onClickListener;

    public void setOnClickListener(WallpaperHorizontalAdapter.onClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public WallpaperHorizontalAdapter(Context mContext, List<CategoryModel> mList) {
        this.mContext = mContext;
        this.mList = mList;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.layout_wallpaper_horizontal_item, null);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        CategoryModel model = mList.get(position);
        holder.tv_category_text.setText(model.getName());
        GlideUtils.loadImageViewCenterCrop(mContext, model.getCover(), holder.iv_category_bg);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener.onClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView iv_category_bg;
        private TextView tv_category_text;

        public ViewHolder(View itemView) {
            super(itemView);
            iv_category_bg = (ImageView) itemView.findViewById(R.id.iv_category_bg);
            tv_category_text = (TextView) itemView.findViewById(R.id.tv_category_text);
        }
    }

    public interface onClickListener {
        void onClick(int position);
    }
}
