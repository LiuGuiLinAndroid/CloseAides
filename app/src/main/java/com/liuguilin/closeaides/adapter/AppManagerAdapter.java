package com.liuguilin.closeaides.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.liuguilin.closeaides.R;
import com.liuguilin.closeaides.model.AppModel;

import java.util.List;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.adapter
 *  文件名:   AppManagerAdapter
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/7 20:31
 *  描述：    应用管理的适配器
 */
public class AppManagerAdapter extends RecyclerView.Adapter<AppManagerAdapter.ViewHolder> {

    private List<AppModel> mList;
    private LayoutInflater inflater;
    private onClickListener onClickListener;

    public void setOnClickListener(AppManagerAdapter.onClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public AppManagerAdapter(Context mContext, List<AppModel> mList) {
        this.mList = mList;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    //加载布局
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.layout_app_list_item, null);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    //填充数据
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        AppModel model = mList.get(position);
        holder.iv_icon.setBackground(model.getAppIcon());
        holder.tv_app_name.setText(model.getAppName());
        holder.tv_package_name.setText(model.getAppPackageName());
        holder.tv_app_size.setText(model.getAppSize());
        //设置点击事件
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener.onClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView iv_icon;
        private TextView tv_app_name;
        private TextView tv_package_name;
        private TextView tv_app_size;

        public ViewHolder(View itemView) {
            super(itemView);
            iv_icon = (ImageView) itemView.findViewById(R.id.iv_icon);
            tv_app_name = (TextView) itemView.findViewById(R.id.tv_app_name);
            tv_package_name = (TextView) itemView.findViewById(R.id.tv_package_name);
            tv_app_size = (TextView) itemView.findViewById(R.id.tv_app_size);
        }
    }

    public interface onClickListener {
        void onClick(int position);
    }
}
