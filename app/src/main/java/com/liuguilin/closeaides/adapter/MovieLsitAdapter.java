package com.liuguilin.closeaides.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.liuguilin.closeaides.R;
import com.liuguilin.closeaides.model.MovieListModel;
import com.liuguilin.closeaides.utils.GlideUtils;

import java.util.List;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.adapter
 *  文件名:   MovieLsitAdapter
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/19 15:23
 *  描述：    電影
 */
public class MovieLsitAdapter extends RecyclerView.Adapter<MovieLsitAdapter.ViewHolder> {

    private Context mContext;
    private LayoutInflater inflater;
    private List<MovieListModel> mList;
    private onClickListener onClickListener;

    public void setOnClickListener(MovieLsitAdapter.onClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public MovieLsitAdapter(Context mContext, List<MovieListModel> mList) {
        this.mContext = mContext;
        this.mList = mList;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.layout_movie_list_item, null);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        MovieListModel model = mList.get(position);
        holder.tv_subtitle.setText(model.getSubtitle());
        holder.tv_title.setText(model.getTitle());
        GlideUtils.loadImageViewCenterCrop(mContext, model.getImg_url(), holder.iv_bg);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener.onClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView iv_bg;
        private TextView tv_subtitle;
        private TextView tv_title;

        public ViewHolder(View itemView) {
            super(itemView);
            iv_bg = (ImageView) itemView.findViewById(R.id.iv_bg);
            tv_subtitle = (TextView) itemView.findViewById(R.id.tv_subtitle);
            tv_title = (TextView) itemView.findViewById(R.id.tv_title);
        }
    }

    public interface onClickListener {
        void onClick(int position);
    }
}
