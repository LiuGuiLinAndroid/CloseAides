package com.liuguilin.closeaides.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.liuguilin.closeaides.R;
import com.liuguilin.closeaides.model.SystemMessageModel;

import java.util.List;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.adapter
 *  文件名:   SystemMessageAdapter
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/14 23:37
 *  描述：    系统信息的适配器
 */
public class SystemMessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int LAYOUT_TITLE = 0;
    public static final int LAYOUT_CONTENT_TEXT = 1;
    public static final int LAYOUT_CONTENT_BUTTON = 2;
    private onButtonClicklistener onButtonClicklistener;

    private Context mContext;
    private LayoutInflater inflater;
    private List<SystemMessageModel> mList;

    public void setOnButtonClicklistener(SystemMessageAdapter.onButtonClicklistener onButtonClicklistener) {
        this.onButtonClicklistener = onButtonClicklistener;
    }

    public SystemMessageAdapter(Context mContext, List<SystemMessageModel> mList) {
        this.mContext = mContext;
        this.mList = mList;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == LAYOUT_TITLE) {
            return new TitleViewHolder(inflater.inflate(R.layout.layout_title_item, null));
        } else if (viewType == LAYOUT_CONTENT_TEXT) {
            return new ContentViewHolder(inflater.inflate(R.layout.layout_content_item, null));
        } else if (viewType == LAYOUT_CONTENT_BUTTON) {
            return new ContentButtonViewHolder(inflater.inflate(R.layout.layout_content_button_item, null));
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        SystemMessageModel model = mList.get(position);
        if (holder instanceof TitleViewHolder) {
            ((TitleViewHolder) holder).tv_title.setText(model.getTitle());
        } else if (holder instanceof ContentViewHolder) {
            ((ContentViewHolder) holder).tv_key.setText(model.getKey());
            ((ContentViewHolder) holder).tv_values.setText(model.getValues());
        } else if (holder instanceof ContentButtonViewHolder) {
            ((ContentButtonViewHolder) holder).tv_name.setText(model.getKey());
            ((ContentButtonViewHolder) holder).tv_button.setText(model.getButton());
            ((ContentButtonViewHolder) holder).tv_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onButtonClicklistener.onClick(v, position);
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        int type = mList.get(position).getType();
        return type;
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class TitleViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_title;

        public TitleViewHolder(View itemView) {
            super(itemView);
            tv_title = (TextView) itemView.findViewById(R.id.tv_title);
        }
    }

    public class ContentViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_key;
        private TextView tv_values;

        public ContentViewHolder(View itemView) {
            super(itemView);
            tv_key = (TextView) itemView.findViewById(R.id.tv_key);
            tv_values = (TextView) itemView.findViewById(R.id.tv_values);
        }
    }

    public class ContentButtonViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name;
        private Button tv_button;

        public ContentButtonViewHolder(View itemView) {
            super(itemView);
            tv_name = (TextView) itemView.findViewById(R.id.tv_name);
            tv_button = (Button) itemView.findViewById(R.id.tv_button);
        }
    }

    public interface onButtonClicklistener {
        void onClick(View view, int position);
    }
}
