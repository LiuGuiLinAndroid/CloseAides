package com.liuguilin.closeaides.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.liuguilin.closeaides.R;
import com.liuguilin.closeaides.model.FileBrowseModel;

import java.util.List;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.adapter
 *  文件名:   FileBrowseAdapter
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/13 21:52
 *  描述：    文件浏览的适配器
 */
public class FileBrowseAdapter extends RecyclerView.Adapter<FileBrowseAdapter.ViewHolder> {

    private Context mContext;
    private LayoutInflater inflater;
    private List<FileBrowseModel> mList;

    private onClickListener onClickListener;

    public void setOnClickListener(FileBrowseAdapter.onClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public FileBrowseAdapter(Context mContext, List<FileBrowseModel> mList) {
        this.mContext = mContext;
        this.mList = mList;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.layout_file_browse_item, null);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        FileBrowseModel model = mList.get(position);
        holder.tv_title.setText(model.getTitle());
        holder.tv_path.setText(model.getPath());
        holder.iv_icon.setBackgroundResource(model.getIcon());

        //设置事件
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener.onClick(position);
            }
        });
        holder.iv_icon.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                onClickListener.onLongClick(position);
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_title;
        private TextView tv_path;
        private ImageView iv_icon;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_title = (TextView) itemView.findViewById(R.id.tv_title);
            tv_path = (TextView) itemView.findViewById(R.id.tv_path);
            iv_icon = (ImageView) itemView.findViewById(R.id.iv_icon);
        }
    }

    public interface onClickListener {
        void onClick(int position);

        void onLongClick(int position);
    }
}
