package com.liuguilin.closeaides.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.liuguilin.closeaides.R;

import java.util.List;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.adapter
 *  文件名:   SystemManagerAdapter
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/12 19:55
 *  描述：    系统管理的适配器
 */
public class SystemManagerAdapter extends RecyclerView.Adapter<SystemManagerAdapter.ViewHolder> {

    private Context mContext;
    private LayoutInflater inflater;
    private List<String> mList;
    private onClickListener onClickListener;

    /**
     * ,
     R.drawable.icon_wifi,
     R.drawable.icon_bluetooth
     */
    private int[] mDrawable = {
            R.drawable.icon_screen,
            R.drawable.icon_file,
            R.drawable.icon_system,
            R.drawable.icon_clear_data};

    public void setOnClickListener(SystemManagerAdapter.onClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public SystemManagerAdapter(Context mContext, List<String> mList) {
        this.mContext = mContext;
        this.mList = mList;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.layout_system_manager_item, null);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.tv_text.setText(mList.get(position));
        holder.iv_icon.setBackgroundResource(mDrawable[position]);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener.onClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView iv_icon;
        private TextView tv_text;

        public ViewHolder(View itemView) {
            super(itemView);
            iv_icon = (ImageView) itemView.findViewById(R.id.iv_icon);
            tv_text = (TextView) itemView.findViewById(R.id.tv_text);
        }
    }

    public interface onClickListener {
        void onClick(int position);
    }
}
