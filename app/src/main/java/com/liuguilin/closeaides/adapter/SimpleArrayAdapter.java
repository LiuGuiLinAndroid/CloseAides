package com.liuguilin.closeaides.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.liuguilin.closeaides.R;

import java.util.List;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.adapter
 *  文件名:   SimpleArrayAdapter
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/8 09:34
 *  描述：    简单的适配器
 */
public class SimpleArrayAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private List<String> mList;

    public SimpleArrayAdapter(Context mContext, List<String> mList) {
        this.mList = mList;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.layout_simple_list_item, null);
            viewHolder.tv_text = (TextView) convertView.findViewById(R.id.tv_text);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.tv_text.setText(mList.get(position));
        return convertView;
    }

    class ViewHolder {
        private TextView tv_text;
    }

}
