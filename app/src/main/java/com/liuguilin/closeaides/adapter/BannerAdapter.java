package com.liuguilin.closeaides.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.liuguilin.closeaides.R;
import com.liuguilin.closeaides.model.BannerRecyclerModel;
import com.liuguilin.closeaides.utils.GlideUtils;

import java.util.List;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.adapter
 *  文件名:   BannerAdapter
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/16 16:25
 *  描述：    TODO
 */
public class BannerAdapter extends RecyclerView.Adapter<BannerAdapter.ViewHolder> {

    private Context mContext;
    private List<BannerRecyclerModel> mList;
    private LayoutInflater inflater;
    private onClickListener onClickListener;

    public void setOnClickListener(onClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public BannerAdapter(Context mContext, List<BannerRecyclerModel> mList) {
        this.mContext = mContext;
        this.mList = mList;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    //加载布局
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.layout_banner_item, null);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    //填充数据
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        BannerRecyclerModel model = mList.get(position);
        GlideUtils.loadImageViewCenterCrop(mContext, model.getSmallImg(), holder.iv_img);
        //设置点击事件
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener.onClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView iv_img;

        public ViewHolder(View itemView) {
            super(itemView);
            iv_img = (ImageView) itemView.findViewById(R.id.iv_img);
        }
    }

    public interface onClickListener {
        void onClick(int position);
    }
}
