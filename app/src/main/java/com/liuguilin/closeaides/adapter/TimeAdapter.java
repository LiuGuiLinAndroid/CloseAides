package com.liuguilin.closeaides.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.liuguilin.closeaides.R;
import com.liuguilin.closeaides.model.TimeModel;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.adapter
 *  文件名:   TimeAdapter
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/17 19:29
 *  描述：    时光的适配器
 */
public class TimeAdapter extends RecyclerView.Adapter<TimeAdapter.ViewHolder> {

    private Context mContext;
    private LayoutInflater inflater;
    private List<TimeModel> mList;
    private onClickListener onClickListener;

    public void setOnClickListener(TimeAdapter.onClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public TimeAdapter(Context mContext, List<TimeModel> mList) {
        this.mContext = mContext;
        this.mList = mList;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.layout_time_item, null);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        TimeModel model = mList.get(position);
        switch (model.getColor()) {
            case "红":
                holder.tv_color.setBackgroundColor(Color.RED);
                break;
            case "橙":
                holder.tv_color.setBackgroundColor(0xFFffbb33);
                break;
            case "黄":
                holder.tv_color.setBackgroundColor(Color.YELLOW);
                break;
            case "绿":
                holder.tv_color.setBackgroundColor(Color.GREEN);
                break;
            case "青":
                holder.tv_color.setBackgroundColor(Color.CYAN);
                break;
            case "蓝":
                holder.tv_color.setBackgroundColor(Color.BLUE);
                break;
            case "紫":
                holder.tv_color.setBackgroundColor(0xFFaa66cc);
                break;
            case "黑":
                holder.tv_color.setBackgroundColor(Color.BLACK);
                break;
        }
        holder.tv_title.setText(model.getTitle());
        holder.tv_time.setText(model.getTime());
        //计算
        holder.tv_last_time.setText(ComputationTime(model.getTime()) + " 天");

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                onClickListener.onLongClick(position);
                //长按 短按的动作
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_color;
        private TextView tv_title;
        private TextView tv_time;
        private TextView tv_last_time;

        public ViewHolder(View itemView) {
            super(itemView);

            tv_color = (TextView) itemView.findViewById(R.id.tv_color);
            tv_title = (TextView) itemView.findViewById(R.id.tv_title);
            tv_time = (TextView) itemView.findViewById(R.id.tv_time);
            tv_last_time = (TextView) itemView.findViewById(R.id.tv_last_time);
        }
    }

    //时间计算
    public int ComputationTime(String time) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        try {
            //今天的时间
            Date date1 = df.parse(df.format(new Date()));
            Date date2 = df.parse(time);
            long diff = date2.getTime() - date1.getTime();
            int days = (int) (diff / (60 * 24 * 1000 * 60));
            if (days >= 0) {
                return days;
            } else {
                return 0;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public interface onClickListener {
        void onLongClick(int position);
    }
}
