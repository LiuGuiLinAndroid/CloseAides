package com.liuguilin.closeaides.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.liuguilin.closeaides.R;
import com.liuguilin.closeaides.model.LifeListModel;

import java.util.List;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.adapter
 *  文件名:   LifeIndexAdapter
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/20 11:42
 *  描述：    TODO
 */
public class LifeIndexAdapter extends RecyclerView.Adapter<LifeIndexAdapter.ViewHolder> {

    private Context mContext;
    private List<LifeListModel> mList;
    private LayoutInflater inflater;
    private int[] mDrawable = {
            R.drawable.icon_weather_comf,
            R.drawable.icon_weather_cw,
            R.drawable.icon_weather_dres,
            R.drawable.icon_weather_flu,
            R.drawable.icon_weather_sport,
            R.drawable.icon_weather_tra,
            R.drawable.icon_weather_uv};

    public LifeIndexAdapter(Context mContext, List<LifeListModel> mList) {
        this.mContext = mContext;
        this.mList = mList;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.layout_life_list_item, null);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        LifeListModel model = mList.get(position);
        holder.iv_icon.setImageResource(mDrawable[position]);
        holder.tv_title.setText(model.getTitle());
        holder.tv_content.setText(model.getContent());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView iv_icon;
        private TextView tv_title;
        private TextView tv_content;

        public ViewHolder(View itemView) {
            super(itemView);
            iv_icon = (ImageView) itemView.findViewById(R.id.iv_icon);
            tv_title = (TextView) itemView.findViewById(R.id.tv_title);
            tv_content = (TextView) itemView.findViewById(R.id.tv_content);
        }
    }
}
