package com.liuguilin.closeaides.utils;

import com.liuguilin.closeaides.model.FileBrowseModel;

import java.util.Comparator;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.utils
 *  文件名:   CompareUtils
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/13 22:28
 *  描述：    比较器
 */
public class CompareUtils implements Comparator {
    @Override
    public int compare(Object o1, Object o2) {
        FileBrowseModel model1 = (FileBrowseModel) o1;
        FileBrowseModel model2 = (FileBrowseModel) o2;
        return model1.getTitle().compareTo(model2.getTitle());
    }
}
