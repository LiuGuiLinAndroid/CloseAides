package com.liuguilin.closeaides.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.utils
 *  文件名:   ProcessUtils
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/5 20:04
 *  描述：    进程管理
 */
public class ProcessUtils {

    //获取主进程
    public static String getTopProcessName() {
        try {
            File file = new File("/proc/" + android.os.Process.myPid() + "/" + "cmdline");
            BufferedReader mBufferedReader = new BufferedReader(new FileReader(file));
            String processName = mBufferedReader.readLine().trim();
            mBufferedReader.close();
            return processName;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
