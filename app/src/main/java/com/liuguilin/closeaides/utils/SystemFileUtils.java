package com.liuguilin.closeaides.utils;

import android.app.ActivityManager;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.os.StatFs;
import android.provider.MediaStore;
import android.text.format.Formatter;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.utils
 *  文件名:   SystemFileUtils
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/12 20:20
 *  描述：    获取系统文件各种状态
 */
public class SystemFileUtils {

    // 0：总大小 1：剩余大小
    public static long getSDSize(int index) {
        File path = Environment.getExternalStorageDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        if (index == 0) {
            long totalBlocks = stat.getBlockCount();
            return blockSize * totalBlocks;
        } else if (index == 1) {
            long availableBlocks = stat.getAvailableBlocks();
            return blockSize * availableBlocks;
        }
        return 0;
    }

    //0: 圖片 1：音频 2 ： 电影 3： 其他
    public long getTotalSize(Context mContext, int index) {
        ArrayList<MemoryInfo> resultList = null;
        switch (index) {
            case 0:
                resultList = queryAllMediaList(mContext, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                break;
            case 1:
                resultList = queryAllMediaList(mContext, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
                break;
            case 2:
                resultList = queryAllMediaList(mContext, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
                break;
        }
        long size = 0L;
        for (MemoryInfo cInfo : resultList) {
            File file = new File(cInfo.getFilePath());
            if (null != file && file.exists()) {
                size += cInfo.getFileSize();
            }
        }
        return size;
    }

    //外部存储中除音频、视频、图片之前其他文件所占内存
    public long getOtherTotalSize(Context mContext) {
        long size = getSDSize(0) - getSDSize(1)
                - getTotalSize(mContext, 0) - getTotalSize(mContext, 1)
                - getTotalSize(mContext, 2);
        if (size < 0L) {
            size = 0L;
        }
        return size;
    }

    public static ArrayList<MemoryInfo> queryAllMediaList(Context mContext, Uri uri) {
        //我们只需要两个字段：大小、文件路径
        Cursor cursor = mContext.getContentResolver().query(
                uri, new String[]{MediaStore.Audio.Media.SIZE,
                        MediaStore.Audio.Media.DATA}, null, null, null);
        ArrayList<MemoryInfo> musicList = new ArrayList<MemoryInfo>();
        try {
            if (cursor.moveToFirst()) {
                do {
                    MemoryInfo mInfo = new MemoryInfo();
                    mInfo.setFileSize(cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media.SIZE)));
                    mInfo.setFilePath(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA)));
                } while (cursor.moveToNext());
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return musicList;
    }

    static class MemoryInfo {
        private long fileSize = 0L;
        private String filePath = "";

        public long getFileSize() {
            return fileSize;
        }

        public void setFileSize(long fileSize) {
            this.fileSize = fileSize;
        }

        public String getFilePath() {
            return filePath;
        }

        public void setFilePath(String filePath) {
            this.filePath = filePath;
        }
    }

    //获取内核版本
    public static String getLinuxCoreVersion() {
        Process process = null;
        String kernelVersion = "";
        try {
            process = Runtime.getRuntime().exec("cat /proc/version");
        } catch (IOException e) {
            e.printStackTrace();
        }
        InputStream outs = process.getInputStream();
        InputStreamReader isrout = new InputStreamReader(outs);
        BufferedReader brout = new BufferedReader(isrout, 8 * 1024);

        String result = "";
        String line;
        try {
            while ((line = brout.readLine()) != null) {
                result += line;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            if (result != "") {
                String Keyword = "version ";
                int index = result.indexOf(Keyword);
                line = result.substring(index + Keyword.length());
                index = line.indexOf(" ");
                kernelVersion = line.substring(0, index);
            }
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
        return kernelVersion;
    }

    // 手机的内存信息主要在/proc/meminfo文件中，其中第一行是总内存，而剩余内存可通过ActivityManager.MemoryInfo得到。

    public static String getAvailMemory(Context mContext) {// 获取android当前可用内存大小
        ActivityManager am = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        am.getMemoryInfo(mi);
        // mi.availMem; 当前系统的可用内存
        return Formatter.formatFileSize(mContext, mi.availMem);// 将获取的内存大小规格化
    }

    public static long getAvailMemorySize(Context mContext) {// 获取android当前可用内存大小
        ActivityManager am = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        am.getMemoryInfo(mi);
        // mi.availMem; 当前系统的可用内存
        return mi.availMem;// 将获取的内存大小规格化
    }

    public static String getTotalMemory(Context mContext) {
        String str1 = "/proc/meminfo";// 系统内存信息文件
        String str2;
        String[] arrayOfString;
        long initial_memory = 0;
        try {
            FileReader localFileReader = new FileReader(str1);
            BufferedReader localBufferedReader = new BufferedReader(
                    localFileReader, 8192);
            str2 = localBufferedReader.readLine();// 读取meminfo第一行，系统总内存大小

            arrayOfString = str2.split("\\s+");
            for (String num : arrayOfString) {
                Log.i(str2, num + "\t");
            }

            initial_memory = Integer.valueOf(arrayOfString[1]).intValue() * 1024;// 获得系统总内存，单位是KB，乘以1024转换为Byte
            localBufferedReader.close();

        } catch (IOException e) {
        }
        return Formatter.formatFileSize(mContext, initial_memory);// Byte转换为KB或者MB，内存大小规格化
    }

    public static long getTotalMemorySize(Context mContext) {
        String str1 = "/proc/meminfo";// 系统内存信息文件
        String str2;
        String[] arrayOfString;
        long initial_memory = 0;
        try {
            FileReader localFileReader = new FileReader(str1);
            BufferedReader localBufferedReader = new BufferedReader(
                    localFileReader, 8192);
            str2 = localBufferedReader.readLine();// 读取meminfo第一行，系统总内存大小

            arrayOfString = str2.split("\\s+");
            for (String num : arrayOfString) {
                Log.i(str2, num + "\t");
            }

            initial_memory = Integer.valueOf(arrayOfString[1]).intValue() * 1024;// 获得系统总内存，单位是KB，乘以1024转换为Byte
            localBufferedReader.close();

        } catch (IOException e) {
        }
        return initial_memory;// Byte转换为KB或者MB，内存大小规格化
    }

    /**
     * mac地址 start
     */
    public static String getLocalMacAddress(Context mContext) {
        WifiManager wifi = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
        WifiInfo info = wifi.getConnectionInfo();
        return info.getMacAddress();
    }

}
