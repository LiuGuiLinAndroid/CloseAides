package com.liuguilin.closeaides.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;

import com.liuguilin.closeaides.model.AppModel;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.utils
 *  文件名:   AppUtils
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/7 20:25
 *  描述：    应用工具类
 */
public class AppUtils {

    //获取全部应用
    public static List<AppModel> getAllApp(Context mContext) {
        List<AppModel> appBeanList = new ArrayList<>();
        PackageManager packageManager = mContext.getPackageManager();
        //获取全部应用
        List<PackageInfo> list = packageManager.getInstalledPackages(0);
        for (PackageInfo p : list) {
            //排除自身
            if (!p.applicationInfo.packageName.equals(mContext.getPackageName())) {
                AppModel bean = new AppModel();
                bean.setAppIcon(p.applicationInfo.loadIcon(packageManager));
                bean.setAppName(packageManager.getApplicationLabel(p.applicationInfo).toString());
                bean.setAppPackageName(p.applicationInfo.packageName);
                File file = new File(p.applicationInfo.sourceDir);
                bean.setAppSize(formatData(file));
                int flags = p.applicationInfo.flags;
                //判断是否是属于系统的apk
                if ((flags & ApplicationInfo.FLAG_SYSTEM) != 0) {
                    bean.setSystem(true);
                }
                appBeanList.add(bean);
            }
        }
        return appBeanList;
    }

    //格式化
    private static String formatData(File file) {
        //这个大小要处理
        float fileSize = (float) file.length() / 1024 / 1024;
        DecimalFormat df = new DecimalFormat("0.00");
        if (fileSize > 1024) {
            float sizes = fileSize / 1024;
            return df.format(sizes) + " G";
        } else {
            return df.format(fileSize) + " MB";
        }
    }

    //获取某个应用的版本号
    public static String getAppVersion(Context mContext, String packageName) {
        try {
            PackageManager manager = mContext.getPackageManager();
            PackageInfo info = manager.getPackageInfo(packageName, 0);
            String version = info.versionName;
            return "版本:" + version;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "无法获取";
    }

    //获取某一个应用的icon
    public static Drawable getAppIcon(Context mContext, String packageName) {
        try {
            PackageManager manager = mContext.getPackageManager();
            PackageInfo info = manager.getPackageInfo(packageName, 0);
            Drawable drawable = info.applicationInfo.loadIcon(manager);
            return drawable;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //获取某个应用的权限
    public static List<String> getAppPermission(Context mContext, String packageName) {
        List<String> mList = new ArrayList<>();
        try {
            PackageManager pm = mContext.getPackageManager();
            PackageInfo pack = pm.getPackageInfo(packageName, PackageManager.GET_PERMISSIONS);
            String[] permissionStrings = pack.requestedPermissions;
            if (permissionStrings != null) {
                for (int i = 0; i < permissionStrings.length; i++) {
                    mList.add(permissionStrings[i]);
                }
            }
            return mList;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return mList;
    }

    //卸载应用
    public static void unInstallApp(Context mContext, String packageName) {
        //通过程序的包名创建URL
        Uri packageURI = Uri.parse("package:" + packageName);
        Intent intent = new Intent(Intent.ACTION_DELETE);
        intent.setData(packageURI);
        mContext.startActivity(intent);
    }

}
