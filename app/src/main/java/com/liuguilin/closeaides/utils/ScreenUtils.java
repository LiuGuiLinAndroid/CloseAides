package com.liuguilin.closeaides.utils;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.WindowManager;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.utils
 *  文件名:   ScreenUtils
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/5 22:46
 *  描述：    屏幕高宽
 */
public class ScreenUtils {

    private DisplayMetrics outMetrics;

    public ScreenUtils(Context mContext) {
        WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        outMetrics = new DisplayMetrics();
        //进行测量
        wm.getDefaultDisplay().getMetrics(outMetrics);
    }

    //获取屏幕宽度
    public int getWidth() {
        return outMetrics.widthPixels;
    }

    //获取屏幕高度
    public int getHeight() {
        return outMetrics.heightPixels;
    }
}
