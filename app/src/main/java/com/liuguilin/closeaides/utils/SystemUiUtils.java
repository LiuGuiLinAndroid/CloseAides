package com.liuguilin.closeaides.utils;

import android.app.Activity;
import android.graphics.Color;
import android.view.View;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.utils
 *  文件名:   SystemUiUtils
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/5 20:05
 *  描述：    沉浸式
 */
public class SystemUiUtils {

    //设置沉浸式状态栏
    public static void showWindowUi(Activity activity) {
        if (VersionUtils.isLollipop()) {
            View decorView = activity.getWindow().getDecorView();
            int option = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
            decorView.setSystemUiVisibility(option);
            activity.getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
    }

}
