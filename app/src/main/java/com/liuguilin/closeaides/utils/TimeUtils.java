package com.liuguilin.closeaides.utils;

import com.liuguilin.closeaides.user.TimeNote;

import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.QueryListener;
import cn.bmob.v3.listener.SaveListener;
import cn.bmob.v3.listener.UpdateListener;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.utils
 *  文件名:   TimeUtils
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/11 19:47
 *  描述：    Bmob数据操作的封装：支持
 *             - 增加数据
 *             - 删除数据
 *             - 查询一条数据
 *             - 条件查询
 *             - 查询全部数据
 *             - 更新数据
 */
public class TimeUtils {

    //添加数据
    public static void Save(TimeNote time, final onSaveListener saveListener) {
        time.save(new SaveListener<String>() {
            @Override
            public void done(String s, BmobException e) {
                if (e == null) {
                    saveListener.onSuccessful(s);
                } else {
                    saveListener.onFailure(e);
                }
            }
        });
    }

    public interface onSaveListener {
        void onSuccessful(String s);

        void onFailure(BmobException e);
    }

    //获取一行数据 获取表中msg这段数据
    public static void Query(String msg, final onQueryListener queryListener) {
        BmobQuery<TimeNote> bmobQuery = new BmobQuery<TimeNote>();
        bmobQuery.getObject(msg, new QueryListener<TimeNote>() {
            @Override
            public void done(TimeNote object, BmobException e) {
                if (e == null) {
                    queryListener.onSuccessful(object);
                } else {
                    queryListener.onFailure(e);
                }
            }
        });
    }

    public interface onQueryListener {
        void onSuccessful(TimeNote object);

        void onFailure(BmobException e);
    }

    //条件查询 查询key为values的字段
    public static void QueryWhere(String key, String values, final onAllQueryListener allQueryListener) {
        BmobQuery<TimeNote> query = new BmobQuery<TimeNote>();
        query.addWhereEqualTo(key, values);
        query.findObjects(new FindListener<TimeNote>() {
            @Override
            public void done(List<TimeNote> list, BmobException e) {
                if (e == null) {
                    allQueryListener.onSuccessful(list);
                } else {
                    allQueryListener.onFailure(e);
                }
            }
        });
    }

    //获取全部的数据
    public static void QueryAll(final onAllQueryListener allQueryListener) {
        BmobQuery<TimeNote> bmobQuery = new BmobQuery<TimeNote>();
        bmobQuery.findObjects(new FindListener<TimeNote>() {
            @Override
            public void done(List<TimeNote> list, BmobException e) {
                if (e == null) {
                    allQueryListener.onSuccessful(list);
                } else {
                    allQueryListener.onFailure(e);
                }
            }
        });
    }

    public interface onAllQueryListener {
        void onSuccessful(List<TimeNote> list);

        void onFailure(BmobException e);
    }

    //修改一行数据 修改表中msg这段数据
    public static void Update(TimeNote time, String msg, final onUpdateListener updateListener) {
        time.update(msg, new UpdateListener() {
            @Override
            public void done(BmobException e) {
                if (e == null) {
                    updateListener.onSuccessful();
                } else {
                    updateListener.onFailure(e);
                }
            }
        });
    }

    //删除数据 删除表中msg这段数据
    public static void Delete(TimeNote time, String msg, final onUpdateListener updateListener) {
        time.delete(new UpdateListener() {
            @Override
            public void done(BmobException e) {
                if (e == null) {
                    updateListener.onSuccessful();
                } else {
                    updateListener.onFailure(e);
                }
            }
        });
    }

    public interface onUpdateListener {
        void onSuccessful();

        void onFailure(BmobException e);
    }

}
