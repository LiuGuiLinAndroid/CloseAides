package com.liuguilin.closeaides.utils;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.utils
 *  文件名:   GlideUtils
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/8 12:31
 *  描述：    图片解析封装
 */
public class GlideUtils {

    //最简单的加载方式
    public static void loadImageView(Context mContext, String url, ImageView imageView) {
        Glide.with(mContext).load(url).into(imageView);
    }

    //加载图片裁剪居中
    public static void loadImageViewCenterCrop(Context mContext, String url, ImageView imageView) {
        Glide.with(mContext).load(url).centerCrop().into(imageView);
    }
}
