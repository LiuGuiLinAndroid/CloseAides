package com.liuguilin.closeaides.utils;

import android.util.Log;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.utils
 *  文件名:   L
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/5 20:07
 *  描述：    封装的Log
 */
public class L {

    //公共的Tag
    public static final String TAG = "CloseAides";
    //调试开关
    public static boolean DEBUG = true;

    public static void i(String log) {
        if (DEBUG) {
            Log.i(TAG, log);
        }
    }

    public static void v(String log) {
        if (DEBUG) {
            Log.v(TAG, log);
        }
    }

    public static void d(String log) {
        if (DEBUG) {
            Log.d(TAG, log);
        }
    }

    public static void w(String log) {
        if (DEBUG) {
            Log.w(TAG, log);
        }
    }

    public static void e(String log) {
        if (DEBUG) {
            Log.e(TAG, log);
        }
    }

}
