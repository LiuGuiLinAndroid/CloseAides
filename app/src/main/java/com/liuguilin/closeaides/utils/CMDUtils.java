package com.liuguilin.closeaides.utils;

import java.io.DataOutputStream;
import java.io.IOException;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.utils
 *  文件名:   CMDUtils
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/21 14:23
 *  描述：    终端权限
 */
public class CMDUtils {

    /**
     * 重启
     *
     * @throws IOException 未获取Root权限
     */
    public static void reboot() throws IOException {
        String cmd = "su -c reboot";
        Runtime.getRuntime().exec(cmd);
    }

    /**
     * 关机
     *
     * @throws IOException 未获取Root权限
     */
    public static void power(int index) throws IOException {
        Process process = Runtime.getRuntime().exec("su");
        DataOutputStream out = new DataOutputStream(process.getOutputStream());
        out.writeBytes("reboot -p\n");
        // 结束
        out.writeBytes("exit\n");
        out.flush();
    }

    /**
     * recovery
     *
     * @throws IOException 未获取Root权限
     */
    public static void recovery() throws IOException {
        Process process = Runtime.getRuntime().exec("su");
        DataOutputStream out = new DataOutputStream(process.getOutputStream());
        out.writeBytes("reboot recovery\n");
        // 结束
        out.writeBytes("exit\n");
        out.flush();
    }
}
