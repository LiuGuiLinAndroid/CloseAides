package com.liuguilin.closeaides.utils;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.os.StatFs;
import android.provider.MediaStore;

import java.io.File;
import java.util.ArrayList;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.utils
 *  文件名:   FileUtils
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/15 09:20
 *  描述：    文件工具类
 */
public class FileUtils {

    // 0：总大小 1：剩余大小
    public static long getSDSize(int index) {
        File path = Environment.getExternalStorageDirectory();
        //系统容量类
        StatFs stat = new StatFs(path.getPath());
        //每块存储块的大小
        long blockSize = stat.getBlockSize();
        if (index == 0) {
            //总存储块的数量
            long totalBlocks = stat.getBlockCount();
            return blockSize * totalBlocks;
        } else if (index == 1) {
            //可用存储块的数量
            long availableBlocks = stat.getAvailableBlocks();
            return blockSize * availableBlocks;
        }
        return 0;
    }

    //0: 圖片 1：音频 2 ： 电影
    public static long getTotalSize(Context mContext, int index) {
        ArrayList<MemoryInfo> resultList = null;
        switch (index) {
            case 0:
                //EXTERNAL_CONTENT_URI
                resultList = queryAllMediaList(mContext, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                break;
            case 1:
                resultList = queryAllMediaList(mContext, MediaStore.Audio.Media.INTERNAL_CONTENT_URI);
                break;
            case 2:
                resultList = queryAllMediaList(mContext, MediaStore.Video.Media.INTERNAL_CONTENT_URI);
                break;
        }
        long size = 0L;
        for (FileUtils.MemoryInfo cInfo : resultList) {
            File file = new File(cInfo.getFilePath());
            if (null != file && file.exists()) {
                size += cInfo.getFileSize();
            }
        }
        return size;
    }

    //外部存储中除音频、视频、图片之前其他文件所占内存
    public static long getOtherTotalSize(Context mContext) {
        long size = getSDSize(0) - getSDSize(1)
                - getTotalSize(mContext, 0) - getTotalSize(mContext, 1)
                - getTotalSize(mContext, 2);
        if (size < 0L) {
            size = 0L;
        }
        return size;
    }

    private static ArrayList<MemoryInfo> queryAllMediaList(Context mContext, Uri uri) {
        //我们只需要两个字段：大小、文件路径
        Cursor cursor = mContext.getContentResolver().query(uri, new String[]{MediaStore.Audio.Media.SIZE, MediaStore.Audio.Media.DATA}, null, null, null);
        ArrayList<MemoryInfo> musicList = new ArrayList<MemoryInfo>();
        try {
            if (cursor.moveToFirst()) {
                do {
                    MemoryInfo mInfo = new MemoryInfo();
                    //大小
                    mInfo.setFileSize(cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media.SIZE)));
                    //路径
                    mInfo.setFilePath(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA)));
                } while (cursor.moveToNext());
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return musicList;
    }

    static class MemoryInfo {
        private long fileSize = 0L;
        private String filePath = "";

        public long getFileSize() {
            return fileSize;
        }

        public void setFileSize(long fileSize) {
            this.fileSize = fileSize;
        }

        public String getFilePath() {
            return filePath;
        }

        public void setFilePath(String filePath) {
            this.filePath = filePath;
        }
    }

}
