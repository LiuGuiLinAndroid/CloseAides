package com.liuguilin.closeaides.utils;

import android.os.Handler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.utils
 *  文件名:   OkHttpUtils
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/8 09:52
 *  描述：    OkHttp封装
 */
public class OkHttpUtils {

    private static Handler mHandler = new Handler();
    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    //get
    public static void get(String url, final onHttpCallback callback) {
        OkHttpClient okHttpClient = new OkHttpClient();
        Request request = new Request.Builder().url(url).get().build();
        Call call = okHttpClient.newCall(request);
        //异步操作
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                callback.onFailure(call, e);
            }

            @Override
            public void onResponse(final Call call, final Response response) throws IOException {
                if (response.isSuccessful()) {
                    final String result = response.body().string();
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResponse(result);
                        }
                    });
                } else {
                    callback.onFailure("isSuccessful = false");
                }
            }
        });
    }

    //post json
    public static void postJson(String url, String json, final onHttpCallback callback) {
        OkHttpClient okHttpClient = new OkHttpClient();
        RequestBody body = RequestBody.create(JSON, json);
        final Request request = new Request.Builder().url(url).post(body).build();
        Call call = okHttpClient.newCall(request);
        //异步操作
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                callback.onFailure(call, e);
            }

            @Override
            public void onResponse(final Call call, final Response response) throws IOException {
                if (response.isSuccessful()) {
                    final String result = request.body().toString();
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResponse(result);
                        }
                    });
                } else {
                    callback.onFailure("isSuccessful = false");
                }
            }
        });
    }

    //post map
    public static void postMap(String url, Map<String, String> params, final onHttpCallback callback) {
        OkHttpClient okHttpClient = new OkHttpClient();
        FormBody.Builder from = new FormBody.Builder();
        //键值对不为空，他的值也不为空
        if (params != null && !params.isEmpty()) {
            for (Map.Entry<String, String> entry : params.entrySet()) {
                //装载表单值
                from.add(entry.getKey(), entry.getValue());
            }
        }
        RequestBody body = from.build();
        Request request = new Request.Builder().url(url).post(body).build();
        Call call = okHttpClient.newCall(request);
        //异步操作
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                callback.onFailure(call, e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    final String result = response.body().string();
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResponse(result);
                        }
                    });
                } else {
                    callback.onFailure("isSuccessful = false");
                }
            }
        });
    }

    //下载 url 结果  进度 存放的地址
    public static void download(final String path, String url, final onHttpCallback callback, final ProgressResponseListener responseListener) {
        OkHttpClient okHttpClient = new OkHttpClient();
        Request request = new Request.Builder().url(url).build();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                callback.onFailure(call, e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    try {
                        InputStream is = response.body().byteStream();
                        long total = response.body().contentLength();
                        File file = new File(path);
                        FileOutputStream fos = new FileOutputStream(file);
                        byte[] buf = new byte[2048];
                        long sum = 0;
                        int len = 0;
                        while ((len = is.read(buf)) != -1) {
                            fos.write(buf, 0, len);
                            sum += len;
                            int progress = (int) (sum * 1.0f / total * 100);
                            responseListener.onResponseProgress(progress, total);
                        }
                        fos.flush();
                        callback.onResponse("done");
                    } catch (IOException e) {
                        callback.onFailure(call, e);
                    }
                } else {
                    callback.onFailure("isSuccessful = false");
                }
            }
        });
    }

    public interface onHttpCallback {

        void onResponse(String result);

        void onFailure(Call call, IOException e);

        void onFailure(String msg);
    }

    public interface ProgressResponseListener {
        void onResponseProgress(long bytesRead, long contentLength);
    }
}
