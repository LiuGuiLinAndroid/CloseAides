package com.liuguilin.closeaides.impl;

import com.liuguilin.closeaides.model.LifeModel;
import com.liuguilin.closeaides.model.NowWeatherModel;
import com.liuguilin.closeaides.model.WeekWeatherModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.impl
 *  文件名:   WeatherApi
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/20 9:44
 *  描述：    天气的Api
 */
public interface WeatherApi {

    //今日天气
    @GET("now?")
    Call<NowWeatherModel> getNowWeather(@Query("city") String city, @Query("key") String key);

    //三天天气
    @GET("forecast?")
    Call<WeekWeatherModel> getWeekWeather(@Query("city") String city, @Query("key") String key);

    //生活指数
    @GET("suggestion?")
    Call<LifeModel> getLifeIndex(@Query("city") String city, @Query("key") String key);

}
