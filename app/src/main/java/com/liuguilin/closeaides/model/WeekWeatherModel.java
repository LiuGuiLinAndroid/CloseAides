package com.liuguilin.closeaides.model;

import java.util.List;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.model
 *  文件名:   WeekWeatherModel
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/20 10:33
 *  描述：    TODO
 */
public class WeekWeatherModel {


    private List<HeWeather5Bean> HeWeather5;

    public List<HeWeather5Bean> getHeWeather5() {
        return HeWeather5;
    }

    public void setHeWeather5(List<HeWeather5Bean> HeWeather5) {
        this.HeWeather5 = HeWeather5;
    }

    public static class HeWeather5Bean {
        /**
         * basic : {"city":"北京","cnty":"中国","id":"CN101010100","lat":"39.904989","lon":"116.405285","update":{"loc":"2017-04-20 09:51","utc":"2017-04-20 01:51"}}
         * daily_forecast : [{"astro":{"mr":"01:44","ms":"12:07","sr":"05:30","ss":"18:57"},"cond":{"code_d":"101","code_n":"101","txt_d":"多云","txt_n":"多云"},"date":"2017-04-20","hum":"22","pcpn":"0.0","pop":"2","pres":"1010","tmp":{"max":"22","min":"10"},"uv":"5","vis":"20","wind":{"deg":"275","dir":"北风","sc":"微风","spd":"3"}},{"astro":{"mr":"02:24","ms":"13:07","sr":"05:28","ss":"18:58"},"cond":{"code_d":"101","code_n":"100","txt_d":"多云","txt_n":"晴"},"date":"2017-04-21","hum":"29","pcpn":"0.0","pop":"7","pres":"1013","tmp":{"max":"22","min":"8"},"uv":"6","vis":"19","wind":{"deg":"320","dir":"南风","sc":"微风","spd":"0"}},{"astro":{"mr":"03:01","ms":"14:10","sr":"05:27","ss":"18:59"},"cond":{"code_d":"101","code_n":"100","txt_d":"多云","txt_n":"晴"},"date":"2017-04-22","hum":"21","pcpn":"0.0","pop":"1","pres":"1008","tmp":{"max":"27","min":"13"},"uv":"6","vis":"20","wind":{"deg":"256","dir":"南风","sc":"微风","spd":"5"}},{"astro":{"mr":"03:37","ms":"15:15","sr":"05:26","ss":"19:00"},"cond":{"code_d":"100","code_n":"100","txt_d":"晴","txt_n":"晴"},"date":"2017-04-23","hum":"23","pcpn":"0.0","pop":"0","pres":"1011","tmp":{"max":"24","min":"12"},"uv":"6","vis":"20","wind":{"deg":"260","dir":"南风","sc":"微风","spd":"0"}},{"astro":{"mr":"04:12","ms":"16:23","sr":"05:24","ss":"19:01"},"cond":{"code_d":"101","code_n":"104","txt_d":"多云","txt_n":"阴"},"date":"2017-04-24","hum":"28","pcpn":"0.0","pop":"0","pres":"1012","tmp":{"max":"25","min":"13"},"uv":"5","vis":"19","wind":{"deg":"184","dir":"南风","sc":"微风","spd":"0"}},{"astro":{"mr":"04:47","ms":"17:34","sr":"05:23","ss":"19:02"},"cond":{"code_d":"104","code_n":"100","txt_d":"阴","txt_n":"晴"},"date":"2017-04-25","hum":"34","pcpn":"0.0","pop":"4","pres":"1013","tmp":{"max":"20","min":"8"},"uv":"5","vis":"19","wind":{"deg":"195","dir":"北风","sc":"微风","spd":"7"}},{"astro":{"mr":"05:24","ms":"18:45","sr":"05:21","ss":"19:03"},"cond":{"code_d":"101","code_n":"104","txt_d":"多云","txt_n":"阴"},"date":"2017-04-26","hum":"38","pcpn":"7.6","pop":"68","pres":"1011","tmp":{"max":"21","min":"10"},"uv":"4","vis":"18","wind":{"deg":"256","dir":"南风","sc":"微风","spd":"3"}}]
         * status : ok
         */

        private BasicBean basic;
        private String status;
        private List<DailyForecastBean> daily_forecast;

        public BasicBean getBasic() {
            return basic;
        }

        public void setBasic(BasicBean basic) {
            this.basic = basic;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public List<DailyForecastBean> getDaily_forecast() {
            return daily_forecast;
        }

        public void setDaily_forecast(List<DailyForecastBean> daily_forecast) {
            this.daily_forecast = daily_forecast;
        }

        public static class BasicBean {
            /**
             * city : 北京
             * cnty : 中国
             * id : CN101010100
             * lat : 39.904989
             * lon : 116.405285
             * update : {"loc":"2017-04-20 09:51","utc":"2017-04-20 01:51"}
             */

            private String city;
            private String cnty;
            private String id;
            private String lat;
            private String lon;
            private UpdateBean update;

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public String getCnty() {
                return cnty;
            }

            public void setCnty(String cnty) {
                this.cnty = cnty;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getLat() {
                return lat;
            }

            public void setLat(String lat) {
                this.lat = lat;
            }

            public String getLon() {
                return lon;
            }

            public void setLon(String lon) {
                this.lon = lon;
            }

            public UpdateBean getUpdate() {
                return update;
            }

            public void setUpdate(UpdateBean update) {
                this.update = update;
            }

            public static class UpdateBean {
                /**
                 * loc : 2017-04-20 09:51
                 * utc : 2017-04-20 01:51
                 */

                private String loc;
                private String utc;

                public String getLoc() {
                    return loc;
                }

                public void setLoc(String loc) {
                    this.loc = loc;
                }

                public String getUtc() {
                    return utc;
                }

                public void setUtc(String utc) {
                    this.utc = utc;
                }
            }
        }

        public static class DailyForecastBean {
            /**
             * astro : {"mr":"01:44","ms":"12:07","sr":"05:30","ss":"18:57"}
             * cond : {"code_d":"101","code_n":"101","txt_d":"多云","txt_n":"多云"}
             * date : 2017-04-20
             * hum : 22
             * pcpn : 0.0
             * pop : 2
             * pres : 1010
             * tmp : {"max":"22","min":"10"}
             * uv : 5
             * vis : 20
             * wind : {"deg":"275","dir":"北风","sc":"微风","spd":"3"}
             */

            private AstroBean astro;
            private CondBean cond;
            private String date;
            private String hum;
            private String pcpn;
            private String pop;
            private String pres;
            private TmpBean tmp;
            private String uv;
            private String vis;
            private WindBean wind;

            public AstroBean getAstro() {
                return astro;
            }

            public void setAstro(AstroBean astro) {
                this.astro = astro;
            }

            public CondBean getCond() {
                return cond;
            }

            public void setCond(CondBean cond) {
                this.cond = cond;
            }

            public String getDate() {
                return date;
            }

            public void setDate(String date) {
                this.date = date;
            }

            public String getHum() {
                return hum;
            }

            public void setHum(String hum) {
                this.hum = hum;
            }

            public String getPcpn() {
                return pcpn;
            }

            public void setPcpn(String pcpn) {
                this.pcpn = pcpn;
            }

            public String getPop() {
                return pop;
            }

            public void setPop(String pop) {
                this.pop = pop;
            }

            public String getPres() {
                return pres;
            }

            public void setPres(String pres) {
                this.pres = pres;
            }

            public TmpBean getTmp() {
                return tmp;
            }

            public void setTmp(TmpBean tmp) {
                this.tmp = tmp;
            }

            public String getUv() {
                return uv;
            }

            public void setUv(String uv) {
                this.uv = uv;
            }

            public String getVis() {
                return vis;
            }

            public void setVis(String vis) {
                this.vis = vis;
            }

            public WindBean getWind() {
                return wind;
            }

            public void setWind(WindBean wind) {
                this.wind = wind;
            }

            public static class AstroBean {
                /**
                 * mr : 01:44
                 * ms : 12:07
                 * sr : 05:30
                 * ss : 18:57
                 */

                private String mr;
                private String ms;
                private String sr;
                private String ss;

                public String getMr() {
                    return mr;
                }

                public void setMr(String mr) {
                    this.mr = mr;
                }

                public String getMs() {
                    return ms;
                }

                public void setMs(String ms) {
                    this.ms = ms;
                }

                public String getSr() {
                    return sr;
                }

                public void setSr(String sr) {
                    this.sr = sr;
                }

                public String getSs() {
                    return ss;
                }

                public void setSs(String ss) {
                    this.ss = ss;
                }
            }

            public static class CondBean {
                /**
                 * code_d : 101
                 * code_n : 101
                 * txt_d : 多云
                 * txt_n : 多云
                 */

                private String code_d;
                private String code_n;
                private String txt_d;
                private String txt_n;

                public String getCode_d() {
                    return code_d;
                }

                public void setCode_d(String code_d) {
                    this.code_d = code_d;
                }

                public String getCode_n() {
                    return code_n;
                }

                public void setCode_n(String code_n) {
                    this.code_n = code_n;
                }

                public String getTxt_d() {
                    return txt_d;
                }

                public void setTxt_d(String txt_d) {
                    this.txt_d = txt_d;
                }

                public String getTxt_n() {
                    return txt_n;
                }

                public void setTxt_n(String txt_n) {
                    this.txt_n = txt_n;
                }
            }

            public static class TmpBean {
                /**
                 * max : 22
                 * min : 10
                 */

                private String max;
                private String min;

                public String getMax() {
                    return max;
                }

                public void setMax(String max) {
                    this.max = max;
                }

                public String getMin() {
                    return min;
                }

                public void setMin(String min) {
                    this.min = min;
                }
            }

            public static class WindBean {
                /**
                 * deg : 275
                 * dir : 北风
                 * sc : 微风
                 * spd : 3
                 */

                private String deg;
                private String dir;
                private String sc;
                private String spd;

                public String getDeg() {
                    return deg;
                }

                public void setDeg(String deg) {
                    this.deg = deg;
                }

                public String getDir() {
                    return dir;
                }

                public void setDir(String dir) {
                    this.dir = dir;
                }

                public String getSc() {
                    return sc;
                }

                public void setSc(String sc) {
                    this.sc = sc;
                }

                public String getSpd() {
                    return spd;
                }

                public void setSpd(String spd) {
                    this.spd = spd;
                }
            }
        }
    }
}
