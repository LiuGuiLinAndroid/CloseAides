package com.liuguilin.closeaides.model;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.model
 *  文件名:   TimeModel
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/17 19:35
 *  描述：    时光的数据
 */
public class TimeModel {

    //标题
    private String title;
    //时间
    private String time;
    //颜色
    private String color;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
