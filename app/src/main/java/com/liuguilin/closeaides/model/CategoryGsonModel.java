package com.liuguilin.closeaides.model;

import java.util.List;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.model
 *  文件名:   CategoryGsonModel
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/18 9:52
 *  描述：    TODO
 */
public class CategoryGsonModel {


    /**
     * link : {"prev":"","next":"http://open.lovebizhi.com/bdrom/category?code=6ghDtD1YckBHFzRXay%29KVDIQaMgaAqRnYHdljrPBhmarsQ2XK4FlzLcSqZiyaUuAJL%7Cr3Mwc2XsoCF2Sh6lxnQ"}
     * data : [{"key":"231204-100","small":"http://s.qdcdn.com/cl/12825369,240,426.jpg","big":"http://s.qdcdn.com/cl/12825369,720,1280.jpg","down":2210467,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=m66z0XL5fzexho%29fNKYp409hAKN6y3Ptb9wlVieNk1HXwLJkD%7CkWEp%7Cgqzbn778J56l50SP1D9f8lddle7pZNI6q3tCTho%7Cvy2Oa4Q"},{"key":"265777-100","small":"http://s.qdcdn.com/cl/13237024,240,426.jpg","big":"http://s.qdcdn.com/cl/13237024,720,1280.jpg","down":1284312,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=3azgGyKdZFLTUuH5Ho4qhRLRHgHH9r6T4gdFlpI82Er9Y2DFDM4XRzh4cxHPRzAY5PCTVVqqnygzgb%7CLtJ9oAhSAnedd7tDb50i%7CHg"},{"key":"177981-100","small":"http://s.qdcdn.com/cl/12165079,240,426.jpg","big":"http://s.qdcdn.com/cl/12165079,720,1280.jpg","down":3574582,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=zUTBTLph7odzP6%7CfN1VJS%7CKssN9LOCeTqXwZOl3dq3eUGDq57Xw5xL9ccU2Mxr17Tet2CcqIydFR7ooy%7CuxXg%7CNnlPg7Zk0SIqhsMQ"},{"key":"304945-100","small":"http://s.qdcdn.com/cl/13717273,240,426.jpg","big":"http://s.qdcdn.com/cl/13717273,720,1280.jpg","down":546515,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=uUOiOIRkSDpCZUl16iUT8952%296kHrLowkILWv0aNWZNHb79LgfVGRU8PeI7UHsbzb%7CucTHVwezGZkDAm4H9z90JxVTdIk1cFYr9NbQ"},{"key":"299471-100","small":"http://s.qdcdn.com/cl/13650852,240,426.jpg","big":"http://s.qdcdn.com/cl/13650852,720,1280.jpg","down":27505,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=1FOAav9PxB5dElRzSaiaivjFkEURcJOAoFJcsHa4Utv7Q0Jyjx1D0weDKELTeOfupytuB5zmHlpJFCQvI6t9IxH7duL0T0hlHggcqg"},{"key":"304939-100","small":"http://s.qdcdn.com/cl/13717238,240,426.jpg","big":"http://s.qdcdn.com/cl/13717238,720,1280.jpg","down":160974,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=bTRoBSR5ju1bv4v%29P5Tha9XTy6YyGu1C0jWqPZvFriTn17m7piOhmmTi2USH3ByIuWkMTS%7C0jgWqZNcpDo2OWZRTqeYAngqi01GHaA"},{"key":"207074-100","small":"http://s.qdcdn.com/cl/12572027,240,426.jpg","big":"http://s.qdcdn.com/cl/12572027,720,1280.jpg","down":261627,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=GuXL2fmd4S%29o59WmtBQpJwsQxXWCaPCTo8hye1EEMcpC7%29eSvbw%7Ct5MKn%7Cb%7C%7CyTPuEJmmFM%29O2XT%290rmWNRLaNcIz1BP7bv4SgtC6w"},{"key":"305067-100","small":"http://s.qdcdn.com/cl/13718157,240,426.jpg","big":"http://s.qdcdn.com/cl/13718157,720,1280.jpg","down":154924,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=%7CtGX6%7CPv2kmNIMmIc%7CicqZpHCots7%7CE8nYJLvWljl0G1PS6frdnMwWMxHd3TGnZ17u89KFPHdW5A4aZxJcdDwgVmLFcqEKsFouoblw"},{"key":"265756-100","small":"http://s.qdcdn.com/cl/13236877,240,426.jpg","big":"http://s.qdcdn.com/cl/13236877,720,1280.jpg","down":321331,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=aqImz0aAxNHy2f%7CRvGi7EufrwIUHjNvaFMBiz1LEk10eRnm6J6vqyCzJGvZTQOw8l%7CzUCda%29qig0P4LA5ahQi9uTVyoIE7pu7eWVEA"},{"key":"299209-100","small":"http://s.qdcdn.com/cl/13648586,240,426.jpg","big":"http://s.qdcdn.com/cl/13648586,720,1280.jpg","down":123060,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=9uwGnflbL1MUIM3j8FrqMgA6%29DKjx%7CPMFlfsN4x6YGUQIOO8VvU8m%29b8H%7C0a5HJXYp6xCGnv1Sa%7Cq%7C2MgRsnQDPni%7CcVQsaKx8BGhQ"},{"key":"265731-100","small":"http://s.qdcdn.com/cl/13236702,240,426.jpg","big":"http://s.qdcdn.com/cl/13236702,720,1280.jpg","down":55542,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=jDQQ%29U%29pV%29fB%7CdqO%29CO1BQhFLEbp%29VwpK5UgxUM%7Cz71WYTgHW7TvFGOPdaCHaAFrKqoKuL721%29cz9rUkEOPvHyQMIrlnIhMzenc4IA"},{"key":"91219-100","small":"http://s.qdcdn.com/cl/10975756,240,426.jpg","big":"http://s.qdcdn.com/cl/10975756,720,1280.jpg","down":417133,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=0u1s9jbEZWHRD1o%7CuCBR8xkjp9wp%29yllGk%29Li2dVq7ig8MT%29F%7CbHJyLHpNK%7CNIzEzzbtlAOPQrVruq92yPlnbhTRNuEpGp2wrp9ppw"},{"key":"305164-100","small":"http://s.qdcdn.com/cl/13718840,240,426.jpg","big":"http://s.qdcdn.com/cl/13718840,720,1280.jpg","down":85122,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=p9O1YJtnuOx1uffOh%29PIlg4mfPXz1HT5NN5U0ZmLedPxvynakhgAbYSpCKPMJA1vTV94pPtFz59cHd06uB9NeIU90czYQnTzXvEFCQ"},{"key":"302591-100","small":"http://s.qdcdn.com/cl/13690420,240,426.jpg","big":"http://s.qdcdn.com/cl/13690420,720,1280.jpg","down":37481,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=EgXtJFd%7CkJJb0urQ49QS89kx7mFQi5LbTbE85BNaKb8y7HxVvHzPv0OAwD5bnwcJh6R81pPbS4Utk1rmwNqh98NfcbIFG3L2bpKZ%29g"},{"key":"340376-100","small":"http://s.qdcdn.com/cl/14223000,240,426.jpg","big":"http://s.qdcdn.com/cl/14223000,720,1280.jpg","down":46061,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=QZMs7ANSL2HnD%7CWMimFnVEklFWWnLg3YrY2VtMF3h3FTrWSb2uk4VpRNUIoL29y2YzZfgGGQ1lSZkPvMYTbHjwGNN%29f7om91SoFKCQ"},{"key":"319056-100","small":"http://s.qdcdn.com/cl/13876560,240,426.jpg","big":"http://s.qdcdn.com/cl/13876560,720,1280.jpg","down":18135,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=750AG9CisI8XpeP%7CZwH6Xdgo5Cmpzy8x3qv1TltFOhgUDrvQ0Sy3sA3FyiIa%7CdohpNr99RsVMg05UZZl1yDi9%29gqjfc9G2PuMAV%7CUg"},{"key":"88817-100","small":"http://s.qdcdn.com/cl/10911974,240,426.jpg","big":"http://s.qdcdn.com/cl/10911974,720,1280.jpg","down":902734,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=SzNidC9K0mjn6LyEU5zP5goH0xxRBg2F9xD%29%7CTHpnTDG3bW0erA0gyObd4N1w3mFvcYmhba3sthxxemVyJg1a7cUkggz2%29rQ8VaPyw"},{"key":"305165-100","small":"http://s.qdcdn.com/cl/13718847,240,426.jpg","big":"http://s.qdcdn.com/cl/13718847,720,1280.jpg","down":88493,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=5S2baa9B5B1htVLs2szpegp4hV2Vd%7CCcG0iamwtsHigGlPXOqay5Au8ck%29JcowRGSavlsUektLpIJn1v%7C3TFHdl0lUVQbuz5uwdWOA"},{"key":"51397-100","small":"http://s.qdcdn.com/cl/10359190,240,426.jpg","big":"http://s.qdcdn.com/cl/10359190,720,1280.jpg","down":83987,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=kdyUX1xLFW6pmy8f3MxK4mHgns5D0R7qYkp75Msvq6SyGa3kbhsPKCnxqSNlYIhdWWp%290nsTWKKbS6u8sbYFDPtMVr%7CmNFYlr4ofdQ"},{"key":"89038-100","small":"http://s.qdcdn.com/cl/10919137,240,426.jpg","big":"http://s.qdcdn.com/cl/10919137,720,1280.jpg","down":292232,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=PuJC0WXgUtoK2Xj03gDEt%7ClF4F6qCQsaI1RAJNXrjiQopfcHSMhmKRuyzWkvS8NlFw%7CjG3o3cYoaUZ5%29SZ%7CE0c1PWMa52Y8xqoAGjQ"},{"key":"135787-100","small":"http://s.qdcdn.com/cl/11727296,240,426.jpg","big":"http://s.qdcdn.com/cl/11727296,720,1280.jpg","down":7667111,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=RHetC8dIBOHdRmyvxfUtsX10360ockk6471fzpj%7Cp268R%29flOi0ERjHffwwjWGOAtNb4zj5VWrYcA1Dpj2b7L2lqBI4GFkNY3CZcRg"},{"key":"205965-100","small":"http://s.qdcdn.com/cl/12556730,240,426.jpg","big":"http://s.qdcdn.com/cl/12556730,720,1280.jpg","down":149270,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=5Vyjcvcdi%29gJWRznAvQrHPkA4TePcJpaww8EbzNQAHGx1HEcZ4dhQMc8dR52aD%7CJvdNKuplRnRXBKwTwLfJ48qoxDjogxqNE%7C7wN2Q"},{"key":"135816-100","small":"http://s.qdcdn.com/cl/11727513,240,426.jpg","big":"http://s.qdcdn.com/cl/11727513,720,1280.jpg","down":1277547,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=MHOzL4osu4Q6nD2KdhTyzg17QYRiP0hxQc9DlgRFyk3SLCaHTu1fuNUiueC8BzsjUbA9hk0HSoukFO3vWVWCxLLp4eD8wYH%7CvGAfrg"},{"key":"25430-100","small":"http://s.qdcdn.com/cl/10191752,240,426.jpg","big":"http://s.qdcdn.com/cl/10191752,720,1280.jpg","down":555786,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=3n4xGdxtCI0E97YrqurbGO8HyrrdtivBXmq4N9zvUM1ldTgVBoeOCsCXiDnRUf1%297pEvvKPM%7CxlUoH3dmhq82tblpuVcuO8oIdTY3w"},{"key":"265769-100","small":"http://s.qdcdn.com/cl/13236968,240,426.jpg","big":"http://s.qdcdn.com/cl/13236968,720,1280.jpg","down":134073,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=qHkATlpQbLzXlsXyTOnHst3gR1sQ%7CA5efduMGDGUzmNO0X4Mf7sCeEoZxoZQGax2NGo6hpoylnmTjPdhTc7BLJ1bKU79MMuojMG3dQ"},{"key":"330536-100","small":"http://s.qdcdn.com/cl/14012070,240,426.jpg","big":"http://s.qdcdn.com/cl/14012070,720,1280.jpg","down":85345,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=6PGdXOYDZUeSsbBGyvvBx2G83Hzm4uaBqxhpsogHN9wxCiu%7CGD3g2JX6wUsavnEINB9TOPbWbtGumyFsFEKC53Sp4ZFdRGVf11jLDg"},{"key":"25978-100","small":"http://s.qdcdn.com/cl/10194333,240,426.jpg","big":"http://s.qdcdn.com/cl/10194333,720,1280.jpg","down":856834,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=QN1HFlIhsXWCvgVlQvOVvHidQQpjULRZwe%7Cdc%7CYeC6g%7CB2mYQfbyn5L7tRiqcQAgDvDGLE0msZOKIpDSrchU3hTm7mEHhZtnAD4b2Q"},{"key":"73140-100","small":"http://s.qdcdn.com/cl/10619265,240,426.jpg","big":"http://s.qdcdn.com/cl/10619265,720,1280.jpg","down":668662,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=LpOMZw%7C1ikjFMITDOzRvnf%7Cs%293rNLM0kgkOvtD0xFcWZS%293SfAvNAgOYs9XGBrtSEEAi2QKLrmW6NMd7IFTPrvzEmnBwYFP3FBiH4w"},{"key":"127611-100","small":"http://s.qdcdn.com/cl/11626230,240,426.jpg","big":"http://s.qdcdn.com/cl/11626230,720,1280.jpg","down":923796,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=sbb%29IiRASqfwS2cUJ%29UEZmkm9WUL7DwhL2Pcl%7CeXRbbgpr%29dxs%290HTl5uVjShoc8twADzwPazA%7C%7Cm%29SuSTWxlzbeFyqHW3lKCs2Fbg"},{"key":"338940-100","small":"http://s.qdcdn.com/cl/14191250,240,426.jpg","big":"http://s.qdcdn.com/cl/14191250,720,1280.jpg","down":17814,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=wL14Sz%7CPezgBbgizipvoJ10TnwnLk%297FVUaUrZjTMRilbHNVVgPnTjVvjmHlkbE8dhy1C0AjRH4IFiqrGia0VYowPJxuOU1xQtR68Q"},{"key":"228810-100","small":"http://s.qdcdn.com/cl/12793031,240,426.jpg","big":"http://s.qdcdn.com/cl/12793031,720,1280.jpg","down":470651,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=SRe%29wyZlWZFz1Uxg6tYneqozqhA23QTqIxC7AL3PwBIQxuBRdcCZXpOlctzsgS4UDLUY3UkmRbExnRAxNay3v7v%29B%7CUvnHQMS7k1%7CQ"},{"key":"330537-100","small":"http://s.qdcdn.com/cl/14012077,240,426.jpg","big":"http://s.qdcdn.com/cl/14012077,720,1280.jpg","down":96061,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=K2lGhlJFONKwdal3IonilG25Wbd6xyJl%29HybGO6M2hc30Z2J1tX1hqJMK8LdXhDgzvwhoeUr%29U1PSkakyDyGqlyilIZ9myeTKbuavg"},{"key":"13600-100","small":"http://s.qdcdn.com/cl/10170248,240,426.jpg","big":"http://s.qdcdn.com/cl/10170248,720,1280.jpg","down":89641,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=lk0pcZMGL8i5Pr3Ogkcv83njpaeszm0ILUIFH0JsdNj69Rqh0dmytQYFEAV4hf1TkRi268mll8rYUNG%29ou8cyMXsnKQtSGUGvl1eQA"},{"key":"304936-100","small":"http://s.qdcdn.com/cl/13717217,240,426.jpg","big":"http://s.qdcdn.com/cl/13717217,720,1280.jpg","down":55579,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=FyGnFrvq%29ba46fVIpbJRPkV5KQIQ6PP4LNCdQdo6i9vxE6Xzr4t1Scs6AL%7CxlQEG1iq1UUFcIaw95H%7CQcc%29nNB%29sdFYJe2WWdQfR%29A"},{"key":"330749-100","small":"http://s.qdcdn.com/cl/14014275,240,426.jpg","big":"http://s.qdcdn.com/cl/14014275,720,1280.jpg","down":21172,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=DDh%7CKhoVOyyCB5tt8AAVEyjGEquYC8a1EQ9FydZS4FJP8wlvd2DvOj%29dJn9GnfVN77%29CVV7ZbmKLUdv3C3RMi2zlZerZYp5c4U1Diw"},{"key":"268405-100","small":"http://s.qdcdn.com/cl/13266803,240,426.jpg","big":"http://s.qdcdn.com/cl/13266803,720,1280.jpg","down":35530,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=PZ42T%29AmNus9wgzB4%7CJvLmDTG6r3Lk3JpBugbsnx8gt1Jk6wxcMM4ZnVD3oG%7C%29qnVa863srWuvnXFJR5n%7C7LMFaqoOlGx2k3mIyZiA"},{"key":"205225-100","small":"http://s.qdcdn.com/cl/12548365,240,426.jpg","big":"http://s.qdcdn.com/cl/12548365,720,1280.jpg","down":20135,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=gVk0VVVDRPN2C%7CDDvPj%29sx2JkraXEIQUH62QXwS5ZEcnfK%7C%7CqCCiVq8wOyFU1g%7CdMdWm7q4%7CkQOmT%7Crh7FFAIhOjJHcZ9nF%7Ce72MBg"},{"key":"70064-100","small":"http://s.qdcdn.com/cl/10574387,240,426.jpg","big":"http://s.qdcdn.com/cl/10574387,720,1280.jpg","down":239615,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=V3cAjOpqcrBMI2EMIsp%7CExpIr6Ytdc3xsxki%29W8i3NsUhEfzsjmGCC2xDoOolGv94jWdYgU7GvGmld7Q6NZDsBfvBvoZuFHz2C2QZA"},{"key":"17127-100","small":"http://s.qdcdn.com/cl/10036817,240,426.jpg","big":"http://s.qdcdn.com/cl/10036817,720,1280.jpg","down":322763,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=ye4NtvOg13wASKPbWaJU3FAMo%29WuuAY2fWcT8bq%29pfSoA5teFkZZ2IcRf3FMQH54BaGSKP4wM2eLq7MOmB1JZA%29R1xwbAVrXpi5v6A"},{"key":"305156-100","small":"http://s.qdcdn.com/cl/13718784,240,426.jpg","big":"http://s.qdcdn.com/cl/13718784,720,1280.jpg","down":195504,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=Ti0CmBMB3lCQoogOpUy%290qgXlzjKFwUyZO3rcFzuuGeQocM4QG0aoqMAeiRTLGDyiKOIBsjKanomi0tKzvSKF6V2%29gz8v8R4VY2fkQ"},{"key":"127627-100","small":"http://s.qdcdn.com/cl/11626410,240,426.jpg","big":"http://s.qdcdn.com/cl/11626410,720,1280.jpg","down":505280,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=Pti2gkB%29bJHiVDUMrcVOavgi%292SLM%7CoAos8trr2pM%299iCJDVe%7CLH1GlVFBN%2954BDglheWLWrKbi1L7HGssoizMO%7CpzrwSYTgbNkA9A"},{"key":"329740-100","small":"http://s.qdcdn.com/cl/13999447,240,426.jpg","big":"http://s.qdcdn.com/cl/13999447,720,1280.jpg","down":102777,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=%7CTIh8xvbWBfhWpPKpCeXMLFdpOR%29KpeF6cvQO3O1FORXlkNJF5dtViRam7nSQuzaAygYNSE%7CESeGdXwx3DxyCdKTUzXBeCTm1lgODw"},{"key":"207542-100","small":"http://s.qdcdn.com/cl/12578193,240,426.jpg","big":"http://s.qdcdn.com/cl/12578193,720,1280.jpg","down":751624,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=fUsl%29EXpuP%29756jVgpN36ysZMIQ5kfRYrsmPhNuph%29k1X5Ry7Q%7CBrLab2CoR%7ChUG%7Cobk3MdXDsAMc2Kol2j2OYgLf4rdwr%29aL96K8g"},{"key":"305163-100","small":"http://s.qdcdn.com/cl/13718833,240,426.jpg","big":"http://s.qdcdn.com/cl/13718833,720,1280.jpg","down":118826,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=5TnVPeEYYKgAxB5r%29QMCZkPrYkm%29Hx47FZFgCcEOseZ%7CABOW2NUjApRIOuSatr%29PtMuwzYZMKjoaSHnEf%7CbGWLsJrInFyvPSGJvsTw"},{"key":"76295-100","small":"http://s.qdcdn.com/cl/10671255,240,426.jpg","big":"http://s.qdcdn.com/cl/10671255,720,1280.jpg","down":3966826,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=5ZDIsf1TeI8M0GnqjFsLYwKP%29kdwZm9Z%29Pr1oj2iUuQy%29QF0%29W6xJW%7CGi2ho7r4%29Fa4wIS8qvOkQhRuGoi71tT5BKJ5Ma2jwVMdQNA"},{"key":"45740-100","small":"http://s.qdcdn.com/cl/10319775,240,426.jpg","big":"http://s.qdcdn.com/cl/10319775,720,1280.jpg","down":125900,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=4M%29wiz5oCy4zMgtekN5aAV0TIexwaGq53e5QZQAsD4glHXJndBvq2pi1FAQdMkk7oVSOFGnS9t1NIfIWKw8jvXYzaQ34%7CyENg5Seiw"},{"key":"15596-100","small":"http://s.qdcdn.com/cl/10045544,240,426.jpg","big":"http://s.qdcdn.com/cl/10045544,720,1280.jpg","down":594811,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=%7Cv%296p6DOxELd2ZXwyBx0%292IRWe09UTUViNEjZz4t5AVH%29HIEStPJ2jWyJQYhqLGbRoQjIT1YWD9TrTNTOLH8Fd2FpHFLpRd%7CzaNqBA"},{"key":"197971-100","small":"http://s.qdcdn.com/cl/12447846,240,426.jpg","big":"http://s.qdcdn.com/cl/12447846,720,1280.jpg","down":32431,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=QzlAUmoJHd5oCvUp3KW7uM%7C4lkI0mAiBKS%7C2wa6e7X1cFd5EtmCYdx3UUCRo6WfxeLm7zXpL0f1tpweXqg%29udgZD1u6bIu44GuU%7C0Q"},{"key":"268370-100","small":"http://s.qdcdn.com/cl/13266563,240,426.jpg","big":"http://s.qdcdn.com/cl/13266563,720,1280.jpg","down":39024,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=W7BqLxNMIp4nKidtGTrCZiPAARTMMB%29h%7CvWGxvh8pF41vsTUVr3e4Qdx%7CPWqdoQeSWyOSIDWrkIOgRHCY5duBCfpSG%7Cirg1h8oK9TQ"},{"key":"73701-100","small":"http://s.qdcdn.com/cl/10626432,240,426.jpg","big":"http://s.qdcdn.com/cl/10626432,720,1280.jpg","down":2065863,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=qIzB7d44mDWNcgQeLxd3D4WIBIpPUTuvihMHvpzbU4KK9NosR%7CJl89XZC0RqbZGDPJmTne%29XEB5bk2DdSfQabifhzJykiugyTCcK4g"},{"key":"305004-100","small":"http://s.qdcdn.com/cl/13717659,240,426.jpg","big":"http://s.qdcdn.com/cl/13717659,720,1280.jpg","down":16234,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=uHLT3m9c9%7C1jC5ADjGU%29wd%7C6WhQwBjqh4MMopK2sNu1vJapq21yj3hvfIPrYjXOCN8K3SD4Vfr4QzxsOifvRuGz7hG0Mkt6Lx5BJuw"},{"key":"205981-100","small":"http://s.qdcdn.com/cl/12556975,240,426.jpg","big":"http://s.qdcdn.com/cl/12556975,720,1280.jpg","down":33740,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=NXNBujOMfBzJ96T0A1dHDdhI%7CBrKG9tuXSOgfFZ5umYWbK37VjPYaK%29mkmtsvODXnhZb5B59%29WXGV6EP4tw3mxvUMuNMmSSsxvBI4A"},{"key":"88813-100","small":"http://s.qdcdn.com/cl/10911945,240,426.jpg","big":"http://s.qdcdn.com/cl/10911945,720,1280.jpg","down":32160,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=gbqse2MTsRVYXccxiqRg1G5PSfLPpDT3AWiZQw330sXxv83%29XgvYWkvVKwQYRvB8iWsXoe90EiKLmIiS7ZJoO3Lvd48dhYnkmqeqcQ"},{"key":"8270-100","small":"http://s.qdcdn.com/cl/10029726,240,426.jpg","big":"http://s.qdcdn.com/cl/10029726,720,1280.jpg","down":10950,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=%7C9KBqQipuM63Cvtyn5aVXDAqdQ2ozKFfC%298d0bPGa12e6d5X0jr8f6b0Gi%7C%29ZbchKq2WDWdchR%29uQrC%7C1PDrxUk40f4T0N2i"},{"key":"205942-100","small":"http://s.qdcdn.com/cl/12556681,240,426.jpg","big":"http://s.qdcdn.com/cl/12556681,720,1280.jpg","down":66493,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=4Ua3XDCS5HhJciwtiorckTFhp0bPVyrRdnglTp8c92bsn7ck5ecv2Ja4U0gOcSOt3sTBMap3Lg9s3ChxnTy%29rN3mkL%7CEk65f%29UMqlw"},{"key":"205972-100","small":"http://s.qdcdn.com/cl/12556912,240,426.jpg","big":"http://s.qdcdn.com/cl/12556912,720,1280.jpg","down":16444,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=2cHcvJAcisUvG8s9clLlvPkD3%29uYXnxp9IQEZOc4R7DFAcNuTs0M1s4T6I3g316Fl4djlqUAq6d3GNrMf4EwUWFPuSOts%29m1GYZ0RQ"},{"key":"205228-100","small":"http://s.qdcdn.com/cl/12548386,240,426.jpg","big":"http://s.qdcdn.com/cl/12548386,720,1280.jpg","down":11432,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=xzWcd6uvYqMqya83LI%298bro3LVQcmCQxYfS6LgnoGU%29fAZbjb95RSNG3MbzY9hYgNyhDMuhpLb44UONEXgLLjVyWpsQRPNv0g5tHbA"},{"key":"88322-100","small":"http://s.qdcdn.com/cl/10893053,240,426.jpg","big":"http://s.qdcdn.com/cl/10893053,720,1280.jpg","down":65703,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=Na3KsxtwWs%29zSEsegkqt09fvcZ6f2is0HLddHY8I8%7Cdpuvq3VemlBtsvWVL3265lQ7x5qpUzZ%29HA7kwkoEOJeYuOO3xbfZrXvaG0WQ"},{"key":"205239-100","small":"http://s.qdcdn.com/cl/12548466,240,426.jpg","big":"http://s.qdcdn.com/cl/12548466,720,1280.jpg","down":147628,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=K1ppr0Rz8kv0sqnHH%2919OCIe8QYSG5SxxmDIj3gkE6BKPRsYmvy1YehYLBwh4rYiqEWa29apu9iLWc6wN8XZqQVcXmZ%7CMsakp1f6NQ"},{"key":"205230-100","small":"http://s.qdcdn.com/cl/12548401,240,426.jpg","big":"http://s.qdcdn.com/cl/12548401,720,1280.jpg","down":164776,"down_stat":"http://open.lovebizhi.com/bdrom/stat?code=fs34%7CUpOvASCNn8mly43tOY30P9wzYlVQM1XAbZ76%290KRHCLkEK03WeUxD7m2Gcz6Aa0glec3%7CT3T3DcRhb3l4XC1ctNxX6KeLBXGw"}]
     */

    private LinkBean link;
    private List<DataBean> data;

    public LinkBean getLink() {
        return link;
    }

    public void setLink(LinkBean link) {
        this.link = link;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class LinkBean {
        /**
         * prev :
         * next : http://open.lovebizhi.com/bdrom/category?code=6ghDtD1YckBHFzRXay%29KVDIQaMgaAqRnYHdljrPBhmarsQ2XK4FlzLcSqZiyaUuAJL%7Cr3Mwc2XsoCF2Sh6lxnQ
         */

        private String prev;
        private String next;

        public String getPrev() {
            return prev;
        }

        public void setPrev(String prev) {
            this.prev = prev;
        }

        public String getNext() {
            return next;
        }

        public void setNext(String next) {
            this.next = next;
        }
    }

    public static class DataBean {
        /**
         * key : 231204-100
         * small : http://s.qdcdn.com/cl/12825369,240,426.jpg
         * big : http://s.qdcdn.com/cl/12825369,720,1280.jpg
         * down : 2210467
         * down_stat : http://open.lovebizhi.com/bdrom/stat?code=m66z0XL5fzexho%29fNKYp409hAKN6y3Ptb9wlVieNk1HXwLJkD%7CkWEp%7Cgqzbn778J56l50SP1D9f8lddle7pZNI6q3tCTho%7Cvy2Oa4Q
         */

        private String key;
        private String small;
        private String big;
        private int down;
        private String down_stat;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getSmall() {
            return small;
        }

        public void setSmall(String small) {
            this.small = small;
        }

        public String getBig() {
            return big;
        }

        public void setBig(String big) {
            this.big = big;
        }

        public int getDown() {
            return down;
        }

        public void setDown(int down) {
            this.down = down;
        }

        public String getDown_stat() {
            return down_stat;
        }

        public void setDown_stat(String down_stat) {
            this.down_stat = down_stat;
        }
    }
}
