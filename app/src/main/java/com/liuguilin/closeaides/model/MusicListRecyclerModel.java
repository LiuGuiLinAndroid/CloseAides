package com.liuguilin.closeaides.model;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.model
 *  文件名:   MusicListRecyclerModel
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/12 22:01
 *  描述：    数据
 */
public class MusicListRecyclerModel {

    //标题
    private String title;
    //图片
    private String cover;
    //标题
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }
}
