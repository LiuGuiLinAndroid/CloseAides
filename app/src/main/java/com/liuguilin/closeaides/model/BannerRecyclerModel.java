package com.liuguilin.closeaides.model;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.model
 *  文件名:   BannerRecyclerModel
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/16 16:09
 *  描述：
 */
public class BannerRecyclerModel {

    private String smallImg;
    private String bigimg;

    public String getSmallImg() {
        return smallImg;
    }

    public void setSmallImg(String smallImg) {
        this.smallImg = smallImg;
    }

    public String getBigimg() {
        return bigimg;
    }

    public void setBigimg(String bigimg) {
        this.bigimg = bigimg;
    }
}
