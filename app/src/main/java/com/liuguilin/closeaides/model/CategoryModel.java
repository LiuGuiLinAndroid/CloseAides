package com.liuguilin.closeaides.model;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.model
 *  文件名:   CategoryModel
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/8 16:22
 *  描述：    分类的数据
 */
public class CategoryModel {

    private String name;
    private String cover;
    private String url;

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }
}
