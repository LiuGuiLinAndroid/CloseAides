package com.liuguilin.closeaides.model;

import java.util.List;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.model
 *  文件名:   MusicListModel
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/12 21:36
 *  描述：    音乐列表数据
 */
public class MusicListModel {

    /**
     * res : 0
     * data : ["1791","1789","1777","1775","1769","1756","1751","1745","1741","1739"]
     */

    private int res;
    private List<String> data;

    public int getRes() {
        return res;
    }

    public void setRes(int res) {
        this.res = res;
    }

    public List<String> getData() {
        return data;
    }

    public void setData(List<String> data) {
        this.data = data;
    }
}
