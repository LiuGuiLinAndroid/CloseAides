package com.liuguilin.closeaides.model;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.model
 *  文件名:   LifeListModel
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/20 11:43
 *  描述：    TODO
 */
public class LifeListModel {

    private String title;
    private String content;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
