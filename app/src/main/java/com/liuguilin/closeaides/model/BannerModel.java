package com.liuguilin.closeaides.model;

import java.util.List;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.model
 *  文件名:   BannerModel
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/16 15:31
 *  描述：    轮播图详情的适配器
 */
public class BannerModel {


    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * key : 345002-100
         * small : http://s.qdcdn.com/cl/14313306,240,426.jpg
         * big : http://s.qdcdn.com/cl/14313306,720,1280.jpg
         * down : 5740
         * down_stat : http://open.lovebizhi.com/bdrom/stat?code=LE4cBtU%7CzawVzlQNXw8WpUpgyNUl7H0VrrcXTperANCI5x%7CgT%7CEnshNzQT2mLfZOLQs8CYofSqQFPiRTXoZzIwU3KaQZlPBc
         */

        private String key;
        private String small;
        private String big;
        private int down;
        private String down_stat;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getSmall() {
            return small;
        }

        public void setSmall(String small) {
            this.small = small;
        }

        public String getBig() {
            return big;
        }

        public void setBig(String big) {
            this.big = big;
        }

        public int getDown() {
            return down;
        }

        public void setDown(int down) {
            this.down = down;
        }

        public String getDown_stat() {
            return down_stat;
        }

        public void setDown_stat(String down_stat) {
            this.down_stat = down_stat;
        }
    }
}
