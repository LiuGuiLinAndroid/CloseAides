package com.liuguilin.closeaides.model;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.model
 *  文件名:   SystemMessageModel
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/15 16:20
 *  描述：    TODO
 */
public class SystemMessageModel {

    //标题
    private String title;
    //文本
    private String key;
    //信息
    private String values;
    //标题文字
    private String button;
    //类型
    private int type;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValues() {
        return values;
    }

    public void setValues(String values) {
        this.values = values;
    }

    public String getButton() {
        return button;
    }

    public void setButton(String button) {
        this.button = button;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
