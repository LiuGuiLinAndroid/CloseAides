package com.liuguilin.closeaides.model;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.model
 *  文件名:   FileBrowseModel
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/13 21:54
 *  描述：    文件浏览的数据模型
 */
public class FileBrowseModel {

    //标题
    private String title;
    //地址
    private String path;
    //图标
    private int icon;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }
}
