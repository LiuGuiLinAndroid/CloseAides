package com.liuguilin.closeaides.model;

import java.util.List;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.model
 *  文件名:   MusicMoreModel
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/12 21:42
 *  描述：    音乐详情Gson的数据模型
 */
public class MusicMoreModel {


    /**
     * res : 0
     * data : {"id":"1789","title":"万事如意","cover":"http://image.wufazhuce.com/Fnfx1_r9z1fvvgg1THNfVij3LwO_","isfirst":"0","story_title":"请你不要不要吃我，我给你唱一支好听的歌","story":"\n<p>1997年4月11日，因心脏病突发，王小波倒在了昌平家中打开的电脑旁。今天是他离开的第二十年，可二十年间，他的作品仍影响着一代青年。<\/p>\n<div class=\"one-img-container one-img-container-no-note\">\n<br><br>这里藏着一张图片，前往应用商店，下载「一个」最新版本查看！<br><br>\n<\/div>\n<p>林少华说他是个不老实的边缘人，总是对主流怀有戒心，不时旁敲侧击，甚至像个天真烂漫口无遮拦的孩子，指出看似西装革履道貌岸然的人其实可能什么也没穿。<\/p>\n<p>高晓松说王小波在他读过的白话文作家中绝对排第一，并且甩开第二名非常远，在他心里是神一样的存在。<\/p>\n<p>冯唐说他的文字，仿佛钻石着光，春花带露，灿烂无比，蛊惑人心。<\/p>\n<p>叶兆言说读他的作品，就告诉你什么是白天，什么是黑夜。<\/p>\n<p>他把人生浓缩为\"那一天我二十一岁，在我一生的黄金时代，我有好多奢望。我想爱，想吃，还想在一瞬间变成天上半明半暗的云，后来我才知道，生活就是个缓慢受锤的过程，人一天天老下去，奢望也一天天消逝，最后变得像挨了锤的牛一样。可是我过二十一岁生日时没有预见到这一点。我觉得自己会永远生猛下去，什么也锤不了我\"。<\/p>\n<p>可这位永远都会生猛下去的\u201c特立独行的猪\u201d，却有着比别人都柔情的一面，被称为中国最会说情话的男人。<\/p>\n<p>他没有最漂亮的皮囊，却拥有美丽到让所有人羡艳的爱情。那句\u201c你好哇，李银河\u201d，仍是如今文艺青年们争相模仿的情话范本。<\/p>\n<div class=\"one-img-container one-img-container-no-note\">\n<br><br>这里藏着一张图片，前往应用商店，下载「一个」最新版本查看！<br><br>\n<\/div>\n<p>他曾经将给妻子李银河的信写在了五线谱上，说\u201c但愿我和你，是一支唱不完的歌\u201d。王小波离世后，他们的书信被集结成册，至今热销，而那些情话也真的被谱上旋律，变成了动人的情歌。<\/p>\n<p>这首《万事如意》，便是歌手王梵瑞以王小波和李银河的书信集《爱你就像爱生命》为灵感创作的。歌曲制作完成后，王梵瑞为表尊重，特别请李银河先听了歌曲，而李银河表示非常喜欢，甚至感慨这首歌打败了她。<\/p>\n<p>今天，就让我们听着这首情歌，读着王小波有趣又深情的情话，在浪漫中怀念他。<\/p>\n<div class=\"one-img-container one-img-container-no-note\">\n<br><br>这里藏着一张图片，前往应用商店，下载「一个」最新版本查看！<br><br>\n<\/div>\n<p>我把我整个的灵魂都给你，连同它的怪癖，耍小脾气，忽明忽暗，一千八百种坏毛病。它真讨厌，只有一点好，爱你。<\/p>\n<p>你想知道我对你的爱情是什么吗 ?就是从心底里喜欢你，觉得你的一举一动都很亲切，不高兴你比喜欢我更喜欢别人 。你要是喜欢别人我会哭，但是还是喜欢你 。<\/p>\n<p>当我跨过沉沦的一切，向着永恒开战的时候，你是我的军旗。<\/p>\n<p>我很讨厌我自己不温不凉的思虑过度，也许我是个坏人，不过我只要你吻我一下就会变好呢。<\/p>\n<p>你的名字美极了。真的，单单你的名字就够我爱一世的了。<\/p>\n<p>要我们能在一起，我们什么都能找到。也许缺乏勇气是到达美好境界的障碍。你看我是多么适合你的人。我的勇气和你的勇气加起来，对付这个世界总够了吧？要无忧无虑地去抒情，去歌舞狂欢，去向世界发出我们的声音，我一个人是不敢的，我怕人家说我疯。有了你我就敢。只要有你一个，就不孤独。<\/p>\n<p>咱们应当在一起，否则就太伤天害理啦。<\/p>\n<p>一想到你，我这张丑脸上就泛起微笑。<\/p>\n<p>我现在不坏了，我有了良心。我的良心就是你。<\/p>\n<p>祝你今天愉快，你明天的愉快留着我明天再祝。<\/p>\n<p>请你不要不要吃我,我给你唱一支好听的歌。<\/p>\n<p>我觉得我的话不能孤孤单单地写在这里,你要把你的信写在空白的地方。这可不是海誓山盟。海誓山盟是把现在的东西固定住，两个人成了活化石。我们用不着它。我们要爱情长久。<\/p>\n<p>别怕美好的一切消失，咱们先来让它存在。<\/p>\n","lyric":"七年前曾遇到过这个座位，\r\n我想都没想就坐了下来。\r\n结果差点丢了命，丢了命\u2026\u2026\r\n七年后的夏天又遇见了这个座位，\r\n想了好久觉得运气不会总这么差，\r\n就又一次挨着她 坐了下来\u2026..\r\n\u201c我是爱你的，看见就爱上了，\r\n就像一个人手里一只鸽子飞走了。\r\n他从心里祝福那鸽子的飞翔，\r\n你也飞翔吧，我会难过，也会高兴\u201d\r\n节选（爱你就像爱生命）王小波\r\n可你是什么呢？你会像爱鸽子般爱我吗？\r\n可我是一只不会飞的鸽子了\r\n可你是那双抓住我的手啊\r\n你放掉我 我会因此丢了命。\r\n\u201c我是爱你的，看见就爱上了，\r\n就像一个人手里一只鸽子飞走了。\r\n他从心里祝福那鸽子的飞翔，\r\n你也飞翔吧，我会难过，也会高兴\u201d\r\n我在我的故乡遇见你。本以为万事如意\r\n没想到这一切还是 不尽人意\u2026..\r\n可你是什么呢？你会像爱鸽子般爱我吗？\r\n可我是一只不会飞的鸽子了\r\n可你是那双抓住我的手啊\r\n你放掉我 我会因此丢了命。\r\n\u201c我是爱你的，看见就爱上了，\r\n就像一个人手里一只鸽子飞走了。\r\n他从心里祝福那鸽子的飞翔，\r\n你也飞翔吧，我会难过，也会高兴\u201d\r\n\u201c我是爱你的，看见就爱上了，\r\n就像一个人手里一只鸽子飞走了。\r\n他从心里祝福那鸽子的飞翔，\r\n你也飞翔吧，我会难过，也会高兴\u201d","info":"作曲 : 王梵瑞\r\n作词 : 王梵瑞\r\n电吉他：段同愿 刘子滔（特邀果味VC乐队）\r\n木吉他：王梵瑞 刘子滔（特邀果味VC乐队）\r\n贝斯： 马海滨\r\n鼓: 姬伟\r\n钢琴：王天恩 王梵瑞\r\n器乐录音：张俊\r\n人声录音：刘时宇\r\n前期混音：刘时宇\r\n混音：Chris Perry（加拿大）\r\n录音棚：S.A.G.录音棚 （北京）\r\n母带工程师: Justin Shturtz（美国）\r\n母带录音室: Sterling Sound（美国）\r\n唱片公司：太合音乐\r\n发行时间：2016年11月25日\r\n专辑类别：录音室专辑","platform":"1","music_id":"1795278313","charge_edt":"（责任编辑：十三妹 shisanmei@wufazhuce.com）","related_to":"0","web_url":"http://h.xiami.com/one-share.html?id=1795278313","praisenum":1101,"hide_flag":"0","sort":"0","maketime":"2017-04-11 06:00:00","last_update_date":"2017-04-10 19:47:23","read_num":"176304","story_summary":"你好哇，王小波。","audio":"","anchor":"","editor_email":"shisanmei@wufazhuce.com","related_musics":"","album":"万重山","start_video":"","media_type":"1","copyright":"封面摄影师Alex Zhu","author":{"user_id":"6152675","user_name":"王梵瑞","desc":"音乐人","wb_name":"@王梵瑞","is_settled":"0","settled_type":"0","summary":"音乐人","fans_total":"6","web_url":"http://image.wufazhuce.com/Ftuisju71gftJrO_3RuwMVE2AXvd"},"story_author":{"user_id":"7570665","user_name":"ONE APP","desc":"ONE官方账号","wb_name":"","is_settled":"0","settled_type":"0","summary":"ONE官方账号","fans_total":"10682","web_url":"http://image.wufazhuce.com/Fj5-t3NM5FkTWzN5FZZUyqXctBrY"},"author_list":[{"user_id":"7570665","user_name":"ONE APP","desc":"ONE官方账号","wb_name":"","is_settled":"0","settled_type":"0","summary":"ONE官方账号","fans_total":"10682","web_url":"http://image.wufazhuce.com/Fj5-t3NM5FkTWzN5FZZUyqXctBrY"}],"next_id":"1791","previous_id":"1777","tag_list":[],"share_list":{"wx":{"title":"音乐 | 请你不要不要吃我，我给你唱一支好听的歌","desc":"文/ONE APP 你好哇，王小波。","link":"http://h.xiami.com/one-share.html?id=1795278313","imgUrl":"http://image.wufazhuce.com/ONE_logo_120_square.png"},"weibo":{"title":"ONE·一个《音乐 | 请你不要不要吃我，我给你唱一支好听的歌》 文/ONE APP： 你好哇，王小波。 阅读全文：http://h.xiami.com/one-share.html?id=1795278313 下载ONE·一个APP:http://weibo.com/p/100404157874","desc":"","link":"","imgUrl":""},"qq":{"title":"请你不要不要吃我，我给你唱一支好听的歌","desc":"你好哇，王小波。","link":"http://h.xiami.com/one-share.html?id=1795278313","imgUrl":"http://image.wufazhuce.com/ONE_logo_120_square.png"}},"sharenum":1310,"commentnum":195}
     */

    private int res;
    private DataBean data;

    public int getRes() {
        return res;
    }

    public void setRes(int res) {
        this.res = res;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 1789
         * title : 万事如意
         * cover : http://image.wufazhuce.com/Fnfx1_r9z1fvvgg1THNfVij3LwO_
         * isfirst : 0
         * story_title : 请你不要不要吃我，我给你唱一支好听的歌
         * story :
         <p>1997年4月11日，因心脏病突发，王小波倒在了昌平家中打开的电脑旁。今天是他离开的第二十年，可二十年间，他的作品仍影响着一代青年。</p>
         <div class="one-img-container one-img-container-no-note">
         <br><br>这里藏着一张图片，前往应用商店，下载「一个」最新版本查看！<br><br>
         </div>
         <p>林少华说他是个不老实的边缘人，总是对主流怀有戒心，不时旁敲侧击，甚至像个天真烂漫口无遮拦的孩子，指出看似西装革履道貌岸然的人其实可能什么也没穿。</p>
         <p>高晓松说王小波在他读过的白话文作家中绝对排第一，并且甩开第二名非常远，在他心里是神一样的存在。</p>
         <p>冯唐说他的文字，仿佛钻石着光，春花带露，灿烂无比，蛊惑人心。</p>
         <p>叶兆言说读他的作品，就告诉你什么是白天，什么是黑夜。</p>
         <p>他把人生浓缩为"那一天我二十一岁，在我一生的黄金时代，我有好多奢望。我想爱，想吃，还想在一瞬间变成天上半明半暗的云，后来我才知道，生活就是个缓慢受锤的过程，人一天天老下去，奢望也一天天消逝，最后变得像挨了锤的牛一样。可是我过二十一岁生日时没有预见到这一点。我觉得自己会永远生猛下去，什么也锤不了我"。</p>
         <p>可这位永远都会生猛下去的“特立独行的猪”，却有着比别人都柔情的一面，被称为中国最会说情话的男人。</p>
         <p>他没有最漂亮的皮囊，却拥有美丽到让所有人羡艳的爱情。那句“你好哇，李银河”，仍是如今文艺青年们争相模仿的情话范本。</p>
         <div class="one-img-container one-img-container-no-note">
         <br><br>这里藏着一张图片，前往应用商店，下载「一个」最新版本查看！<br><br>
         </div>
         <p>他曾经将给妻子李银河的信写在了五线谱上，说“但愿我和你，是一支唱不完的歌”。王小波离世后，他们的书信被集结成册，至今热销，而那些情话也真的被谱上旋律，变成了动人的情歌。</p>
         <p>这首《万事如意》，便是歌手王梵瑞以王小波和李银河的书信集《爱你就像爱生命》为灵感创作的。歌曲制作完成后，王梵瑞为表尊重，特别请李银河先听了歌曲，而李银河表示非常喜欢，甚至感慨这首歌打败了她。</p>
         <p>今天，就让我们听着这首情歌，读着王小波有趣又深情的情话，在浪漫中怀念他。</p>
         <div class="one-img-container one-img-container-no-note">
         <br><br>这里藏着一张图片，前往应用商店，下载「一个」最新版本查看！<br><br>
         </div>
         <p>我把我整个的灵魂都给你，连同它的怪癖，耍小脾气，忽明忽暗，一千八百种坏毛病。它真讨厌，只有一点好，爱你。</p>
         <p>你想知道我对你的爱情是什么吗 ?就是从心底里喜欢你，觉得你的一举一动都很亲切，不高兴你比喜欢我更喜欢别人 。你要是喜欢别人我会哭，但是还是喜欢你 。</p>
         <p>当我跨过沉沦的一切，向着永恒开战的时候，你是我的军旗。</p>
         <p>我很讨厌我自己不温不凉的思虑过度，也许我是个坏人，不过我只要你吻我一下就会变好呢。</p>
         <p>你的名字美极了。真的，单单你的名字就够我爱一世的了。</p>
         <p>要我们能在一起，我们什么都能找到。也许缺乏勇气是到达美好境界的障碍。你看我是多么适合你的人。我的勇气和你的勇气加起来，对付这个世界总够了吧？要无忧无虑地去抒情，去歌舞狂欢，去向世界发出我们的声音，我一个人是不敢的，我怕人家说我疯。有了你我就敢。只要有你一个，就不孤独。</p>
         <p>咱们应当在一起，否则就太伤天害理啦。</p>
         <p>一想到你，我这张丑脸上就泛起微笑。</p>
         <p>我现在不坏了，我有了良心。我的良心就是你。</p>
         <p>祝你今天愉快，你明天的愉快留着我明天再祝。</p>
         <p>请你不要不要吃我,我给你唱一支好听的歌。</p>
         <p>我觉得我的话不能孤孤单单地写在这里,你要把你的信写在空白的地方。这可不是海誓山盟。海誓山盟是把现在的东西固定住，两个人成了活化石。我们用不着它。我们要爱情长久。</p>
         <p>别怕美好的一切消失，咱们先来让它存在。</p>

         * lyric : 七年前曾遇到过这个座位，
         我想都没想就坐了下来。
         结果差点丢了命，丢了命……
         七年后的夏天又遇见了这个座位，
         想了好久觉得运气不会总这么差，
         就又一次挨着她 坐了下来…..
         “我是爱你的，看见就爱上了，
         就像一个人手里一只鸽子飞走了。
         他从心里祝福那鸽子的飞翔，
         你也飞翔吧，我会难过，也会高兴”
         节选（爱你就像爱生命）王小波
         可你是什么呢？你会像爱鸽子般爱我吗？
         可我是一只不会飞的鸽子了
         可你是那双抓住我的手啊
         你放掉我 我会因此丢了命。
         “我是爱你的，看见就爱上了，
         就像一个人手里一只鸽子飞走了。
         他从心里祝福那鸽子的飞翔，
         你也飞翔吧，我会难过，也会高兴”
         我在我的故乡遇见你。本以为万事如意
         没想到这一切还是 不尽人意…..
         可你是什么呢？你会像爱鸽子般爱我吗？
         可我是一只不会飞的鸽子了
         可你是那双抓住我的手啊
         你放掉我 我会因此丢了命。
         “我是爱你的，看见就爱上了，
         就像一个人手里一只鸽子飞走了。
         他从心里祝福那鸽子的飞翔，
         你也飞翔吧，我会难过，也会高兴”
         “我是爱你的，看见就爱上了，
         就像一个人手里一只鸽子飞走了。
         他从心里祝福那鸽子的飞翔，
         你也飞翔吧，我会难过，也会高兴”
         * info : 作曲 : 王梵瑞
         作词 : 王梵瑞
         电吉他：段同愿 刘子滔（特邀果味VC乐队）
         木吉他：王梵瑞 刘子滔（特邀果味VC乐队）
         贝斯： 马海滨
         鼓: 姬伟
         钢琴：王天恩 王梵瑞
         器乐录音：张俊
         人声录音：刘时宇
         前期混音：刘时宇
         混音：Chris Perry（加拿大）
         录音棚：S.A.G.录音棚 （北京）
         母带工程师: Justin Shturtz（美国）
         母带录音室: Sterling Sound（美国）
         唱片公司：太合音乐
         发行时间：2016年11月25日
         专辑类别：录音室专辑
         * platform : 1
         * music_id : 1795278313
         * charge_edt : （责任编辑：十三妹 shisanmei@wufazhuce.com）
         * related_to : 0
         * web_url : http://h.xiami.com/one-share.html?id=1795278313
         * praisenum : 1101
         * hide_flag : 0
         * sort : 0
         * maketime : 2017-04-11 06:00:00
         * last_update_date : 2017-04-10 19:47:23
         * read_num : 176304
         * story_summary : 你好哇，王小波。
         * audio :
         * anchor :
         * editor_email : shisanmei@wufazhuce.com
         * related_musics :
         * album : 万重山
         * start_video :
         * media_type : 1
         * copyright : 封面摄影师Alex Zhu
         * author : {"user_id":"6152675","user_name":"王梵瑞","desc":"音乐人","wb_name":"@王梵瑞","is_settled":"0","settled_type":"0","summary":"音乐人","fans_total":"6","web_url":"http://image.wufazhuce.com/Ftuisju71gftJrO_3RuwMVE2AXvd"}
         * story_author : {"user_id":"7570665","user_name":"ONE APP","desc":"ONE官方账号","wb_name":"","is_settled":"0","settled_type":"0","summary":"ONE官方账号","fans_total":"10682","web_url":"http://image.wufazhuce.com/Fj5-t3NM5FkTWzN5FZZUyqXctBrY"}
         * author_list : [{"user_id":"7570665","user_name":"ONE APP","desc":"ONE官方账号","wb_name":"","is_settled":"0","settled_type":"0","summary":"ONE官方账号","fans_total":"10682","web_url":"http://image.wufazhuce.com/Fj5-t3NM5FkTWzN5FZZUyqXctBrY"}]
         * next_id : 1791
         * previous_id : 1777
         * tag_list : []
         * share_list : {"wx":{"title":"音乐 | 请你不要不要吃我，我给你唱一支好听的歌","desc":"文/ONE APP 你好哇，王小波。","link":"http://h.xiami.com/one-share.html?id=1795278313","imgUrl":"http://image.wufazhuce.com/ONE_logo_120_square.png"},"weibo":{"title":"ONE·一个《音乐 | 请你不要不要吃我，我给你唱一支好听的歌》 文/ONE APP： 你好哇，王小波。 阅读全文：http://h.xiami.com/one-share.html?id=1795278313 下载ONE·一个APP:http://weibo.com/p/100404157874","desc":"","link":"","imgUrl":""},"qq":{"title":"请你不要不要吃我，我给你唱一支好听的歌","desc":"你好哇，王小波。","link":"http://h.xiami.com/one-share.html?id=1795278313","imgUrl":"http://image.wufazhuce.com/ONE_logo_120_square.png"}}
         * sharenum : 1310
         * commentnum : 195
         */

        private String id;
        private String title;
        private String cover;
        private String isfirst;
        private String story_title;
        private String story;
        private String lyric;
        private String info;
        private String platform;
        private String music_id;
        private String charge_edt;
        private String related_to;
        private String web_url;
        private int praisenum;
        private String hide_flag;
        private String sort;
        private String maketime;
        private String last_update_date;
        private String read_num;
        private String story_summary;
        private String audio;
        private String anchor;
        private String editor_email;
        private String related_musics;
        private String album;
        private String start_video;
        private String media_type;
        private String copyright;
        private AuthorBean author;
        private StoryAuthorBean story_author;
        private String next_id;
        private String previous_id;
        private ShareListBean share_list;
        private int sharenum;
        private int commentnum;
        private List<AuthorListBean> author_list;
        private List<?> tag_list;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getCover() {
            return cover;
        }

        public void setCover(String cover) {
            this.cover = cover;
        }

        public String getIsfirst() {
            return isfirst;
        }

        public void setIsfirst(String isfirst) {
            this.isfirst = isfirst;
        }

        public String getStory_title() {
            return story_title;
        }

        public void setStory_title(String story_title) {
            this.story_title = story_title;
        }

        public String getStory() {
            return story;
        }

        public void setStory(String story) {
            this.story = story;
        }

        public String getLyric() {
            return lyric;
        }

        public void setLyric(String lyric) {
            this.lyric = lyric;
        }

        public String getInfo() {
            return info;
        }

        public void setInfo(String info) {
            this.info = info;
        }

        public String getPlatform() {
            return platform;
        }

        public void setPlatform(String platform) {
            this.platform = platform;
        }

        public String getMusic_id() {
            return music_id;
        }

        public void setMusic_id(String music_id) {
            this.music_id = music_id;
        }

        public String getCharge_edt() {
            return charge_edt;
        }

        public void setCharge_edt(String charge_edt) {
            this.charge_edt = charge_edt;
        }

        public String getRelated_to() {
            return related_to;
        }

        public void setRelated_to(String related_to) {
            this.related_to = related_to;
        }

        public String getWeb_url() {
            return web_url;
        }

        public void setWeb_url(String web_url) {
            this.web_url = web_url;
        }

        public int getPraisenum() {
            return praisenum;
        }

        public void setPraisenum(int praisenum) {
            this.praisenum = praisenum;
        }

        public String getHide_flag() {
            return hide_flag;
        }

        public void setHide_flag(String hide_flag) {
            this.hide_flag = hide_flag;
        }

        public String getSort() {
            return sort;
        }

        public void setSort(String sort) {
            this.sort = sort;
        }

        public String getMaketime() {
            return maketime;
        }

        public void setMaketime(String maketime) {
            this.maketime = maketime;
        }

        public String getLast_update_date() {
            return last_update_date;
        }

        public void setLast_update_date(String last_update_date) {
            this.last_update_date = last_update_date;
        }

        public String getRead_num() {
            return read_num;
        }

        public void setRead_num(String read_num) {
            this.read_num = read_num;
        }

        public String getStory_summary() {
            return story_summary;
        }

        public void setStory_summary(String story_summary) {
            this.story_summary = story_summary;
        }

        public String getAudio() {
            return audio;
        }

        public void setAudio(String audio) {
            this.audio = audio;
        }

        public String getAnchor() {
            return anchor;
        }

        public void setAnchor(String anchor) {
            this.anchor = anchor;
        }

        public String getEditor_email() {
            return editor_email;
        }

        public void setEditor_email(String editor_email) {
            this.editor_email = editor_email;
        }

        public String getRelated_musics() {
            return related_musics;
        }

        public void setRelated_musics(String related_musics) {
            this.related_musics = related_musics;
        }

        public String getAlbum() {
            return album;
        }

        public void setAlbum(String album) {
            this.album = album;
        }

        public String getStart_video() {
            return start_video;
        }

        public void setStart_video(String start_video) {
            this.start_video = start_video;
        }

        public String getMedia_type() {
            return media_type;
        }

        public void setMedia_type(String media_type) {
            this.media_type = media_type;
        }

        public String getCopyright() {
            return copyright;
        }

        public void setCopyright(String copyright) {
            this.copyright = copyright;
        }

        public AuthorBean getAuthor() {
            return author;
        }

        public void setAuthor(AuthorBean author) {
            this.author = author;
        }

        public StoryAuthorBean getStory_author() {
            return story_author;
        }

        public void setStory_author(StoryAuthorBean story_author) {
            this.story_author = story_author;
        }

        public String getNext_id() {
            return next_id;
        }

        public void setNext_id(String next_id) {
            this.next_id = next_id;
        }

        public String getPrevious_id() {
            return previous_id;
        }

        public void setPrevious_id(String previous_id) {
            this.previous_id = previous_id;
        }

        public ShareListBean getShare_list() {
            return share_list;
        }

        public void setShare_list(ShareListBean share_list) {
            this.share_list = share_list;
        }

        public int getSharenum() {
            return sharenum;
        }

        public void setSharenum(int sharenum) {
            this.sharenum = sharenum;
        }

        public int getCommentnum() {
            return commentnum;
        }

        public void setCommentnum(int commentnum) {
            this.commentnum = commentnum;
        }

        public List<AuthorListBean> getAuthor_list() {
            return author_list;
        }

        public void setAuthor_list(List<AuthorListBean> author_list) {
            this.author_list = author_list;
        }

        public List<?> getTag_list() {
            return tag_list;
        }

        public void setTag_list(List<?> tag_list) {
            this.tag_list = tag_list;
        }

        public static class AuthorBean {
            /**
             * user_id : 6152675
             * user_name : 王梵瑞
             * desc : 音乐人
             * wb_name : @王梵瑞
             * is_settled : 0
             * settled_type : 0
             * summary : 音乐人
             * fans_total : 6
             * web_url : http://image.wufazhuce.com/Ftuisju71gftJrO_3RuwMVE2AXvd
             */

            private String user_id;
            private String user_name;
            private String desc;
            private String wb_name;
            private String is_settled;
            private String settled_type;
            private String summary;
            private String fans_total;
            private String web_url;

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public String getUser_name() {
                return user_name;
            }

            public void setUser_name(String user_name) {
                this.user_name = user_name;
            }

            public String getDesc() {
                return desc;
            }

            public void setDesc(String desc) {
                this.desc = desc;
            }

            public String getWb_name() {
                return wb_name;
            }

            public void setWb_name(String wb_name) {
                this.wb_name = wb_name;
            }

            public String getIs_settled() {
                return is_settled;
            }

            public void setIs_settled(String is_settled) {
                this.is_settled = is_settled;
            }

            public String getSettled_type() {
                return settled_type;
            }

            public void setSettled_type(String settled_type) {
                this.settled_type = settled_type;
            }

            public String getSummary() {
                return summary;
            }

            public void setSummary(String summary) {
                this.summary = summary;
            }

            public String getFans_total() {
                return fans_total;
            }

            public void setFans_total(String fans_total) {
                this.fans_total = fans_total;
            }

            public String getWeb_url() {
                return web_url;
            }

            public void setWeb_url(String web_url) {
                this.web_url = web_url;
            }
        }

        public static class StoryAuthorBean {
            /**
             * user_id : 7570665
             * user_name : ONE APP
             * desc : ONE官方账号
             * wb_name :
             * is_settled : 0
             * settled_type : 0
             * summary : ONE官方账号
             * fans_total : 10682
             * web_url : http://image.wufazhuce.com/Fj5-t3NM5FkTWzN5FZZUyqXctBrY
             */

            private String user_id;
            private String user_name;
            private String desc;
            private String wb_name;
            private String is_settled;
            private String settled_type;
            private String summary;
            private String fans_total;
            private String web_url;

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public String getUser_name() {
                return user_name;
            }

            public void setUser_name(String user_name) {
                this.user_name = user_name;
            }

            public String getDesc() {
                return desc;
            }

            public void setDesc(String desc) {
                this.desc = desc;
            }

            public String getWb_name() {
                return wb_name;
            }

            public void setWb_name(String wb_name) {
                this.wb_name = wb_name;
            }

            public String getIs_settled() {
                return is_settled;
            }

            public void setIs_settled(String is_settled) {
                this.is_settled = is_settled;
            }

            public String getSettled_type() {
                return settled_type;
            }

            public void setSettled_type(String settled_type) {
                this.settled_type = settled_type;
            }

            public String getSummary() {
                return summary;
            }

            public void setSummary(String summary) {
                this.summary = summary;
            }

            public String getFans_total() {
                return fans_total;
            }

            public void setFans_total(String fans_total) {
                this.fans_total = fans_total;
            }

            public String getWeb_url() {
                return web_url;
            }

            public void setWeb_url(String web_url) {
                this.web_url = web_url;
            }
        }

        public static class ShareListBean {
            /**
             * wx : {"title":"音乐 | 请你不要不要吃我，我给你唱一支好听的歌","desc":"文/ONE APP 你好哇，王小波。","link":"http://h.xiami.com/one-share.html?id=1795278313","imgUrl":"http://image.wufazhuce.com/ONE_logo_120_square.png"}
             * weibo : {"title":"ONE·一个《音乐 | 请你不要不要吃我，我给你唱一支好听的歌》 文/ONE APP： 你好哇，王小波。 阅读全文：http://h.xiami.com/one-share.html?id=1795278313 下载ONE·一个APP:http://weibo.com/p/100404157874","desc":"","link":"","imgUrl":""}
             * qq : {"title":"请你不要不要吃我，我给你唱一支好听的歌","desc":"你好哇，王小波。","link":"http://h.xiami.com/one-share.html?id=1795278313","imgUrl":"http://image.wufazhuce.com/ONE_logo_120_square.png"}
             */

            private WxBean wx;
            private WeiboBean weibo;
            private QqBean qq;

            public WxBean getWx() {
                return wx;
            }

            public void setWx(WxBean wx) {
                this.wx = wx;
            }

            public WeiboBean getWeibo() {
                return weibo;
            }

            public void setWeibo(WeiboBean weibo) {
                this.weibo = weibo;
            }

            public QqBean getQq() {
                return qq;
            }

            public void setQq(QqBean qq) {
                this.qq = qq;
            }

            public static class WxBean {
                /**
                 * title : 音乐 | 请你不要不要吃我，我给你唱一支好听的歌
                 * desc : 文/ONE APP 你好哇，王小波。
                 * link : http://h.xiami.com/one-share.html?id=1795278313
                 * imgUrl : http://image.wufazhuce.com/ONE_logo_120_square.png
                 */

                private String title;
                private String desc;
                private String link;
                private String imgUrl;

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public String getDesc() {
                    return desc;
                }

                public void setDesc(String desc) {
                    this.desc = desc;
                }

                public String getLink() {
                    return link;
                }

                public void setLink(String link) {
                    this.link = link;
                }

                public String getImgUrl() {
                    return imgUrl;
                }

                public void setImgUrl(String imgUrl) {
                    this.imgUrl = imgUrl;
                }
            }

            public static class WeiboBean {
                /**
                 * title : ONE·一个《音乐 | 请你不要不要吃我，我给你唱一支好听的歌》 文/ONE APP： 你好哇，王小波。 阅读全文：http://h.xiami.com/one-share.html?id=1795278313 下载ONE·一个APP:http://weibo.com/p/100404157874
                 * desc :
                 * link :
                 * imgUrl :
                 */

                private String title;
                private String desc;
                private String link;
                private String imgUrl;

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public String getDesc() {
                    return desc;
                }

                public void setDesc(String desc) {
                    this.desc = desc;
                }

                public String getLink() {
                    return link;
                }

                public void setLink(String link) {
                    this.link = link;
                }

                public String getImgUrl() {
                    return imgUrl;
                }

                public void setImgUrl(String imgUrl) {
                    this.imgUrl = imgUrl;
                }
            }

            public static class QqBean {
                /**
                 * title : 请你不要不要吃我，我给你唱一支好听的歌
                 * desc : 你好哇，王小波。
                 * link : http://h.xiami.com/one-share.html?id=1795278313
                 * imgUrl : http://image.wufazhuce.com/ONE_logo_120_square.png
                 */

                private String title;
                private String desc;
                private String link;
                private String imgUrl;

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public String getDesc() {
                    return desc;
                }

                public void setDesc(String desc) {
                    this.desc = desc;
                }

                public String getLink() {
                    return link;
                }

                public void setLink(String link) {
                    this.link = link;
                }

                public String getImgUrl() {
                    return imgUrl;
                }

                public void setImgUrl(String imgUrl) {
                    this.imgUrl = imgUrl;
                }
            }
        }

        public static class AuthorListBean {
            /**
             * user_id : 7570665
             * user_name : ONE APP
             * desc : ONE官方账号
             * wb_name :
             * is_settled : 0
             * settled_type : 0
             * summary : ONE官方账号
             * fans_total : 10682
             * web_url : http://image.wufazhuce.com/Fj5-t3NM5FkTWzN5FZZUyqXctBrY
             */

            private String user_id;
            private String user_name;
            private String desc;
            private String wb_name;
            private String is_settled;
            private String settled_type;
            private String summary;
            private String fans_total;
            private String web_url;

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public String getUser_name() {
                return user_name;
            }

            public void setUser_name(String user_name) {
                this.user_name = user_name;
            }

            public String getDesc() {
                return desc;
            }

            public void setDesc(String desc) {
                this.desc = desc;
            }

            public String getWb_name() {
                return wb_name;
            }

            public void setWb_name(String wb_name) {
                this.wb_name = wb_name;
            }

            public String getIs_settled() {
                return is_settled;
            }

            public void setIs_settled(String is_settled) {
                this.is_settled = is_settled;
            }

            public String getSettled_type() {
                return settled_type;
            }

            public void setSettled_type(String settled_type) {
                this.settled_type = settled_type;
            }

            public String getSummary() {
                return summary;
            }

            public void setSummary(String summary) {
                this.summary = summary;
            }

            public String getFans_total() {
                return fans_total;
            }

            public void setFans_total(String fans_total) {
                this.fans_total = fans_total;
            }

            public String getWeb_url() {
                return web_url;
            }

            public void setWeb_url(String web_url) {
                this.web_url = web_url;
            }
        }
    }
}
