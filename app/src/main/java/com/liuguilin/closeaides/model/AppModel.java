package com.liuguilin.closeaides.model;

import android.graphics.drawable.Drawable;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.model
 *  文件名:   AppModel
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/7 20:24
 *  描述：    应用数据模型
 */
public class AppModel {

    //icon
    private Drawable appIcon;
    //名字
    private String appName;
    //大小
    private String appSize;
    //是否是系統应用
    private boolean isSystem = false;
    //包名
    private String appPackageName;

    public Drawable getAppIcon() {
        return appIcon;
    }

    public void setAppIcon(Drawable appIcon) {
        this.appIcon = appIcon;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppSize() {
        return appSize;
    }

    public void setAppSize(String appSize) {
        this.appSize = appSize;
    }

    public boolean isSystem() {
        return isSystem;
    }

    public void setSystem(boolean system) {
        isSystem = system;
    }

    public String getAppPackageName() {
        return appPackageName;
    }

    public void setAppPackageName(String appPackageName) {
        this.appPackageName = appPackageName;
    }
}
