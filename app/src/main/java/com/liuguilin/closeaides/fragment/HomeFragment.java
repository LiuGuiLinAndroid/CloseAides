package com.liuguilin.closeaides.fragment;

import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.liuguilin.closeaides.R;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.fragment
 *  文件名:   HomeFragment
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/6 21:26
 *  描述：    主页
 */
public class HomeFragment extends Fragment implements BottomNavigationView.OnNavigationItemSelectedListener {

    private SelectFragment selectFragment;
    private MusicFragment musicFragment;
    private MovieFragment movieFragment;
    private MeFragment meFragment;

    private BottomNavigationView mBottomNavigationView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, null);
        initView(view);
        return view;
    }

    private void initView(View view) {
        mBottomNavigationView = (BottomNavigationView) view.findViewById(R.id.mBottomNavigationView);
        //设置颜色显示原色
        mBottomNavigationView.setItemIconTintList(null);
        mBottomNavigationView.setOnNavigationItemSelectedListener(this);

        initSelectFragment();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_select:
                initSelectFragment();
                break;
            case R.id.menu_music:
                initMusicFragment();
                break;
            case R.id.menu_movie:
                initMovieFragment();
                break;
            case R.id.menu_me:
                initMeFragment();
                break;
        }
        return true;
    }

    private void initSelectFragment() {
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        if (selectFragment == null) {
            selectFragment = new SelectFragment();
            fragmentTransaction.add(R.id.home_content_layout, selectFragment);
        }
        hideAllFragment(fragmentTransaction);
        //需要显示的fragment
        fragmentTransaction.show(selectFragment);
        fragmentTransaction.commit();
    }

    private void initMusicFragment() {
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        if (musicFragment == null) {
            musicFragment = new MusicFragment();
            fragmentTransaction.add(R.id.home_content_layout, musicFragment);
        }
        hideAllFragment(fragmentTransaction);
        //需要显示的fragment
        fragmentTransaction.show(musicFragment);
        fragmentTransaction.commit();
    }

    private void initMovieFragment() {
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        if (movieFragment == null) {
            movieFragment = new MovieFragment();
            fragmentTransaction.add(R.id.home_content_layout, movieFragment);
        }
        hideAllFragment(fragmentTransaction);
        //需要显示的fragment
        fragmentTransaction.show(movieFragment);
        fragmentTransaction.commit();
    }

    private void initMeFragment() {
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        if (meFragment == null) {
            meFragment = new MeFragment();
            fragmentTransaction.add(R.id.home_content_layout, meFragment);
        }
        hideAllFragment(fragmentTransaction);
        //需要显示的fragment
        fragmentTransaction.show(meFragment);
        fragmentTransaction.commit();
    }

    private void hideAllFragment(FragmentTransaction fragmentTransaction) {
        if (selectFragment != null) {
            fragmentTransaction.hide(selectFragment);
        }
        if (musicFragment != null) {
            fragmentTransaction.hide(musicFragment);
        }
        if (movieFragment != null) {
            fragmentTransaction.hide(movieFragment);
        }
        if (meFragment != null) {
            fragmentTransaction.hide(meFragment);
        }
    }
}
