package com.liuguilin.closeaides.fragment;

import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.liuguilin.closeaides.R;
import com.liuguilin.closeaides.adapter.LifeIndexAdapter;
import com.liuguilin.closeaides.adapter.SimplePagerAdapter;
import com.liuguilin.closeaides.entity.Constans;
import com.liuguilin.closeaides.entity.Key;
import com.liuguilin.closeaides.impl.WeatherApi;
import com.liuguilin.closeaides.model.LifeListModel;
import com.liuguilin.closeaides.model.LifeModel;
import com.liuguilin.closeaides.model.NowWeatherModel;
import com.liuguilin.closeaides.model.WeekWeatherModel;
import com.liuguilin.closeaides.utils.GlideUtils;
import com.liuguilin.closeaides.view.AutoMeasureRecyclerView;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.fragment
 *  文件名:   MeFragment
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/7 19:57
 *  描述：    我
 */
public class MeFragment extends Fragment {

    private Retrofit retrofit;
    private WeatherApi api;

    private CircleImageView tv_header_photo;
    private TextView tv_me_name;
    private TextView tv_me_desc;
    private ViewPager mViewPager;
    private ScrollView mScrollView;
    private AutoMeasureRecyclerView mAutoMeasureRecyclerView;

    private View viewNowWeather, viewWeekWeather;
    private SimplePagerAdapter simpleAdapter;
    private List<View> mList = new ArrayList<>();

    //天气图标
    private ImageView iv_weather_icon;
    //天气城市
    private TextView tv_now_city;
    //天气
    private TextView tv_now_weather;
    //东北方
    private TextView tv_now_deg_dir;
    //风力
    private TextView tv_now_sc;
    //风速
    private TextView tv_now_spd;

    private LifeIndexAdapter lifeIndexAdapter;
    private List<LifeListModel> mListLife = new ArrayList<>();

    //线形图
    private LineChart mLineChar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_me, null);
        initView(view);
        return view;
    }

    private void initView(View view) {

        tv_header_photo = (CircleImageView) view.findViewById(R.id.tv_header_photo);
        tv_me_name = (TextView) view.findViewById(R.id.tv_me_name);
        tv_me_desc = (TextView) view.findViewById(R.id.tv_me_desc);
        mViewPager = (ViewPager) view.findViewById(R.id.mViewPager);
        mScrollView = (ScrollView) view.findViewById(R.id.mScrollView);

        mAutoMeasureRecyclerView = (AutoMeasureRecyclerView) view.findViewById(R.id.mAutoMeasureRecyclerView);
        mAutoMeasureRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        lifeIndexAdapter = new LifeIndexAdapter(getActivity(), mListLife);
        mAutoMeasureRecyclerView.setAdapter(lifeIndexAdapter);

        viewNowWeather = View.inflate(getActivity(), R.layout.layout_now_weather_item, null);
        iv_weather_icon = (ImageView) viewNowWeather.findViewById(R.id.iv_weather_icon);
        tv_now_city = (TextView) viewNowWeather.findViewById(R.id.tv_now_city);
        tv_now_weather = (TextView) viewNowWeather.findViewById(R.id.tv_now_weather);
        tv_now_deg_dir = (TextView) viewNowWeather.findViewById(R.id.tv_now_deg_dir);
        tv_now_sc = (TextView) viewNowWeather.findViewById(R.id.tv_now_sc);
        tv_now_spd = (TextView) viewNowWeather.findViewById(R.id.tv_now_spd);

        viewWeekWeather = View.inflate(getActivity(), R.layout.layout_week_weather_item, null);
        mLineChar = (LineChart) viewWeekWeather.findViewById(R.id.mLineChar);
        initLineChart();

        mList.add(viewNowWeather);
        mList.add(viewWeekWeather);

        simpleAdapter = new SimplePagerAdapter(mList);
        mViewPager.setAdapter(simpleAdapter);

        //初始化retrofit
        retrofit = new Retrofit.Builder()
                .baseUrl(Constans.WEATHER_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        api = retrofit.create(WeatherApi.class);

        initNowWeather();
        initWeekWeather();
        initLife();
    }

    //初始化线形图
    private void initLineChart() {
        //设置描述文本
        mLineChar.getDescription().setEnabled(false);
        //设置支持触控手势
        mLineChar.setTouchEnabled(false);
        //设置缩放
        mLineChar.setDragEnabled(false);
        //设置推动
        mLineChar.setScaleEnabled(false);

        XAxis xAxis = mLineChar.getXAxis();
        //设置x轴的最大值
        xAxis.setAxisMaximum(6f);
        //设置x轴的最小值
        xAxis.setAxisMinimum(0f);

        YAxis leftAxis = mLineChar.getAxisLeft();
        //y轴最大
        leftAxis.setAxisMaximum(50f);
        //y轴最小
        leftAxis.setAxisMinimum(-30f);
        //设置Y轴间隔
        leftAxis.setGranularity(10);

        //隱藏
        mLineChar.getAxisRight().setEnabled(false);

        //这里我模拟一些数据
        ArrayList<Entry> values = new ArrayList<Entry>();
        values.add(new Entry(0, 10));
        values.add(new Entry(1, 50));
        values.add(new Entry(2, -30));
        values.add(new Entry(3, 40));
        values.add(new Entry(4, 50));
        values.add(new Entry(5, 20));
        values.add(new Entry(6, 10));

        //设置数据
        setData(values);

        //默认动画
        mLineChar.animateX(2500);

        //隱藏
        mLineChar.getLegend().setEnabled(false);
    }

    //初始化今日天氣
    private void initNowWeather() {
        api.getNowWeather(Constans.WEATHER_NOW_CITY, Key.HF_WEATHER_KEY).enqueue(new Callback<NowWeatherModel>() {
            @Override
            public void onResponse(Call<NowWeatherModel> call, Response<NowWeatherModel> response) {
                if (response.isSuccessful()) {
                    loadNowWeather(response.body().getHeWeather5().get(0));
                }
            }

            @Override
            public void onFailure(Call<NowWeatherModel> call, Throwable t) {

            }
        });
    }


    //初始化一周天氣
    private void initWeekWeather() {
        api.getWeekWeather(Constans.WEATHER_NOW_CITY, Key.HF_WEATHER_KEY).enqueue(new Callback<WeekWeatherModel>() {
            @Override
            public void onResponse(Call<WeekWeatherModel> call, Response<WeekWeatherModel> response) {
                if (response.isSuccessful()) {
                    loadWeekWeather(response.body().getHeWeather5().get(0));
                }
            }

            @Override
            public void onFailure(Call<WeekWeatherModel> call, Throwable t) {

            }
        });
    }

    //初始化生活指数
    private void initLife() {
        api.getLifeIndex(Constans.WEATHER_NOW_CITY, Key.HF_WEATHER_KEY).enqueue(new Callback<LifeModel>() {
            @Override
            public void onResponse(Call<LifeModel> call, Response<LifeModel> response) {
                if (response.isSuccessful()) {
                    loadLifeIndex(response.body().getHeWeather5().get(0));
                }
            }

            @Override
            public void onFailure(Call<LifeModel> call, Throwable t) {

            }
        });
    }

    //加载今日数据
    private void loadNowWeather(NowWeatherModel.HeWeather5Bean heWeather5Bean) {
        GlideUtils.loadImageViewCenterCrop(getActivity(),
                "https://cdn.heweather.com/cond_icon/" + heWeather5Bean.getNow().getCond().getCode() + ".png", iv_weather_icon);
        //天气城市
        tv_now_city.setText(Constans.WEATHER_NOW_CITY);
        //天气
        tv_now_weather.setText(heWeather5Bean.getNow().getCond().getTxt() + " | " + heWeather5Bean.getNow().getTmp() + "°");
        //东北风
        tv_now_deg_dir.setText(heWeather5Bean.getNow().getWind().getDir() + " | " + heWeather5Bean.getNow().getWind().getDeg() + "度");
        //风力
        tv_now_sc.setText("风力:" + heWeather5Bean.getNow().getWind().getSc());
        //风速
        tv_now_spd.setText("风速:" + heWeather5Bean.getNow().getWind().getSpd());
    }

    //加载一周天气
    private void loadWeekWeather(WeekWeatherModel.HeWeather5Bean heWeather5Bean) {

    }

    //加载生活指数
    private void loadLifeIndex(LifeModel.HeWeather5Bean heWeather5Bean) {
        LifeModel.HeWeather5Bean.SuggestionBean suggestionBean = heWeather5Bean.getSuggestion();

        LifeListModel model1 = new LifeListModel();
        model1.setTitle("舒适度指数:" + suggestionBean.getComf().getBrf());
        model1.setContent(suggestionBean.getComf().getTxt());
        mListLife.add(model1);

        LifeListModel model2 = new LifeListModel();
        model2.setTitle("洗车指数:" + suggestionBean.getCw().getBrf());
        model2.setContent(suggestionBean.getCw().getTxt());
        mListLife.add(model2);

        LifeListModel model3 = new LifeListModel();
        model3.setTitle("穿衣指数:" + suggestionBean.getDrsg().getBrf());
        model3.setContent(suggestionBean.getDrsg().getTxt());
        mListLife.add(model3);

        LifeListModel model4 = new LifeListModel();
        model4.setTitle("感冒指数:" + suggestionBean.getFlu().getBrf());
        model4.setContent(suggestionBean.getFlu().getTxt());
        mListLife.add(model4);

        LifeListModel model5 = new LifeListModel();
        model5.setTitle("运动指数:" + suggestionBean.getSport().getBrf());
        model5.setContent(suggestionBean.getSport().getTxt());
        mListLife.add(model5);

        LifeListModel model6 = new LifeListModel();
        model6.setTitle("旅游指数:" + suggestionBean.getTrav().getBrf());
        model6.setContent(suggestionBean.getTrav().getTxt());
        mListLife.add(model6);

        LifeListModel model7 = new LifeListModel();
        model7.setTitle("紫外线指数:" + suggestionBean.getUv().getBrf());
        model7.setContent(suggestionBean.getUv().getTxt());
        mListLife.add(model7);

        lifeIndexAdapter.notifyDataSetChanged();
    }

    //传递数据集
    private void setData(ArrayList<Entry> values) {
        LineDataSet set1;
        if (mLineChar.getData() != null && mLineChar.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) mLineChar.getData().getDataSetByIndex(0);
            set1.setValues(values);
            mLineChar.getData().notifyDataChanged();
            mLineChar.notifyDataSetChanged();
        } else {
            // 创建一个数据集,并给它一个类型
            set1 = new LineDataSet(values, "");

            // 在这里设置线
            set1.enableDashedLine(10f, 5f, 0f);
            set1.enableDashedHighlightLine(10f, 5f, 0f);
            set1.setColor(Color.BLACK);
            set1.setCircleColor(Color.BLACK);
            set1.setLineWidth(1f);
            set1.setCircleRadius(3f);
            set1.setDrawCircleHole(false);
            set1.setValueTextSize(9f);
            set1.setDrawFilled(true);
            set1.setFormLineWidth(1f);
            set1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            set1.setFormSize(15.f);

            set1.setFillColor(0xd84315);

            //设置显示模式
            set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);

            ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
            //添加数据集
            dataSets.add(set1);

            //创建一个数据集的数据对象
            LineData data = new LineData(dataSets);

            //谁知数据
            mLineChar.setData(data);
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            if (mScrollView != null) {
                mScrollView.post(new Runnable() {
                    @Override
                    public void run() {
                        //滑动到顶部
                        mScrollView.fullScroll(ScrollView.FOCUS_UP);
                    }
                });
            }
        }
    }
}
