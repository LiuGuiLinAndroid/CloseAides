package com.liuguilin.closeaides.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.liuguilin.closeaides.R;
import com.liuguilin.closeaides.adapter.AppManagerAdapter;
import com.liuguilin.closeaides.model.AppModel;
import com.liuguilin.closeaides.ui.AppMessageActivity;
import com.liuguilin.closeaides.utils.AppUtils;
import com.liuguilin.closeaides.utils.L;

import java.util.ArrayList;
import java.util.List;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.fragment
 *  文件名:   AppFragment
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/6 21:29
 *  描述：    应用
 */
public class AppFragment extends Fragment implements TabLayout.OnTabSelectedListener, SwipeRefreshLayout.OnRefreshListener {

    private AppManagerAdapter appManagerAdapteer;
    private String[] mStr = {"已安装", "系统应用", "全部"};
    private List<AppModel> mAllList = new ArrayList<>();
    private List<AppModel> mList = new ArrayList<>();
    private TabLayout mTabLayout;
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    //当前的选项卡
    private int currentTab = 0;

    //卸载广告
    private unInstallReceiver unReceiver;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1000:
                    parsingApk(currentTab);
                    if (mSwipeRefreshLayout.isRefreshing()) {
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                    break;
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_app, null);
        initView(view);
        return view;
    }

    private void initView(View view) {

        //动态注册广播
        unReceiver = new unInstallReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_PACKAGE_REMOVED);
        intentFilter.addDataScheme("package");
        getActivity().registerReceiver(unReceiver, intentFilter);

        mTabLayout = (TabLayout) view.findViewById(R.id.mTabLayout);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.mRecyclerView);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.mSwipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setRefreshing(true);

        for (int i = 0; i < mStr.length; i++) {
            mTabLayout.addTab(mTabLayout.newTab().setText(mStr[i]));
        }
        mTabLayout.addOnTabSelectedListener(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        appManagerAdapteer = new AppManagerAdapter(getActivity(), mList);
        mRecyclerView.setAdapter(appManagerAdapteer);
        appManagerAdapteer.setOnClickListener(new AppManagerAdapter.onClickListener() {
            @Override
            public void onClick(int position) {
                Intent intent = new Intent(getActivity(), AppMessageActivity.class);
                AppModel appModel = mList.get(position);
                intent.putExtra("name", appModel.getAppName());
                intent.putExtra("packageName", appModel.getAppPackageName());
                startActivity(intent);
            }
        });

        new Thread(new Runnable() {
            @Override
            public void run() {
                mAllList = AppUtils.getAllApp(getActivity());
                mHandler.sendEmptyMessage(1000);
            }
        }).start();
    }

    //index 0:已安装 1：系统 2:全部
    private void parsingApk(int index) {
        currentTab = index;
        if (mList.size() != 0) {
            mList.clear();
        }
        for (int i = 0; i < mAllList.size(); i++) {
            AppModel model = mAllList.get(i);
            switch (index) {
                case 0:
                    if (!model.isSystem()) {
                        mList.add(model);
                    }
                    break;
                case 1:
                    if (model.isSystem()) {
                        mList.add(model);
                    }
                    break;
                case 2:
                    mList.addAll(mAllList);
                    break;
            }
        }
        appManagerAdapteer.notifyDataSetChanged();
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        switch (tab.getPosition()) {
            case 0:
                parsingApk(0);
                break;
            case 1:
                parsingApk(1);
                break;
            case 2:
                parsingApk(2);
                break;
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(unReceiver);
    }

    //下拉刷新
    @Override
    public void onRefresh() {
        mAllList = AppUtils.getAllApp(getActivity());
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                parsingApk(currentTab);
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }, 200);
    }

    //监听卸载的广播
    class unInstallReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            L.i("action:" + action);
            switch (action) {
                case Intent.ACTION_PACKAGE_REMOVED:
                    //如果卸载了，那就可以获取到包名
                    String packageName = intent.getData().getSchemeSpecificPart();
                    L.i("packageName:" + packageName);
                    for (int i = 0; i < mList.size(); i++) {
                        AppModel appModel = mList.get(i);
                        if (appModel.getAppPackageName().equals(packageName)) {
                            //获取到position
                            mList.remove(i);
                            appManagerAdapteer.notifyDataSetChanged();
                            return;
                        }
                    }
                    break;
            }
        }
    }
}
