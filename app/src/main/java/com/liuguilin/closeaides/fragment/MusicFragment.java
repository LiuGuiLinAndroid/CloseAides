package com.liuguilin.closeaides.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.liuguilin.closeaides.R;
import com.liuguilin.closeaides.adapter.MusicListAdapter;
import com.liuguilin.closeaides.entity.Constans;
import com.liuguilin.closeaides.model.MusicListModel;
import com.liuguilin.closeaides.model.MusicListRecyclerModel;
import com.liuguilin.closeaides.model.MusicMoreModel;
import com.liuguilin.closeaides.ui.MusicMoreActivity;
import com.liuguilin.closeaides.utils.OkHttpUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.fragment
 *  文件名:   MusicFragment
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/7 19:56
 *  描述：    音乐
 */
public class MusicFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private RecyclerView mRecyclerView;
    private List<MusicListRecyclerModel> mList = new ArrayList<>();
    private MusicListAdapter musicListAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_music, null);
        initView(view);
        return view;
    }

    //初始化
    private void initView(View view) {
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.mSwipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setRefreshing(true);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.mRecyclerView);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        musicListAdapter = new MusicListAdapter(getActivity(), mList);
        mRecyclerView.setAdapter(musicListAdapter);
        //点击事件
        musicListAdapter.setOnClickListener(new MusicListAdapter.onClickListener() {
            @Override
            public void onClick(int position) {
                Intent intent = new Intent(getActivity(), MusicMoreActivity.class);
                intent.putExtra("id", mList.get(position).getId());
                startActivity(intent);
            }
        });

        initMusicList();
    }

    //获取音乐列表
    private void initMusicList() {
        OkHttpUtils.get(Constans.MUSIC_LIST, new OkHttpUtils.onHttpCallback() {
            @Override
            public void onResponse(String result) {
                Gson gson = new Gson();
                MusicListModel model = gson.fromJson(result, MusicListModel.class);
                if (model.getRes() == 0) {
                    for (int i = 0; i < model.getData().size(); i++) {
                        initMusicMore(model.getData().get(i));
                    }
                }
            }

            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onFailure(String msg) {

            }
        });
    }

    //初始化音乐详情
    private void initMusicMore(String data) {
        OkHttpUtils.get(Constans.MUSIC_MORE + data, new OkHttpUtils.onHttpCallback() {
            @Override
            public void onResponse(String result) {
                Gson gson = new Gson();
                MusicMoreModel model = gson.fromJson(result, MusicMoreModel.class);
                initData(model);
            }

            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onFailure(String msg) {

            }
        });
    }

    //开始初始化数据
    private void initData(MusicMoreModel model) {
        MusicListRecyclerModel musicModel = new MusicListRecyclerModel();
        musicModel.setTitle(model.getData().getTitle());
        musicModel.setCover(model.getData().getCover());
        musicModel.setId(model.getData().getId());
        mList.add(musicModel);
        musicListAdapter.notifyDataSetChanged();
        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onRefresh() {
        if (mSwipeRefreshLayout.isRefreshing()) {
            mList.clear();
            initMusicList();
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (hidden) {
            if (mSwipeRefreshLayout.isRefreshing()) {
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }
    }
}
