package com.liuguilin.closeaides.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.liuguilin.closeaides.R;
import com.liuguilin.closeaides.adapter.MovieLsitAdapter;
import com.liuguilin.closeaides.entity.Constans;
import com.liuguilin.closeaides.model.MovieListModel;
import com.liuguilin.closeaides.model.MovieModel;
import com.liuguilin.closeaides.ui.MovieMoreActivity;
import com.liuguilin.closeaides.utils.OkHttpUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.fragment
 *  文件名:   MovieFragment
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/7 19:56
 *  描述：    电影
 */
public class MovieFragment extends Fragment {

    private RecyclerView mMovieRecyclerView;
    private List<MovieListModel> mList = new ArrayList<>();
    private MovieLsitAdapter movieListAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movie, null);
        initView(view);
        return view;
    }

    private void initView(View view) {
        mMovieRecyclerView = (RecyclerView) view.findViewById(R.id.mMovieRecyclerView);
        mMovieRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        movieListAdapter = new MovieLsitAdapter(getActivity(), mList);
        mMovieRecyclerView.setAdapter(movieListAdapter);

        movieListAdapter.setOnClickListener(new MovieLsitAdapter.onClickListener() {
            @Override
            public void onClick(int position) {
                Intent intent = new Intent(getActivity(), MovieMoreActivity.class);
                intent.putExtra("name", mList.get(position).getSubtitle());
                intent.putExtra("id", mList.get(position).getId());
                startActivity(intent);
            }
        });

        initMovieData();
    }

    //加载电影数据
    private void initMovieData() {
        OkHttpUtils.get(Constans.MOVIE_LIST, new OkHttpUtils.onHttpCallback() {
            @Override
            public void onResponse(String result) {
                MovieModel model = new Gson().fromJson(result, MovieModel.class);
                loadData(model);
            }

            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onFailure(String msg) {

            }
        });
    }

    //显示数据
    private void loadData(MovieModel model) {
        for (int i = 0; i < model.getData().size(); i++) {
            MovieListModel mode = new MovieListModel();
            mode.setId(model.getData().get(i).getItem_id());
            mode.setTitle(model.getData().get(i).getTitle());
            mode.setImg_url(model.getData().get(i).getImg_url());
            mode.setSubtitle(model.getData().get(i).getSubtitle());
            mList.add(mode);
        }
        movieListAdapter.notifyDataSetChanged();
    }
}
