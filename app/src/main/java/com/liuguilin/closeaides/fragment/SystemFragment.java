package com.liuguilin.closeaides.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.liuguilin.closeaides.R;
import com.liuguilin.closeaides.adapter.SystemManagerAdapter;
import com.liuguilin.closeaides.ui.BluetoothManagerActivity;
import com.liuguilin.closeaides.ui.CacheClearActivity;
import com.liuguilin.closeaides.ui.FileBrowseActivity;
import com.liuguilin.closeaides.ui.ScreenBadPointActivity;
import com.liuguilin.closeaides.ui.SystemMessageActivity;
import com.liuguilin.closeaides.ui.WifiManagerActivity;
import com.liuguilin.closeaides.utils.FileUtils;
import com.liuguilin.closeaides.utils.FormatUtils;
import com.liuguilin.closeaides.utils.L;
import com.liuguilin.closeaides.utils.SystemFileUtils;

import java.util.ArrayList;
import java.util.List;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.fragment
 *  文件名:   SystemFragment
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/6 21:29
 *  描述：    系统
 */
public class SystemFragment extends Fragment {

    private RecyclerView mRecyclerView;
    //, "WIFI管理", "蓝牙管理"
    private String[] mStr = {"屏幕检测", "文件浏览", "系统信息", "缓存清理"};
    private List<String> mList = new ArrayList<>();
    private SystemManagerAdapter systemManagerAdapter;

    //饼状图表
    private PieChart mPieChartRam, mPieChartRom;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_system, null);
        initView(view);
        return view;
    }

    private void initView(View view) {

        for (int i = 0; i < mStr.length; i++) {
            mList.add(mStr[i]);
        }
        mPieChartRam = (PieChart) view.findViewById(R.id.mPieChartRam);
        mPieChartRom = (PieChart) view.findViewById(R.id.mPieChartRom);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.mRecyclerView);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 4));
        systemManagerAdapter = new SystemManagerAdapter(getActivity(), mList);
        mRecyclerView.setAdapter(systemManagerAdapter);

        //点击事件
        systemManagerAdapter.setOnClickListener(new SystemManagerAdapter.onClickListener() {
            @Override
            public void onClick(int position) {
                switch (position) {
                    case 0:
                        startActivity(new Intent(getActivity(), ScreenBadPointActivity.class));
                        break;
                    case 1:
                        startActivity(new Intent(getActivity(), FileBrowseActivity.class));
                        break;
                    case 2:
                        startActivity(new Intent(getActivity(), SystemMessageActivity.class));
                        break;
                    case 3:
                        startActivity(new Intent(getActivity(), CacheClearActivity.class));
                        break;
                    case 4:
                        startActivity(new Intent(getActivity(), WifiManagerActivity.class));
                        break;
                    case 5:
                        startActivity(new Intent(getActivity(), BluetoothManagerActivity.class));
                        break;
                }
            }
        });

        initMpAndroidChart(mPieChartRam);
        initMpAndroidChart(mPieChartRom);

    }

    //初始化图图表
    private void initMpAndroidChart(PieChart mPieChart) {
        L.i("图片:" + FileUtils.getTotalSize(getActivity(), 0));
        L.i("音频:" + FileUtils.getTotalSize(getActivity(), 1));
        L.i("视频:" + FileUtils.getTotalSize(getActivity(), 2));
        L.i("其他:" + FileUtils.getOtherTotalSize(getActivity()));
        L.i("可用大小:" + FileUtils.getSDSize(1));
        L.i("总大小:" + FileUtils.getSDSize(0));

        //显示百分比
        mPieChart.setUsePercentValues(true);
        //铺满
        mPieChart.setDrawHoleEnabled(false);
        // 触摸旋转
        mPieChart.setRotationEnabled(true);
        if(mPieChart == mPieChartRam){
            //设置简介
            mPieChart.getDescription().setText("RAM总大小:" + FormatUtils.FormatSize(Math.abs(SystemFileUtils.getTotalMemorySize(getActivity()))));
        }else if(mPieChart == mPieChartRom){
            //设置简介
            mPieChart.getDescription().setText("ROM总大小:" + FormatUtils.FormatSize(FileUtils.getSDSize(0)));
        }
        mPieChart.getDescription().setTextColor(Color.WHITE);
        mPieChart.getDescription().setTextSize(10f);
        mPieChart.getDescription().setXOffset(5);
        mPieChart.getDescription().setYOffset(5);


        //entries.add(new PieEntry(FileUtils.getTotalSize(getActivity(), 0), "图片" + FormatUtils.FormatSize(FileUtils.getTotalSize(getActivity(), 0))));
        //entries.add(new PieEntry(FileUtils.getTotalSize(getActivity(), 1), "音频" + FormatUtils.FormatSize(FileUtils.getTotalSize(getActivity(), 1))));
        //entries.add(new PieEntry(FileUtils.getTotalSize(getActivity(), 2), "视频" + FormatUtils.FormatSize(FileUtils.getTotalSize(getActivity(), 2))));
        //entries.add(new PieEntry(FileUtils.getOtherTotalSize(getActivity()), "其他" + FormatUtils.FormatSize(FileUtils.getOtherTotalSize(getActivity()))));

        if (mPieChart == mPieChartRam) {
            ArrayList<PieEntry> entries = new ArrayList<PieEntry>();
            entries.add(new PieEntry(Math.abs(SystemFileUtils.getAvailMemorySize(getActivity())), "可用:" + FormatUtils.FormatSize(SystemFileUtils.getAvailMemorySize(getActivity()))));
            entries.add(new PieEntry(
                    Math.abs(SystemFileUtils.getTotalMemorySize(getActivity())) - Math.abs(SystemFileUtils.getAvailMemorySize(getActivity()))
                    , "已用:" +
                    FormatUtils.FormatSize(Math.abs(SystemFileUtils.getTotalMemorySize(getActivity()))
                            - Math.abs(SystemFileUtils.getAvailMemorySize(getActivity())))));
            //设置数据
            setPieChartData(mPieChart, entries);
        } else if (mPieChart == mPieChartRom) {
            ArrayList<PieEntry> entries = new ArrayList<PieEntry>();
            entries.add(new PieEntry(FileUtils.getSDSize(1), "可用" + FormatUtils.FormatSize(FileUtils.getSDSize(1))));
            entries.add(new PieEntry(FileUtils.getSDSize(0) - FileUtils.getSDSize(1), "已用" + FormatUtils.FormatSize((FileUtils.getSDSize(0) - FileUtils.getSDSize(1)))));
            //设置数据
            setPieChartData(mPieChart, entries);
        }

        //默认动画
        mPieChart.animateXY(1400, 1400);

        //设置分类颜色
        mPieChart.getLegend().setTextColor(Color.WHITE);
        mPieChart.getLegend().setOrientation(Legend.LegendOrientation.HORIZONTAL);
    }

    //设置图表数据
    private void setPieChartData(PieChart mPieChart, ArrayList<PieEntry> entries) {
        PieDataSet dataSet = new PieDataSet(entries, "");
        //模块间的空隙
        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(5f);
        ArrayList<Integer> colors = new ArrayList<Integer>();
        for (int c : ColorTemplate.COLORFUL_COLORS) {
            colors.add(c);
        }
        colors.add(ColorTemplate.getHoloBlue());
        //设置模块颜色
        dataSet.setColors(colors);

        //模块间的提示
        dataSet.setValueLinePart1OffsetPercentage(80.f);
        dataSet.setValueLinePart1Length(0.2f);
        dataSet.setValueLinePart2Length(0.4f);
        //设置颜色
        dataSet.setValueLineColor(Color.WHITE);
        dataSet.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);

        PieData data = new PieData(dataSet);
        //文本提示的格式
        data.setValueFormatter(new PercentFormatter());
        //文本提示的大小
        data.setValueTextSize(11f);
        //文本提示的颜色
        data.setValueTextColor(Color.WHITE);
        //设置数据
        mPieChart.setData(data);

        // 撤销所有的亮点
        mPieChart.highlightValues(null);
        //刷新
        mPieChart.invalidate();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            if (mPieChartRam != null) {
                //默认动画
                mPieChartRam.animateXY(1400, 1400);
            }
        }
    }
}
