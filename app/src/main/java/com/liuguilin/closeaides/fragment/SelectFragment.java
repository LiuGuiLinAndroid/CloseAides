package com.liuguilin.closeaides.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.liuguilin.closeaides.R;
import com.liuguilin.closeaides.entity.Constans;
import com.liuguilin.closeaides.model.SelectModel;
import com.liuguilin.closeaides.ui.WebActivity;
import com.liuguilin.closeaides.utils.GlideUtils;
import com.liuguilin.closeaides.utils.L;
import com.liuguilin.closeaides.utils.OkHttpUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.fragment
 *  文件名:   SelectFragment
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/7 19:56
 *  描述：    精选
 */
public class SelectFragment extends Fragment {

    private ViewPager mViewPager;
    private TextView tv_now_count;
    private TextView tv_all_count;

    //数据
    private List<SelectModel> mSelectList = new ArrayList<>();
    //view
    private List<View> mViewList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_select, null);
        initView(view);
        return view;
    }

    private void initView(View view) {

        mViewPager = (ViewPager) view.findViewById(R.id.mViewPager);
        tv_now_count = (TextView) view.findViewById(R.id.tv_now_count);
        tv_all_count = (TextView) view.findViewById(R.id.tv_all_count);

        //滑动监听
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                setPosition(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        initSelect();
    }

    private void setPosition(int position) {
        tv_all_count.setText(mSelectList.size() + "");
        tv_now_count.setText(position + 1 + "");
    }

    //初始化精选数据
    private void initSelect() {
        OkHttpUtils.get(Constans.SELECT_URL, new OkHttpUtils.onHttpCallback() {
            @Override
            public void onResponse(String result) {
                L.i("result：" + result);
                initViewPager(result);
            }

            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onFailure(String msg) {

            }
        });
    }

    //初始化ViewPager
    private void initViewPager(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            JSONArray jsonArray = jsonObject.getJSONArray("data");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject json = (JSONObject) jsonArray.get(i);
                String hp_title = json.getString("hp_title");
                String imgUrl = json.getString("hp_img_url");
                String hp_author = json.getString("hp_author");
                String hp_content = json.getString("hp_content");
                String web_url = json.getString("web_url");
                String hp_update_time = json.getString("last_update_date");

                SelectModel model = new SelectModel();
                model.setTitle(hp_title);
                model.setImgUrl(imgUrl);
                model.setAuthor(hp_author);
                model.setContent(hp_content);
                model.setWebUrl(web_url);
                model.setUpdateTime(hp_update_time);
                mSelectList.add(model);
            }

            for (int i = 0; i < mSelectList.size(); i++) {
                //初始化数据
                View view = View.inflate(getActivity(), R.layout.layout_select_item, null);
                final SelectModel model = mSelectList.get(i);
                TextView tv_vol = (TextView) view.findViewById(R.id.tv_vol);
                tv_vol.setText(model.getTitle());
                TextView tv_author = (TextView) view.findViewById(R.id.tv_author);
                tv_author.setText(model.getAuthor());
                TextView tv_content = (TextView) view.findViewById(R.id.tv_content);
                tv_content.setText(model.getContent());
                TextView tv_date = (TextView) view.findViewById(R.id.tv_date);
                tv_date.setText(model.getUpdateTime());
                ImageView iv_title = (ImageView) view.findViewById(R.id.iv_title);
                GlideUtils.loadImageViewCenterCrop(getActivity(), model.getImgUrl(), iv_title);

                //设置点击事件
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(getActivity(), WebActivity.class);
                        i.putExtra("name", model.getAuthor());
                        i.putExtra("url", model.getWebUrl());
                        startActivity(i);
                    }
                });
                mViewList.add(view);
            }
            tv_all_count.setText(mSelectList.size() + "");
            tv_now_count.setText(1 + "");
            //设置预加载
            mViewPager.setOffscreenPageLimit(mSelectList.size());
            mViewPager.setAdapter(new SelectPagerAdapter());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //适配器
    public class SelectPagerAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return mViewList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            ((ViewPager) container).addView(mViewList.get(position));
            return mViewList.get(position);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView(mViewList.get(position));
        }
    }
}
