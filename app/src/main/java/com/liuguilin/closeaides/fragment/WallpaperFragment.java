package com.liuguilin.closeaides.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.liuguilin.closeaides.R;
import com.liuguilin.closeaides.adapter.WallpaperHorizontalAdapter;
import com.liuguilin.closeaides.adapter.WallpaperVerticalAdapter;
import com.liuguilin.closeaides.entity.Constans;
import com.liuguilin.closeaides.model.CategoryModel;
import com.liuguilin.closeaides.model.EverydayModel;
import com.liuguilin.closeaides.model.SpecialModel;
import com.liuguilin.closeaides.ui.BannerActivity;
import com.liuguilin.closeaides.ui.CategoryActivity;
import com.liuguilin.closeaides.utils.GlideUtils;
import com.liuguilin.closeaides.utils.L;
import com.liuguilin.closeaides.utils.OkHttpUtils;
import com.liuguilin.closeaides.view.AutoViewPager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.fragment
 *  文件名:   WallpaperFragment
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/6 21:28
 *  描述：    壁纸
 */
public class WallpaperFragment extends Fragment {

    private AutoViewPager mViewPager;
    private RecyclerView mHorizontalRecyclerView;
    private RecyclerView mVerticalRecyclerView;

    //分类的适配器
    private WallpaperHorizontalAdapter wallpaperHorizontalAdapter;
    //每日推荐的适配器
    private WallpaperVerticalAdapter wallpaperVerticalAdapter;

    //轮播数据
    private List<SpecialModel> mSpecialList = new ArrayList<>();
    //分类的数据
    private List<CategoryModel> mCategoryList = new ArrayList<>();
    //每日推荐的数据
    private List<EverydayModel> mEverydayList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_wallpaper, null);
        initView(view);
        return view;
    }

    private void initView(View view) {

        mViewPager = (AutoViewPager) view.findViewById(R.id.mViewPager);
        mViewPager.setAutoTime(2000);
        mViewPager.setStartAuto(true);

        mHorizontalRecyclerView = (RecyclerView) view.findViewById(R.id.mHorizontalRecyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mHorizontalRecyclerView.setLayoutManager(linearLayoutManager);
        //设置好适配器
        wallpaperHorizontalAdapter = new WallpaperHorizontalAdapter(getActivity(), mCategoryList);
        mHorizontalRecyclerView.setAdapter(wallpaperHorizontalAdapter);

        mVerticalRecyclerView = (RecyclerView) view.findViewById(R.id.mVerticalRecyclerView);
        mVerticalRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        wallpaperVerticalAdapter = new WallpaperVerticalAdapter(getActivity(), mEverydayList);
        mVerticalRecyclerView.setAdapter(wallpaperVerticalAdapter);

        initWallpaper();
    }

    //加载壁纸数据
    private void initWallpaper() {
        //获取壁纸信息
        OkHttpUtils.get(Constans.WALLPAPER_URL, new OkHttpUtils.onHttpCallback() {

            @Override
            public void onResponse(String result) {
                L.i("onResponse");
                parsingJson(result);
            }

            @Override
            public void onFailure(Call call, IOException e) {
                L.i("onFailure" + e.toString());
            }

            @Override
            public void onFailure(String msg) {
                L.i("onFailure");
            }
        });

    }

    /**
     * 在本课程中，我将使用两种数据解析的方法
     * Retrofit2.0
     * OkHttp3 封装
     * 在本课程中，我将使用三种的解析Json的方法
     * 原生的API ： JSONObject JSONArray + OkHttp
     * Google的Gson + OkHttp
     * Retrofit的Gson + Retrofit
     */

    //解析壁纸Json
    private void parsingJson(String json) {
        try {
            JSONObject jsonObject = new JSONObject(json);
            //分类
            JSONArray jsonArrayCategory = jsonObject.getJSONArray("category");
            //每日推荐
            JSONArray jsonArrayEveryday = jsonObject.getJSONArray("everyday");
            //轮播
            JSONArray jsonArraySpecial = jsonObject.getJSONArray("special");

            //初始化轮播图
            initViewPager(jsonArraySpecial);
            //初始化分类
            initHorizontalRecyclerView(jsonArrayCategory);
            //初始化每日推荐
            initVerticalRecyclerView(jsonArrayEveryday);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //初始化轮播图
    private void initViewPager(JSONArray jsonArraySpecial) {
        L.i("initViewPager");
        for (int i = 0; i < jsonArraySpecial.length(); i++) {
            try {
                JSONObject jsonObject = (JSONObject) jsonArraySpecial.get(i);
                //获取到数据
                SpecialModel speciaModel = new SpecialModel();
                speciaModel.setName(jsonObject.getString("name"));
                speciaModel.setDesc(jsonObject.getString("desc"));
                speciaModel.setIconUrl(jsonObject.getString("icon"));
                speciaModel.setUrl(jsonObject.getString("url"));
                mSpecialList.add(speciaModel);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        //开始布局
        for (int i = 0; i < mSpecialList.size(); i++) {
            final SpecialModel model = mSpecialList.get(i);
            View view = View.inflate(getActivity(), R.layout.layout_specia_item, null);
            ImageView iv_special_bg = (ImageView) view.findViewById(R.id.iv_special_bg);
            //解析图片
            GlideUtils.loadImageViewCenterCrop(getActivity(), model.getIconUrl(), iv_special_bg);
            TextView tv_special_name = (TextView) view.findViewById(R.id.tv_special_name);
            tv_special_name.setText(model.getName());
            TextView tv_special_desc = (TextView) view.findViewById(R.id.tv_special_desc);
            tv_special_desc.setText(model.getDesc());

            //点击事件
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), BannerActivity.class);
                    intent.putExtra("name", model.getName());
                    intent.putExtra("url", model.getUrl());
                    startActivity(intent);
                }
            });
            //装载
            mViewPager.setView(view);
        }
    }

    //初始化分类
    private void initHorizontalRecyclerView(JSONArray jsonArrayCategory) {
        for (int i = 0; i < jsonArrayCategory.length(); i++) {
            try {
                JSONObject jsonObject = (JSONObject) jsonArrayCategory.get(i);
                CategoryModel model = new CategoryModel();
                model.setName(jsonObject.getString("name"));
                model.setCover(jsonObject.getString("cover"));
                model.setUrl(jsonObject.getString("url"));
                mCategoryList.add(model);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        //刷新适配器
        wallpaperHorizontalAdapter.notifyDataSetChanged();
        //点击事件
        wallpaperHorizontalAdapter.setOnClickListener(new WallpaperHorizontalAdapter.onClickListener() {
            @Override
            public void onClick(int position) {
                CategoryModel model = mCategoryList.get(position);
                Intent intent = new Intent(getActivity(), CategoryActivity.class);
                intent.putExtra("name", model.getName());
                intent.putExtra("url", model.getUrl());
                startActivity(intent);
            }
        });
    }

    //初始化每日推荐
    private void initVerticalRecyclerView(JSONArray jsonArrayEveryday) {
        for (int i = 0; i < jsonArrayEveryday.length(); i++) {
            try {
                JSONObject jsonObject = (JSONObject) jsonArrayEveryday.get(i);
                EverydayModel model = new EverydayModel();
                model.setDate(jsonObject.getString("date"));
                model.setUrl(jsonObject.getString("url"));
                model.setImage(jsonObject.getString("image"));
                mEverydayList.add(model);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        //刷新适配器
        wallpaperVerticalAdapter.notifyDataSetChanged();
        //点击事件
        wallpaperVerticalAdapter.setOnClickListener(new WallpaperVerticalAdapter.onClickListener() {
            @Override
            public void onClick(int position) {
                EverydayModel model = mEverydayList.get(position);
                Intent intent = new Intent(getActivity(), BannerActivity.class);
                intent.putExtra("name", model.getDate());
                intent.putExtra("url", model.getUrl());
                startActivity(intent);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mViewPager != null) {
            mViewPager.setStartAuto(false);
        }
    }
}
