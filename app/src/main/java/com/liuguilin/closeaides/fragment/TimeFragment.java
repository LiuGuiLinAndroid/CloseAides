package com.liuguilin.closeaides.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.liuguilin.closeaides.R;
import com.liuguilin.closeaides.adapter.TimeAdapter;
import com.liuguilin.closeaides.entity.Constans;
import com.liuguilin.closeaides.model.TimeModel;
import com.liuguilin.closeaides.ui.TimeAddActivity;
import com.liuguilin.closeaides.user.TimeNote;
import com.liuguilin.closeaides.utils.DialogUtils;
import com.liuguilin.closeaides.utils.L;
import com.liuguilin.closeaides.utils.TimeUtils;

import java.util.ArrayList;
import java.util.List;

import cn.bmob.v3.exception.BmobException;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.fragment
 *  文件名:   TimeFragment
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/7 19:51
 *  描述：    时光
 *
 *  默认第一个为封面
 *  按照时间排序
 */
public class TimeFragment extends Fragment implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    private ImageView iv_add;
    private TextView tv_title;
    private TextView tv_day;
    private TextView tv_date;
    private RecyclerView mTimeRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private TimeAdapter timeAdapter;
    private List<TimeModel> mList = new ArrayList<>();

    private BroUpdateTimeReceiver broUpdateTimeReceiver;
    private List<TimeNote> listData;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_time, null);
        initView(view);
        return view;
    }

    private void initView(View view) {

        broUpdateTimeReceiver = new BroUpdateTimeReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constans.BRO_UPDATE_TIME);
        getActivity().registerReceiver(broUpdateTimeReceiver, intentFilter);

        iv_add = (ImageView) view.findViewById(R.id.iv_add);
        iv_add.setOnClickListener(this);
        tv_title = (TextView) view.findViewById(R.id.tv_title);
        tv_day = (TextView) view.findViewById(R.id.tv_day);
        tv_date = (TextView) view.findViewById(R.id.tv_date);
        mTimeRecyclerView = (RecyclerView) view.findViewById(R.id.mTimeRecyclerView);
        mTimeRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        timeAdapter = new TimeAdapter(getActivity(), mList);
        mTimeRecyclerView.setAdapter(timeAdapter);

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.mSwipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setRefreshing(true);

        //设置长按事件
        timeAdapter.setOnClickListener(new TimeAdapter.onClickListener() {
            @Override
            public void onLongClick(final int position) {
                DialogUtils.showStandardDialog(getActivity(), "", "是否删除这条时光?")
                        .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                L.i("position" + position);
                                TimeNote note = listData.get(position + 1);
                                L.i("" + note.getTitle());
                                //删除
                                TimeUtils.Delete(note, note.getObjectId(), new TimeUtils.onUpdateListener() {
                                    @Override
                                    public void onSuccessful() {
                                        mList.remove(position);
                                        timeAdapter.notifyDataSetChanged();
                                    }

                                    @Override
                                    public void onFailure(BmobException e) {

                                    }
                                });
                            }
                        }).show();
            }
        });

        loadTimeData();
    }

    //查询数据
    private void loadTimeData() {
        TimeUtils.QueryAll(new TimeUtils.onAllQueryListener() {
            @Override
            public void onSuccessful(List<TimeNote> list) {
                listData = list;
                for (int i = 0; i < list.size(); i++) {
                    TimeNote note = list.get(i);
                    TimeModel model = new TimeModel();
                    model.setTitle(note.getTitle());
                    model.setTime(note.getTime());
                    model.setColor(note.getColor());
                    mList.add(model);
                }
                mSwipeRefreshLayout.setRefreshing(false);
                //第一個
                TimeModel model = mList.get(0);
                tv_title.setText(model.getTitle());
                tv_date.setText(model.getTime());
                tv_day.setText(timeAdapter.ComputationTime(model.getTime()) + "");
                //可以设置背景颜色
                timeAdapter.notifyDataSetChanged();
                mList.remove(0);
                timeAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(BmobException e) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_add:
                startActivity(new Intent(getActivity(), TimeAddActivity.class));
                //向上动画
                getActivity().overridePendingTransition(R.anim.animation_in, R.anim.animation_out);
                break;
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (hidden) {
            if (mSwipeRefreshLayout.isRefreshing()) {
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(broUpdateTimeReceiver);
    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    class BroUpdateTimeReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (Constans.BRO_UPDATE_TIME.equals(action)) {
                mSwipeRefreshLayout.setRefreshing(true);
                //更新
                mList.clear();
                loadTimeData();
            }
        }
    }

}
