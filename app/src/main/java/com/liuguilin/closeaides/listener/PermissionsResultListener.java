package com.liuguilin.closeaides.listener;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.listener
 *  文件名:   PermissionsResultListener
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/15 11:29
 *  描述：    请求权限的接口
 */
public interface PermissionsResultListener {
    //成功
    void onSuccessful(int[] grantResults);

    //失败
    void onFailure();
}
