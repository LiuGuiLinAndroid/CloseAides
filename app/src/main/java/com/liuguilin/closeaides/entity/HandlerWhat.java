package com.liuguilin.closeaides.entity;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.entity
 *  文件名:   HandlerWhat
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/5 21:29
 *  描述：    handler的一些消息
 */
public class HandlerWhat {

    //第一次启动
    public static final int HANDLER_IS_FIRST = 1000;

}
