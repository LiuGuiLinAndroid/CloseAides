package com.liuguilin.closeaides.entity;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.entity
 *  文件名:   Key
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/8 10:07
 *  描述：    存放ID
 */
public class Key {

    //Bmob Application ID
    public static final String BMOB_APPLICATION_KEY = "00a0df3ed7cb6081ae0125a8ba74aa5d";

    //和风天气KEY
    public static final String HF_WEATHER_KEY = "cd6a56ab6f4544a4b5d8206064551f85";
}

