package com.liuguilin.closeaides.entity;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.entity
 *  文件名:   Constans
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/5 21:24
 *  描述：    常亮类
 */
public class Constans {

    //调试开关
    public static final boolean DEBUG = true;

    //壁纸接口
    public static final String WALLPAPER_URL = "http://open.lovebizhi.com/baidu_rom.php?width=720&height=1280&type=1";

    //精选接口
    public static final String SELECT_URL = "http://v3.wufazhuce.com:8000/api/hp/more/0";

    //音乐列表
    public static final String MUSIC_LIST = "http://v3.wufazhuce.com:8000/api/music/idlist/0";

    //歌曲详情
    public static final String MUSIC_MORE = "http://v3.wufazhuce.com:8000/api/music/detail/";

    //电影列表
    public static final String MOVIE_LIST = "http://v3.wufazhuce.com:8000/api/channel/movie/more/0?channel=wdj&version=4.0.2&uuid=ffffffff-a90e-706a-63f7-ccf973aae5ee&platform=android";

    //城市列表
    public static final String CITY_LIST = "https://cdn.heweather.com/china-city-list.json";

    //电影详情

    //当前电量
    public static String BATTERY = "";
    //当前温度
    public static double BatteryT = 0.0;


    //通知更新時光数据
    public static final String BRO_UPDATE_TIME = "com.liuguilin.bro_update_time";

    //天气接口的Base Url
    public static final String WEATHER_BASE_URL = "https://free-api.heweather.com/v5/";

    //天气城市
    public static final String WEATHER_NOW_CITY = "北京";

}

