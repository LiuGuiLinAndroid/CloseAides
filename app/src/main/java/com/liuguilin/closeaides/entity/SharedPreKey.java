package com.liuguilin.closeaides.entity;

/*
 *  项目名：  CloseAides 
 *  包名：    com.liuguilin.closeaides.entity
 *  文件名:   SharedPreKey
 *  创建者:   刘某人程序员
 *  创建时间:  2017/4/5 21:30
 *  描述：    SharedPreferences的key
 */
public class SharedPreKey {

    //判断是否是第一次运行的key
    public static final String SHARED_IS_FIRST_KEY = "isFirst";

    //是否需要弹框
    public static final String SHARED_IS_DIALOG = "isDialog";
}
